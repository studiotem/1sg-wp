var cc      = initCookieConsent();  
cc.run({
    current_lang : document.documentElement.lang,
    autoclear_cookies : true,
    theme_css: '',
    cookie_name: 'cc_cookie_'+document.domain,
    cookie_expiration : 365,
    page_scripts: true,
    auto_language: document,
    cookie_domain: location.hostname,
    autorun: true,
    delay: 0,
    force_consent: false,
    hide_from_bots: false,
    remove_cookie_tables: false,
    cookie_path: "/",
    cookie_same_site: "Lax",
    use_rfc_cookie: false,
    revision: 0,
    gui_options: cmCookieStyle,     
    onAccept: function (cookie) {

    }, 
    onChange: function (cookie, changed_preferences) {
        if (changed_preferences.indexOf('analytics') > -1) {
            if (!cc.allowedCategory('analytics')) {

                window.dataLayer = window.dataLayer || [];

                function gtag() { dataLayer.push(arguments); }

                gtag('consent', 'default', {
                    'ad_storage': 'denied',
                    'analytics_storage': 'denied'
                });

            }
        }
    }, 
    languages: cmCookieConsent
});

var div = document.getElementById('s-c-bn');
div.innerHTML += '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M368 368L144 144M368 144L144 368"/></svg>';