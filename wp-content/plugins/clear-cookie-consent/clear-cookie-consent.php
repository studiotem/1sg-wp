<?php
	 
/* 
Plugin Name: Clear Cookie Consent
Plugin URI: http://www.clearmedia.sk/
Version: 1.0
Description:  
Author: CLEAR Media s.r.o.
Author URI: http://www.clearmedia.sk/
*/
 
class Clear_Cookie_Consent
{

	public function __construct() { 
		
	}

	public function init()
    {  
		add_action( 'admin_init', array( $this, 'ccc_plugin_activate') );
		add_filter( 'init', array( $this, 'ccc_add_options_page' ) ); 
		add_action('admin_head', array( $this, 'ccc_admin_style' ));
		add_action( 'wp_enqueue_scripts', array( $this, 'ccc_scripts'), 100 );
		add_action('wp_head', array( $this, 'ccc_styles'), 100);
		require_once plugin_dir_path( __FILE__ ).'/clear-cookie-consent-acf.php';  
    }
      
	function ccc_scripts() 
	{ 
	    wp_register_script( 'ccc-script', plugin_dir_url( __FILE__ ) . '/assets/clear-cookie-consent.js', array('jquery'), '1.0.1', true );
	    wp_enqueue_script( 'ccc-script' );
	    wp_register_script( 'ccc-settings', plugin_dir_url( __FILE__ ) . '/assets/clear-cookie-consent-settings.js', array('jquery'), '1.0.1', true );
	    wp_enqueue_script( 'ccc-settings' );
        wp_enqueue_style( 'ccc-style', plugin_dir_url( __FILE__ ) . '/assets/clear-cookie-consent.css', array(), '1.0.1' );
	}
	 
	function ccc_styles()
	{ ?>
	<style>
			:root{
				--cc-bg: <?= get_field("cc-bg", "option")?>;
				--cc-text: <?= get_field("cc-text", "option")?>;
				--cc-btn-primary-bg: <?= get_field("cc-btn-primary-bg", "option")?>;
				--cc-btn-primary-text: <?= get_field("cc-btn-primary-text", "option")?>;
				--cc-btn-primary-hover-bg: <?= get_field("cc-btn-primary-hover-bg", "option")?>;
				--cc-btn-secondary-bg: <?= get_field("cc-btn-secondary-bg", "option")?>;
				--cc-btn-secondary-text: <?= get_field("cc-btn-secondary-text", "option")?>;
				--cc-btn-secondary-hover-bg: <?= get_field("cc-btn-secondary-hover-bg", "option")?>;
				--cc-toggle-bg-off: <?= get_field("cc-toggle-bg-off", "option")?>;
				--cc-toggle-bg-on: <?= get_field("cc-toggle-bg-on", "option")?>;
				--cc-toggle-bg-readonly: <?= get_field("cc-toggle-bg-readonly", "option")?>;
				--cc-toggle-knob-bg: <?= get_field("cc-toggle-knob-bg", "option")?>;
				--cc-toggle-knob-icon-color: <?= get_field("cc-toggle-knob-icon-color", "option")?>;
				--cc-block-text: <?= get_field("cc-block-text", "option")?>;
				--cc-cookie-category-block-bg: <?= get_field("cc-cookie-category-block-bg", "option")?>;
				--cc-cookie-category-block-bg-hover:<?= get_field("cc-cookie-category-block-bg-hover", "option")?>;
				--cc-section-border: <?= get_field("cc-section-border", "option")?>;
				--cc-cookie-table-border: <?= get_field("cc-cookie-table-border", "option")?>;
				--cc-webkit-scrollbar-bg: <?= get_field("cc-webkit-scrollbar-bg", "option")?>;
				--cc-webkit-scrollbar-bg-hover: <?= get_field("cc-webkit-scrollbar-bg-hover", "option")?>;
				--cc-overlay-bg: rgba(4, 6, 8, .85);
			}
		</style>
		<script> 
			<?php   
			$lang					= get_bloginfo("language");
			//$lang					= explode('-', get_bloginfo("language"));
			//$lang 				= reset($lang);	
			
			$ccStyleConsentModal	= get_field('cc-consent-modal', 'option');
			$ccStyleSettingsModal	= get_field('cc-settings-modal', 'option');
			
			$cmCookieStyle		= []; 
			$cmCookieStyle 		= [
				'consent_modal' 	=> [
					'layout'	=> $ccStyleConsentModal['layout'],
					'position'	=> $ccStyleConsentModal['positiony'].' '.$ccStyleConsentModal['positionx'],
					'transition'=> $ccStyleConsentModal['transition'],
				],
				'settings_modal'	=> [
					'layout'	=> $ccStyleSettingsModal['layout'],
					'position'	=> $ccStyleSettingsModal['position'],
					'transition'=> $ccStyleSettingsModal['transition'],
				] 
			]; 
			
			$cmCookieConsent 	= []; 
			if($lang) {
				$ccButtons 			= get_field('cc-buttons', 'option');
				$ccInfoText			= get_field('cc-info-text', 'option');
				$ccConsentModal 	= get_field('cc-consent-modal-text', 'option'); 
				$ccSettingsModal 	= get_field('cc-settings-modal-text', 'option');
				$cmCookieConsent[$lang]		= []; 
				$cmCookieConsent[$lang]['consent_modal'] = [  
	                'title'			=> $ccConsentModal['cc-title'],
	                'description'	=> $ccConsentModal['cc-description'] .' <button type="button" data-cc="c-settings" class="cc-link">'.$ccButtons['cc-change-settings-title'].'</button>',
	                'primary_btn' 	=> [
	                    'text'		=>  $ccButtons['cc-accept-all-title'],
	                    'role'		=> 'accept_all',
	                ],
	                'secondary_btn'	=>[
	                    'text'		=> $ccButtons['cc-accept-necessary-title'],
	                    'role'		=> 'accept_necessary',
	                ]
	            ];
	            $cmCookieConsent[$lang]['settings_modal'] = [ 
	                'title'					=> 'Cookies',
	                'save_settings_btn'		=> $ccButtons['cc-save-settings-title'],
	                'accept_all_btn'		=> $ccButtons['cc-accept-all-title'],
	                'reject_all_btn'		=> $ccButtons['cc-accept-necessary-title'],
	                'close_btn_label'		=> $ccButtons['cc-close-settings-title'],
	                'cookie_table_headers'	=> [
	                    ['col1'	=> 'Názov'],
	                    ['col2'	=> 'Doména'],
	                    ['col3'	=> 'Expirácia'],
	                    ['col4'	=> 'Popis']
	                ]  
				];  
				$ccCookiesKey 		= 0;
				$cmCookieConsent[$lang]['settings_modal']['blocks'] 	= [];
				$cmCookieConsent[$lang]['settings_modal']['blocks'][0]	= [
                    'title'			=> '',
                    'description'	=> str_replace('href=', 'class="cc-link" href=', $ccSettingsModal['cc-description']),
                ];
                $ccCookiesKey++; 
				$ccCookiesCategoriesNecessary	= get_field('cc-cookies-categories', 'option');
				$ccCookiesCategoriesOptional 	= get_field('cc-cookies-optional', 'option');
				$ccCookiesCategories 			= [
					$ccCookiesCategoriesNecessary['cc-cookies-necessary'],
					$ccCookiesCategoriesNecessary['cc-cookies-analytics'],
					$ccCookiesCategoriesNecessary['cc-cookies-targeting'],
				];  
				if($ccCookiesCategoriesOptional){
					$ccCookiesCategories		= array_merge($ccCookiesCategories, $ccCookiesCategoriesOptional); 
				}
				if($ccCookiesCategories){
					foreach($ccCookiesCategories as  $ccCookiesCategory){ 
						$cookieTable = $ccCookiesCategory['cc-cookies'];  
						if($cookieTable || $ccCookiesKey == 1){
							$cmCookieConsent[$lang]['settings_modal']['blocks'][$ccCookiesKey]	= [
		                    	'title'				=> $ccCookiesCategory['cc-title'],
		                    	'description'		=> $ccCookiesCategory['cc-description'],
		                    	'toggle'			=> [
		                        	'value'			=> $ccCookiesKey,
		                        	'enabled'		=> (string)($ccCookiesCategory['cc-enabled']) ? true : false,
		                        	'readonly'		=> (string)($ccCookiesCategory['cc-readonly']) ? true : false,
		                    	]
	                    	]; 
							if($cookieTable){
		                    	foreach($cookieTable as $cookieTableItem){
									$cmCookieConsent[$lang]['settings_modal']['blocks'][$ccCookiesKey]['cookie_table'][]	= [ 
		                            	'col1'		=> $cookieTableItem['cc-title'],
		                            	'col2'		=> $cookieTableItem['cc-domain'],
		                            	'col3'		=> $cookieTableItem['cc-expiration'],
		                            	'col4'		=> $cookieTableItem['cc-description'],
		                            	'is_regex'	=> $cookieTableItem['cc-regex'],     
				                	];
			                	}
							}
							$ccCookiesKey++;
		                } 
	               }
               	}    
				$cmCookieConsent[$lang]['settings_modal']['blocks'][$ccCookiesKey] = [
                    'title'			=> $ccInfoText['cc-title'],
                    'description'	=> str_replace('href=', 'class="cc-link" href=', $ccInfoText['cc-description']),
                ]; 
			} ?> 
			var cmCookieConsent = <?= json_encode($cmCookieConsent); ?>  
			var cmCookieStyle 	= <?= json_encode($cmCookieStyle); ?>  	
		</script> 
	<?php }
 
	function ccc_plugin_activate() 
	{
	    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  ( ! is_plugin_active( 'advanced-custom-fields/acf.php' ) && ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' )) ) {
	        add_action( 'admin_notices',  array( $this, 'ccc_child_plugin_notice') );
	
	        deactivate_plugins( plugin_basename( __FILE__ ) ); 
	
	        if ( isset( $_GET['activate'] ) ) {
	            unset( $_GET['activate'] );
	        }
	    }
	}

	function ccc_child_plugin_notice()
	{
	    echo '<div class="error">
		    <p>Sorry, but <b>Clear Cookie Consent</b> plugin requires the <a href="https://sk.wordpress.org/plugins/advanced-custom-fields/" target="_blank"><b>Advanced Custom Fields</b></a> to be installed and active. <a href="' . admin_url( 'plugins.php' ) . '">Return to Plugins</a>
		    </p>
	    </div>';
	} 
	
	function ccc_admin_style() 
	{
		echo '<style>
			#adminmenu #toplevel_page_clear-cookie-consent .wp-menu-image img {
				padding: 6px 0 0; 
				width: 20px;
			}
		</style>';
	}
    
    function ccc_add_options_page()
    {
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page(array(
				'page_title' 	=> 'Cookie Consent',
				'menu_title'	=> 'Cookie Consent',
				'menu_slug' 	=> 'clear-cookie-consent',
				'capability'	=> 'edit_posts',
				'redirect'		=> false,
				'position'		=> '50',
				'icon_url'		=> plugin_dir_url( __FILE__ ).'/assets/icon.svg'
			));
		}
	}

}

add_action( 'plugins_loaded', array( new Clear_Cookie_Consent, 'init' ) );
   