<?php 
/*
Plugin Name: Clear Facebook Feed
Version: 1.0.0
License: GPLv2 or later
*/
 

class ClearFacebookFeed
{

	public function __construct() 
    { 
        include dirname( __FILE__ ) .'/vendor/autoload.php';
        require_once(ABSPATH.'/wp-admin/includes/image.php');
        require_once(ABSPATH.'/wp-admin/includes/file.php');
        require_once(ABSPATH.'/wp-admin/includes/media.php');  
    }

    public function init()
    {
        add_shortcode('custom-facebook-feed', array($this, 'custom_facebook_feed') );
        add_filter( 'template_include', array( $this, 'include_template' ) );
        add_filter( 'init', array( $this, 'rewrite_rules' ) ); 
        
        $this->custom_facebook_feed_import_duplicites();
    }

    public function include_template( $template )
    { 
        if(get_query_var( 'feed_import' ) == 'facebook')
        {
            $this->custom_facebook_feed_import();
            exit; 
        } 
        return $template; 
    }

    public function flush_rules()
    {
        $this->rewrite_rules(); 
        flush_rewrite_rules();
    }

    public function rewrite_rules()
    {
        add_rewrite_rule( 'feed_import/(.+?)/?$', 'index.php?feed_import=$matches[1]', 'top');
        add_rewrite_tag( '%feed_import%', '([^&]+)' );
    }

    public function custom_facebook_feed_import_duplicites(){
        global $wpdb; 
        $array      = [];
        $get_rows   = $wpdb->get_results('SELECT ID, post_title FROM wp_posts WHERE post_type = "social"', 'ARRAY_N'); 
        if($get_rows){
            foreach($get_rows as $get_row){ 
                $array[$get_row['0']] = $get_row['1'];
            }
        }
        $duplicate_array = array_diff_key( $array , array_unique( $array ) );
        $unique_array = [];
        foreach ($array as $key => $value) {
            if ( in_array($value, $duplicate_array)) {
                $duplicate_array[$key] = $value;
            }
            else {
                $unique_array[$key] = $value;
            }  
        } 
        $delete_array = [];
        if( $duplicate_array){
            foreach( $duplicate_array as $id => $title){
                $delete_array[$title] = $id; 
            }
        }
        $delete_ids = array_values($delete_array);
        if($delete_ids){
            foreach($delete_ids as $delete_id){
                $wpdb->delete( 'wp_posts', array( 'ID' => $delete_id ) );
                $wpdb->delete( 'wp_postmeta', array( 'post_id' => $delete_id ) );
            }
        } 
    }
 
    public function custom_facebook_feed_import($atts = []) {
        //https://smashballoon.com/custom-facebook-feed/page-token/
        global $wpdb; 
        $secret_key     = get_field('facebook_secret_key', 'option');   //'09bcafa612c8989819f92eae37e77419';
        $app_id         = get_field('facebook_app_id', 'option');       //'414014780407102';
        $page_name      = get_field('facebook_page_name', 'option');    //'1sg.sk';
        $access_token   = get_field('facebook_access_token', 'option'); //'EAAF4i1PqZCT4BAMNtD00ghBUZCWI9XuLyUGXKgBjWOwKfWlcBnVbYggrijHVgegrlZANpoDnO3l6DPhQ9OWeaQMoPFcnhDsWjZCItCsrzfHkFCluQT8FhFqblv9q8VIcZCkWZBvC4nIUI3LHJvoPZBv5PT4PrPbKZC2wOJf1J6068Tb8qKoZA1VMLZCyexsieCGdyhorZAvRsvthgZDZD';
        $config         = [
            'secret_key'    => $secret_key,
            'app_id'        => $app_id,
            'page_name'     => $page_name,
            'access_token'  => $access_token,
        ];
        $data = fb_feed($config)->fetch();  
        if(isset($data['status_code']) && $data['status_code'] == 200 && isset($data['data']) && $data['data']){
            foreach($data['data'] as $post){ 
                $get_row = $wpdb->get_row('SELECT ID FROM wp_posts WHERE post_title = "' . $post['id'] . '" && post_type = "social"', 'ARRAY_N'); 
                if(!$get_row && isset($post['message']) && $post['message']){
                    $postData       = array(
                        'post_title'   => $post['id'],
                        'post_content' => $post['message'],
                        'post_status'  => 'publish',
                        'post_excerpt' => '',
                        'post_name'    => $post['id'], 
                        'post_type'    => "social",
                        'post_date'    => date( 'Y-m-d H:i:s', strtotime($post['created_time'])),
                    );             
                    $new_post_id    = wp_insert_post( $postData ); 
                    update_post_meta( $new_post_id, 'type', 'facebook' );
                    update_post_meta( $new_post_id, 'url', $post['permalink_url'] ); 
                    if(isset($post['full_picture']) && $post['full_picture']){
                        if($image       = $post['full_picture']){ 
                            $uploadedfile			= download_url( $image ); 
                            $file_array 			= array(); 
                            preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $image, $matches);
                            if(isset($matches[0]) && $matches[0]){
                                $file_array['name'] 	= basename($matches[0]);
                                $file_array['type']     = wp_check_filetype($matches[0]);
                                $file_array['tmp_name'] = $uploadedfile;  
                                $file_array['error']    = 0;
                                $file_array['size']     = filesize($uploadedfile);
                                $thumbnail_id 			= media_handle_sideload( $file_array, $new_post_id, '' ); 
                                if ( !is_wp_error($thumbnail_id) ) {  
                                    set_post_thumbnail($new_post_id, $thumbnail_id);
                                } 
                            }
                        }
                    }  
                } 
            } 
        }
        $this->custom_facebook_feed_import_duplicites();
    }

}

add_action( 'plugins_loaded', array( new ClearFacebookFeed, 'init' ) );
register_activation_hook( 'clear-facebook-feed/clear-facebook-feed.php', array( new ClearFacebookFeed, 'flush_rules' ) );