<?php

defined('ABSPATH') || exit;

$includes = array(
    'setup.php',
    'extras.php',
    'enqueue.php',
    'acf_theme_options.php',
    'acf_theme_fields.php',
    'menu.php',
    'image_sizes.php',
    'widgets.php',
    'post_types.php',
    'pagination.php',
    'date.php',
    'text.php',
    'cf7.php',
);

if(function_exists('get_field')) {
    //$includes[] = 'mailchimp.php';
}

if(WP_DEBUG != true){
    //$includes[] = 'minify.php';
}

foreach ($includes as $file) {
    require_once get_stylesheet_directory() . '/inc/' . $file;
}

function ddp($object) {
	echo '<pre>';
	var_dump($object);
	echo '<pre>';
	exit;
}

function get_page_by_template($template){
    global $wpdb;
    $page_details = $wpdb->get_var( $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND  meta_value = %s LIMIT 1" , '_wp_page_template', $template) );
	if($page_details){
		return $page_details;
	}
	return;
}

function in_array_multiple($needle = [], $array = []){ 
	if($needle && $array){
		foreach($needle as $value){
			if(isset($array[$value])){
				return true;
			}
		}
	}
	return false;
}

function inside_class($class = []){
    $class[]        = 'selects-inside';
    if (is_post_type_archive('gallery')) {
        $class[]    = 'galery-page';
    } elseif (is_post_type_archive('events')) {
        $class[]    = 'kalendar-page';
    } elseif (is_post_type_archive('projets')) {
        $class[]    = 'project-page';
    } elseif (is_page_template('template/tpl-projects.php')) {
        $class[]    = 'project-page';
    } elseif (is_post_type_archive('teachers')) {
        $class[]    = 'teachers-page';
    } elseif (is_page_template('template/tpl-teachers.php')) {
        $class[]    = 'teachers-page';
    } elseif (is_page_template('template/tpl-classes.php')) {
        $class[]    = 'triedy-page';
    } elseif (is_page_template('template/tpl-classrooms.php')) {
        $class[]    = 'classes-page';
    } elseif (is_page_template('template/tpl-student-council.php')) {
        $class[]    = 'rada-page';
    } elseif (is_page_template('template/tpl-contact.php')) {
        $class[]    = 'kontakt-page';
    } elseif (is_page_template('template/tpl-school.php')) {
        $class[]    = 'school-detail-page';
        $class[]    = get_field('page_color').'-detail';
    } elseif ( is_category() ) {
        $class[]    = 'news-page';
    } elseif ( is_tag() ) {
        $class[]    = '';
    } elseif ( is_author() ) {
        $class[]    = '';
    } elseif ( is_tax() ) {
        $class[]    = '';
    } elseif (is_404()) {
        $class[]    = 'error-page';
    } elseif (is_home() || is_front_page()) {
        $class[]    = 'index-page';
    } elseif ( is_page() ) {
        $class[]    = 'podstranka-b-page';
    } elseif ( is_search() ) {
        $class[]    = 'news-page';
    } elseif ( is_archive() ) {
        $class[]    = 'news-page';
    } elseif ( is_singular('gallery') ) {
        $class[]    = 'photography-page';
    } elseif ( is_singular('projects') ) {
        $class[]    = 'project-detail-a-page';
        $class[]    = get_field('project_color');
    } elseif ( is_singular('teachers') ) {
        $class[]    = 'teachers-detail-page';
    } elseif ( is_singular('classrooms') ) {
        $class[]    = 'classespage-page';
    } elseif ( is_single() ) {
        $class[]    = 'news-page-page';
    } else {
        $class[]    = 'index-page';
    }
    return implode(' ', $class);
}

ini_set( 'mysql.trace_mode', 0 );

function query_filter($args = []){
    $args['tax_query'] = [];
    $args['date_query']= [];
    $args['meta_query']= [];

    // Blog
    $termCurrent = get_queried_object();
    if($termCurrent && isset($termCurrent->taxonomy)):
        $args['tax_query'][] 	= array(
            'taxonomy' => $termCurrent->taxonomy,
            'field'    => 'id',
            'terms'    => $termCurrent->term_id,
        );
    endif;
    if(isset($_GET['date_from']) || isset($_GET['date_to'])):
        if(isset($_GET['date_from']) && $_GET['date_from']):
            $args['date_query'][0]['after']     = date('Y-m-d', strtotime($_GET['date_from']));
        endif;
        if(isset($_GET['date_to']) && $_GET['date_to']):
            $args['date_query'][0]['before']    = date('Y-m-d', strtotime($_GET['date_to']));
        endif;
        $args['date_query'][0]['inclusive']     = true;
    endif;

    // Gallery
    if(isset($_GET['gallery_category']) && $_GET['gallery_category']):
        $args['tax_query'][] 	= array(
            'taxonomy' => 'gallery_category',
            'field'    => 'slug',
            'terms'    => $_GET['gallery_category'],
        );
    endif;
    if(isset($_GET['gallery_year']) && $_GET['gallery_year']):
        $args['tax_query'][] 	= array(
            'taxonomy' => 'gallery_year',
            'field'    => 'slug',
            'terms'    => $_GET['gallery_year'],
        );
    endif;

    // Events
    if(isset($_GET['month']) && $_GET['month']):
        $maxDays = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($_GET['month'])), date('Y', strtotime($_GET['month'])));
        $args['meta_query'] = array(
            'relation'      => 'AND',
            array(
                'key'       => 'start',
                'value'     => date('Y-m-01', strtotime($_GET['month'])),
                'type'      => 'date',
                'compare'   => '>='
            ),
            array(
                'key'       => 'end',
                'value'     => date('Y-m-'.$maxDays, strtotime($_GET['month'])),
                'type'      => 'date',
                'compare'   => '<='
            )
        );
    endif; 

    // Search
    if(isset($_GET['s']) && $_GET['s']):
        $args['s'] = $_GET['s'];
    endif;

    return $args;
}

function get_next_key_array($array, $key){
    $keys       = array_keys($array);
    $position   = array_search($key, $keys);
    $nextKey    = false;
    if (isset($keys[$position + 1])) {
        $nextKey= $keys[$position + 1];
    }
    return $nextKey;
}

function get_previous_key_array($array, $key){
    $keys       = array_keys($array);
    $position   = array_search($key, $keys);
    $prevKey    = false;
    if (isset($keys[$position - 1])) {
        $prevKey= $keys[$position - 1];
    }
    return $prevKey;
}
