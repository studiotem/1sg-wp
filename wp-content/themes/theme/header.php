<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<link rel="schema.dcterms" href="http://purl.org/dc/terms/">
		<meta name="DC.coverage" content="Slovak" />
		<meta name="DC.description" content="<?php echo get_bloginfo ( 'description' );?>" />
		<meta name="DC.format" content="text/html" />
		<meta name="DC.identifier" content="<?php echo get_site_url(); ?>" />
		<meta name="DC.publisher" content="<?php wp_title(' - ', true, 'left'); ?>" />
		<meta name="DC.title" content="<?php wp_title(' - ', true, 'left'); ?>" />
		<meta name="DC.type" content="Text" />  
		<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/print.css" type="text/css" media="print" />
        <meta name="it-rating" content="it-rat-fa9ef9f8c2095cc94c5d33de60ee9bb3">
        <link rel="preload" href="<?php bloginfo('template_directory'); ?>/front/assets/fonts/Poppins-Bold.woff2" as="font" type="font/woff2" crossorigin="anonymous">
        <link rel="preload" href="<?php bloginfo('template_directory'); ?>/front/assets/fonts/Poppins-Regular.woff2" as="font" type="font/woff2" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer">
        <?php wp_head(); ?>
        <!--[if lt IE 9]>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <![endif]-->
		<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<?php echo get_field('google_map', 'option'); ?>"></script>
		<?php if($ga = get_field('google_analytics', 'option')): ?>
			<script async src="https://www.googletagmanager.com/gtag/js?id=<?= $ga; ?>"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());
				gtag('config', '<?= $ga; ?>');
			</script>
		<?php endif; ?>
		<?php if($gtm = get_field('google_tagmanager', 'option')): ?> 
			<script>
				(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','<?= $gtm; ?>');
			</script> 
		<?php endif; ?> 
		<?php if($fapp = get_field('facebook_app_id', 'option')): ?>
		    <script>
				(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = 'https://connect.facebook.net/sk_SK/sdk.js#xfbml=1&version=v3.2&appId=<?= $fapp; ?>&autoLogAppEvents=1';
				fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
		<?php endif; ?>
	</head>
	<body <?php body_class(''); ?>>
		<?php if($gtm): ?> 
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $gtm; ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
		<?php endif; ?>    
      	<div id="fb-root"></div>
        <div class="<?= inside_class(); ?>">
            <div class="wrapper">
                <header class="header">
                    <div class="header__wrap">
                        <div class="container">
                            <div class="header"> 
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"  class="header__logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                                    <?php $logo = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' ); ?>
                                    <?php if ( $logo ) : ?> 
                                        <img class="logo__img" src="<?= esc_url( $logo[0] ); ?>" alt="img"> 
                                    <?php endif; ?>
                                </a>
                                <?= Theme_Desktop_Menu(); ?>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="hide__wrapper"></div>
                <?= Theme_Mobile_Menu(); ?>
                <div class="content">