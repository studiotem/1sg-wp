<?php get_header(); ?>

<div class="news__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title(get_option('page_for_posts')); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')):
                bcn_display();
            endif; ?>
        </ul>
        <?php
        $_referer   = wp_get_referer();
        $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
        ?>
        <div class="hero__back">
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
    </div>
</div>

<div class="news__selects">
    <div class="selects__container">
        <div class="selects__overlay">
            <form class="selects__row" action="" method="get">
                <div class="selects__wrapp">
                    <div class="form__item">
                        <div class="select__close">
                            <svg class="icon icon-close ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#close"></use>
                            </svg>
                        </div>
                        <div class="select__title">
                            <span class="value"><?= __('Čo hľadáte', 'theme'); ?></span>
                        </div>
                        <input class="select__input" name="s" value="<?= the_search_query(); ?>" type="text" placeholder="<?= __('Zadajte frázu...', 'theme'); ?>">
                    </div>
                    <div class="form__wrapp">
                        <div class="form__item form-trigger">
                            <div class="select__close">
                                <svg class="icon icon-close ">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#close"></use>
                                </svg>
                            </div>
                            <div class="select__arrow">
                                <svg class="icon icon-arrow ">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                                </svg>
                            </div>
                            <div class="select__title">
                                <svg class="icon icon-kalendar ">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#kalendar"></use>
                                </svg>
                                <span class="value"><?= __('Dátum od', 'theme'); ?></span>
                            </div>
                            <input class="select__input select__input-date" name="date_from" value="<?= isset($_GET['date_from']) ? $_GET['date_from'] : ''; ?>" type="text" placeholder="<?= __('Vyberte dátum', 'theme'); ?>" id="datepicker-od" autocomplete="off">
                        </div>
                        <div class="form__item form-trigger">
                            <div class="select__close">
                                <svg class="icon icon-close">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#close"></use>
                                </svg>
                            </div>
                            <div class="select__arrow">
                                <svg class="icon icon-arrow">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                                </svg>
                            </div>
                            <div class="select__title">
                                <svg class="icon icon-kalendar">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#kalendar"></use>
                                </svg>
                                <span class="value"><?= __('Dátum do', 'theme'); ?></span>
                            </div>
                            <input class="select__input select__input-date" name="date_to" value="<?= isset($_GET['date_to']) ? $_GET['date_to'] : ''; ?>" type="text" placeholder="<?= __('Vyberte dátum', 'theme'); ?>" id="datepicker-do" autocomplete="off">
                        </div>
                    </div>
                </div>
                <button class="form____search" type="submit">
                    <svg class="icon icon-search ">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#search"></use>
                    </svg>
                    <span><?= __('Hľadať', 'theme'); ?></span>
                </button>
            </form>
            <div class="selects__buttons">
                <div class="buttons__block">
                    <?php $categories   = get_categories(); ?>
                    <?php $termCurrent  = get_queried_object(); ?>
                    <?php foreach($categories as $category): ?>
                        <?php if($category && isset($category->term_id)): ?>
                            <label class="category__label">
                                <a href="<?= get_category_link($category->term_id); ?>">
                                    <input class="checkbox__category" type="checkbox" id="<?= $category->term_id; ?>" <?php if($termCurrent && isset($termCurrent->term_id) && $category->term_id == $termCurrent->term_id): ?>checked="checked"<?php endif; ?>>
                                    <span class="category__inner <?= get_field('category_color', $category); ?>">
                                        <span class="icons">
                                            <svg class="icon icon-done">
                                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#done"></use>
                                            </svg>
                                        </span>
                                        <span class="value"><?= $category->name; ?></span>
                                    </span>
                                </a>
                            </label>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="select__dropdown-trigger" data-open="<?= __('Rozšírené hľadanie', 'theme'); ?>" data-close="<?= __('Skryť rozšírené hľadanie', 'theme'); ?>"><?= __('Rozšírené hľadanie', 'theme'); ?></div>
    </div>
</div>
<div class="news__info">
    <div class="info__container">
        <?php
        $paged	    = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
        $args 	    = array(
            'post_type'	=> 'post',
            'paged'		=> $paged,
        );
        $posts  = new WP_Query( query_filter($args) );
        if ( $posts->have_posts() ) : ?>
            <div class="info__block">
                <?php while ( $posts->have_posts() ) : $posts->the_post();
                    get_template_part( 'template-parts/content-list' );
                endwhile; ?>
            </div>
            <?php echo prev_next_pagination($posts); ?>
        <?php else :
            get_template_part( 'template-parts/content', 'none' );
        endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</div>
 
<?php get_footer(); ?>