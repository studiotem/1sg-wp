<div class="teachers-detail__content">
    <div class="content__container">
        <?php if (has_post_thumbnail() ): ?>
            <div class="content__photo">
                <img class="teacher__img" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" alt="<?= get_the_title();?>">
            </div>
        <?php endif; ?>
        <div class="content__info">
            <div class="teacher__name"><?= get_the_title(); ?></div>
            <div class="teacher__level"><?= get_field('teacher_job'); ?></div>
            <?php if($teacher_email = get_field('teacher_email')): ?>
                <div class="teacher__mail">
                    <div class="mail__icon">
                        <svg class="icon icon-mail">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#mail"></use>
                        </svg>
                    </div>
                    <a class="mail__value" href="mailto:<?= $teacher_email; ?>"><?= $teacher_email; ?></a>
                </div>
            <?php endif; ?>
            <?php if($teacher_phone = get_field('teacher_phone')): ?>
                <div class="teacher__mail">
                    <div class="mail__icon">
                        <svg class="icon icon-mail">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#phone"></use>
                        </svg>
                    </div>
                    <a class="mail__value" href="tel:<?= $teacher_phone; ?>"><?= $teacher_phone; ?></a>
                </div>
            <?php endif; ?>
            <?php if($teacher_other = get_field('teacher_other')): ?>
                <div class="teacher__mail teacher__other">
                    <div class="mail__icon">
                        <svg class="icon icon-mail">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#directory"></use>
                        </svg>
                    </div>
                    <span class="mail__value"><?= $teacher_other; ?></span>
                </div>
            <?php endif; ?>
            <div class="content__text">
                <?= the_content(); ?>
            </div>
        </div>
    </div>
</div>



