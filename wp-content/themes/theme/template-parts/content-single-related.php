<div class="newspage__adds">
    <div class="adds__container">
        <div class="adds__banner">
            <img class="b2__img elem__img" src="<?= get_template_directory_uri(); ?>/front/assets/svg/ng2.svg" alt="Shape">
            <img class="b3__img elem__img" src="<?= get_template_directory_uri(); ?>/front/assets/svg/ng3.svg" alt="Shape">
            <img class="b4__img elem__img" src="<?= get_template_directory_uri(); ?>/front/assets/svg/ng4.svg" alt="Shape">
            <img class="b5__img elem__img" src="<?= get_template_directory_uri(); ?>/front/assets/svg/ng5.svg" alt="Shape">
            <img class="b6__img elem__img" src="<?= get_template_directory_uri(); ?>/front/assets/svg/ng6.svg" alt="Shape">
            <div class="banner__title"><?= __('Páčila sa ti táto novinka?', 'theme'); ?></div>
            <div class="banner__sub"><?= __('Podel sa o ňu s ostatnými', 'theme'); ?></div>
            <div class="banner__social">
                <div class="social__block">
                    <a class="social__item" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>" target="_blank">
                        <svg class="icon icon-facebook">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#facebook"></use>
                        </svg>
                    </a>
                    <a class="social__item" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_the_permalink(); ?>" target="_blank">
                        <svg class="icon icon-linkedin">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#linkedin"></use>
                        </svg>
                    </a>
                    <a class="social__item" href="https://twitter.com/intent/tweet?url=<?= get_the_permalink(); ?>&text=<?= get_the_excerpt(); ?>" target="_blank">
                        <svg class="icon icon-twitter">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#twitter"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
        <?php $args 	= array(
            'post_type' 	=> 'post',
            'posts_per_page'=> 3,
            'post__not_in'  => array(get_the_ID()),
        );
        $posts = new WP_Query( $args );
        if($posts->have_posts()): ?>
            <div class="adds__news">
                <div class="news__title"><?= __('Ďalšie novinky', 'theme'); ?></div>
                <div class="news__block">
                    <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                        <div class="slide__item">
                            <a class="item__icon" href="<?= get_the_permalink(); ?>">
                                <?php if (has_post_thumbnail( get_the_ID() ) ): ?>
                                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), '190x145' ); ?>
                                    <img class="news__img" src="<?php echo $image[0]; ?>" alt="Image"/>
                                <?php endif; ?>
                                <div class="item__day">
                                    <div class="day__number"><?= date_i18n('j', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                                    <div class="day__month"><?= date_i18n('M', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                                    <div class="day__year"><?= date_i18n('Y', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                                </div>
                            </a>
                            <div class="item__info">
                                <?php if($categories = get_the_category( get_the_ID() )): ?>
                                    <div class="item__categories">
                                        <?php foreach($categories as $category): ?>
                                            <a class="item__categoria <?= get_field('category_color', $category); ?>" href="<?= get_term_link($category); ?>">
                                                <?= $category->name; ?>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                                <a class="item__title" href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a>
                                <div class="item__text"><?= get_the_excerpt(); ?></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</div>

