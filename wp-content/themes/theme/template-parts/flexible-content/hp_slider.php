<?php if($template): ?>
    <div class="mainpage__hero">
        <div class="hero__container">
            <?php if($template['content']): ?>
                <div class="hero__slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php foreach($template['content'] as $slider): ?>
                                <div class="swiper-slide" data-color="<?= $slider['color']; ?>" data-fill="<?= $slider['color_bg']; ?>">
                                    <div class="slide__item">
                                        <div class="slide__text">
                                            <div class="slide__info"><?= $slider['subtitle']; ?></div>
                                            <div class="slide__title"><?= $slider['title']; ?></div>
                                            <div class="slide__subtitle"><?= $slider['description']; ?></div>
                                            <?php if($button = $slider['button']): ?>
                                                <a class="slide__link" href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?= $button['title']; ?></a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="slide__img"> 
                                            <img class="hero-one__img" src="<?= $slider['image']; ?>" alt="Slider image"> 
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="swiper__buttons">
                            <div class="swiper-button-prev swiper-button">
                                <svg class="icon icon-arrow">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-next swiper-button">
                                <svg class="icon icon-arrow">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($template['icons']): ?>
                <div class="hero__info">
                    <?php foreach($template['icons'] as $icon): ?>
                        <div class="info__item <?= $icon['color']; ?>">
                            <a class="info__inner" href="<?= $icon['url']; ?>"> 
                                <img class="add__img" src="<?= $icon['bg']; ?>" alt="Icon"> 
                                <div class="info__number"><?= $icon['number']; ?></div>
                                <div class="info__wrapp">
                                    <div class="info__title"><?= $icon['title']; ?></div>
                                    <div class="info__text"><?= $icon['description']; ?></div>
                                    <svg class="icon icon-arrow-link ">
                                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                                    </svg>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
