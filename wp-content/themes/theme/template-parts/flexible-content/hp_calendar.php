<?php if($template):
    $args 	= array(
        'post_type' 	=> 'events',
        'posts_per_page'=> 10,
        'orderby'    	=> 'start',
        'order'      	=> 'asc',
        'meta_query'    => array( 
            array(
                'key'       => 'start',
                'value'     => date('Y-m-01'),
                'type'      => 'date',
                'compare'   => '>='
            ), 
        )
    ); 
    $posts      = new WP_Query( $args );
    $page_id    = get_page_by_template('template/tpl-events.php');
    if($posts->have_posts()): ?>
        <div class="mainpage__info">
            <div class="kalendar__container">
                <div class="info__row">
                    <div class="info__title"><?= $template['title']; ?></div>
                    <div class="swiper__buttons">
                        <div class="swiper-button-prev swiper-button">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                        <div class="swiper-button-next swiper-button">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="kalendar__slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php while ($posts->have_posts()) : $posts->the_post();
                                $start 	= get_field('start');
                                $end 	= get_field('end');
                                $link   = '?month='.date('Y', strtotime($start)).'-'.date('m', strtotime($start)).'-01';
                                ?>  
                                <a class="swiper-slide" href="<?= get_the_permalink($page_id).$link; ?>">
                                    <div class="slide__inner">
                                        <div class="slide__icon">
                                            <svg class="icon icon-kalendar">
                                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#kalendar"></use>
                                            </svg>
                                        </div>
                                        <div class="slide__date">
                                            <?= eventDate($end, $start, false); ?>
                                        </div> 
                                        <div class="slide__text">
                                            <?= get_the_title(); ?>
                                        </div>
                                    </div>
                                </a> 
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <a class="info__btn" href="<?= get_post_type_archive_link('events'); ?>"><?= __('Zobraziť celý kalendár', 'theme'); ?></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
