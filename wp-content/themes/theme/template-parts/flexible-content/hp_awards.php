<?php if($template): ?>
	<?php if($template['content']): ?>
        <div class="mainpage-marks">
            <div class="marks__container">
                <div class="info__row">
                    <div class="marks__title"><?= $template['title']; ?></div>
                    <div class="swiper__buttons">
                        <div class="swiper-button-prev swiper-button">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                        <div class="swiper-button-next swiper-button">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="marks__block marks__slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php foreach($template['content'] as $icon): ?>
                                <div class="swiper-slide block__item">
                                    <div class="slide__inner">
                                        <div class="item__icon">
                                            <img class="marks__img" src="<?= $icon['icon']; ?>" alt="icon">
                                        </div>
                                        <div class="item__title"><?= $icon['title']; ?></div>
                                        <div class="item__subtitle"><?= $icon['subtitle']; ?></div>
                                        <div class="item__text"><?= $icon['description']; ?></div>
                                    </div>   
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>