<?php if($template):
	$args 	= array(
		'post_type' 	=> 'post',
		'posts_per_page'=> 8,
        'orderby'       => 'post_date', 
        'order'         => 'DESC'
	);
    $posts = new WP_Query( $args );
	if($posts->have_posts()): ?>
        <div class="mainpage__news">
            <div class="news__container">
                <div class="news__title"><?= $template['title']; ?></div>
                <div class="news__slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php while ($posts->have_posts()) : $posts->the_post(); ?> 
                                <div class="swiper-slide">
                                    <a class="slide__inner" href="<?= get_the_permalink(); ?>">
                                        <div class="slide__item"> 
                                            <div class="item__icon">
                                                <?php if (has_post_thumbnail( get_the_ID() ) ): ?>
                                                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), '380x290' ); ?>
                                                    <img class="news__img" src="<?php echo $image[0]; ?>" alt="Image"/>
                                                <?php endif; ?>
                                                <div class="item__day">
                                                    <div class="day__number"><?= date_i18n('j', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                                                    <div class="day__month"><?= date_i18n('M', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                                                    <div class="day__year"><?= date_i18n('Y', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                                                </div>
                                            </div>
                                            <div class="item__info">
                                                <?php if($categories = get_the_category( get_the_ID() )): ?>
                                                    <div class="item__categories">
                                                        <?php foreach($categories as $category): ?>
                                                            <div class="item__categoria <?= get_field('category_color', $category); ?>">
                                                                <?= $category->name; ?>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="item__title"><?= get_the_title(); ?></div>
                                                <div class="item__text"><?= get_the_excerpt(); ?></div>
                                            </div>
                                        </div> 
                                    </a>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="swiper__buttons">
                        <div class="swiper-button-prev swiper-button">
                            <svg class="icon icon-arrow">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                        <div class="swiper-button-next swiper-button">
                            <svg class="icon icon-arrow">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <a href="<?= get_post_type_archive_link('post'); ?>" class="news__link"><?= __('Všetky novinky', 'theme'); ?></a>
            </div>
            <img class="quad__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/quad.png" alt="Image"/>
        </div>
 	<?php endif; ?> 
<?php endif; ?>
<?php wp_reset_query(); ?>