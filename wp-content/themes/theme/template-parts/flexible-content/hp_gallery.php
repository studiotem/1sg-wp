<?php if($template): ?>
    <div class="mainpage__info">
        <img class="element__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/element.png" alt="Image"/>
        <div class="info__container">
            <div class="info__row">
                <div class="info__title"><?= $template['title']; ?></div>
                <div class="swiper__buttons">
                    <div class="swiper-button-prev swiper-button">
                        <svg class="icon icon-arrow ">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                        </svg>
                    </div>
                    <div class="swiper-button-next swiper-button">
                        <svg class="icon icon-arrow ">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="info__slider" id="lightgallery">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php if($template['photos']): ?>
                            <?php foreach($template['photos'] as $image): ?>
                                <div class="swiper-slide">
                                    <div class="slide__inner">
                                        <a href="<?= $image['sizes']['large']; ?>" data-fancybox="gallery"> 
                                            <img class="info__img" src="<?= $image['sizes']['350x350']; ?>" alt="<?= $image['alt']; ?>"/>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if($button = $template['button']): ?>
                    <a class="info__btn" href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?= $button['title']; ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
