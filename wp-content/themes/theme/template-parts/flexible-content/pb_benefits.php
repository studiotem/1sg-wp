<?php if($template): ?>
    <div class="posibility__hero">
        <?php if($bg = $template['bg']): ?>
            <img class="elem__img" src="<?= $bg; ?>" alt="img"/>
        <?php endif; ?>
        <div class="hero__container">
            <div class="hero__block">
                <?php if($benefits = $template['benefits']): ?>
                    <?php foreach($benefits as $benefit): ?>
                        <div class="hero__item">
                            <div class="item__icon">
                                <img class="icon icon-hand" src="<?= $benefit['icon']; ?>">
                            </div>
                            <div class="item__text">
                                <div class="item__title"><?= $benefit['title']; ?></div>
                                <div class="item__subtitle"><?= $benefit['desc']; ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="hero__stats">
                <div class="stat__text"><?= $template['content']; ?></div>
            </div>
        </div>
    </div>
<?php endif; ?>