<?php if($template):
	$args 	= array(
		'post_type' 	=> 'classrooms',
		'posts_per_page'=> 8,
	);
    $posts = new WP_Query( $args );
    if($posts->have_posts()): ?>
        <div class="posibility__class">
            <div class="class__container">
                <?php if($bg = $template['bg']): ?>
                    <img class="elem__img" src="<?= $bg; ?>" alt="img">
                <?php endif; ?>
                <div class="class__text">
                    <div class="class__row">
                        <div class="class__title"><?= $template['title']; ?></div>
                        <div class="slider__buttons">
                            <div class="swiper-button-prevp swiper-button">
                                <svg class="icon icon-arrow ">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                                </svg>
                            </div>
                            <div class="swiper-button-nextp swiper-button">
                                <svg class="icon icon-arrow ">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="class__subtitle"><?= $template['desc']; ?></div>
                        <a href="<?= get_post_type_archive_link('classrooms'); ?>" class="class__link"><?= __('Všetky učebne', 'theme'); ?></a>
                    </div>
                    <div class="class__slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                                    <div class="swiper-slide">
                                        <?php if (has_post_thumbnail( get_the_ID() ) ): ?>
                                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), '380x290' ); ?>
                                            <div class="slide__icon">
                                                <a href="<?= get_the_permalink(); ?>">
                                                    <img class="slide__img" src="<?php echo $image[0]; ?>" alt="img"/>
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="slide__info">
                                            <div class="slide__title">
                                                <a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a>
                                            </div>
                                            <div class="slide__text"><?= get_the_excerpt(); ?></div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                    <div class="hide__btn">
                        <a href="<?= get_post_type_archive_link('classrooms'); ?>" class="class__link"><?= __('Všetky učebne', 'theme'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>