<?php if($template): ?>
    <?php $button = $template['button']; ?>
    <div class="school-detail__info">
        <div class="info__container">
            <div class="info__block  <?= $template['bg_color']; ?> <?= $template['position']; ?>">
                <div class="info__item">
                    <div class="item__text">
                        <div class="item__title">
                            <?php if($button): ?><a href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?php endif; ?>
                                <?= $template['title']; ?>
                            <?php if($button): ?></a><?php endif; ?>
                        </div>
                        <div class="item__subtitle"><?= $template['text']; ?></div>
                        <?php if($button): ?>
                            <a class="item__link" href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?= $button['title']; ?></a>
                        <?php endif; ?>
                    </div>
                    <?php if($image = $template['image']): ?>
                        <div class="item__image">
                            <?php if($button): ?><a href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?php endif; ?>
                                <img class="item__img" src="<?= $image['sizes']['large']; ?>" alt="<?= $image['alt']; ?>"/>
                                <?php if($button): ?></a><?php endif; ?> 
                                <?php $key = isset($content_order[$template['acf_fc_layout']]) ? ($content_order[$template['acf_fc_layout']]) : false; ?>
                                <?php if (is_page_template('template/tpl-school.php')):
                                    $color = get_field('page_color');
                                if($key == 1 && $color == 'orange'):
                                   $key = 1;
                                elseif($key == 1 && $color == 'blue'):
                                   $key = 5;
                                elseif($key == 1 && $color == 'green'):
                                   $key = 3;
                                elseif(($key == 2 && $color == 'orange') || ($key == 2 && $color == 'blue') || ($key == 2 && $color == 'green')):
                                   $key = false;
                                elseif($key == 3 && $color == 'orange'):
                                   $key = 2;
                                elseif($key == 3 && $color == 'blue'):
                                   $key = 6;
                                elseif($key == 3 && $color == 'green'):
                                   $key = 4;
                                endif; ?>
                                <?php if($key!=false && file_exists(get_template_directory().'/front/web/img/content/elem'.$key.'.png')): ?>
                                    <img class="elem<?= $key; ?>__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/elem<?= $key; ?>.png" alt="img">
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>