<?php if($template): ?>
    <?php if($template['content']): ?>
        <div class="mainpage__studia">
            <img class="retangle__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/ret1.png" alt="Image"/>
            <div class="studia__container">
                <div class="studia__title"><?= $template['title']; ?></div>
                <div class="studia__info"><?= $template['description']; ?></div>
                <div class="studia__block">
                    <?php foreach($template['content'] as $item): ?>
                        <div class="studia__item">
                            <div class="item__image">
                                <img class="studia-one__img" src="<?= $item['bg']['url']; ?>" alt="<?= $item['bg']['alt']; ?>"/>
                            </div>
                            <div class="item__text">
                                <div class="item__title"><?= $item['title']; ?></div>
                                <?php if($item['conditions']): ?>
                                    <div class="item__stats <?= $item['color']; ?>">
                                        <?php foreach($item['conditions'] as $condition): ?>
                                            <div class="stats__row">
                                                <div class="stat__icon">
                                                    <img src="<?= $condition['icon']; ?>" alt="Icon">
                                                </div>
                                                <div class="stat__text">
                                                    <p><?= $condition['text']; ?></p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                                <div class="item__information">
                                    <p><?= $item['description']; ?></p>
                                </div>
                                <?php if($buttons = $item['buttons']): ?>
                                    <div class="item__buttons">
                                        <?php foreach($buttons as $button): ?>
                                            <a class="item__link <?= $button['style']; ?>" target="<?= ($button['url']['target']) ? $button['url']['target'] : '_self'; ?>" href="<?= $button['url']['url']; ?>">
                                                <?= $button['url']['title']; ?>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>