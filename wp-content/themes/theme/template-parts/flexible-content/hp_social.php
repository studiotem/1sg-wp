<?php if($template):
    $args 	= array(
        'post_type' 	=> 'social',
        'posts_per_page'=> 10,
    );
    $posts = new WP_Query( $args );
    if($posts->have_posts()): ?>
        <div class="mainpage__social">
            <div class="kalendar__container">
                <div class="info__row">
                    <div class="info__title"><?= $template['title']; ?></div>
                    <div class="swiper__buttons">
                        <div class="swiper-button-prev swiper-button">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                        <div class="swiper-button-next swiper-button">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                    </div> 
                    <div class="social_icons">
                        <?php if($youtube = $template['youtube']): ?> 
                            <div class="social__wrapp">
                                <a class="social__item" href="<?= $youtube; ?>" target="_blank">
                                    <div class="item__icon">
                                        <svg class="icon icon-youtube" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"/>
                                        </svg>
                                    </div> 
                                    <span>Youtube</span>
                                </a>
                            </div> 
                        <?php endif; ?>
                        <?php if($instagram = $template['instagram']): ?> 
                            <div class="social__wrapp">
                                <a class="social__item" href="<?= $instagram; ?>" target="_blank">
                                    <div class="item__icon">
                                        <svg class="icon icon-instagram">
                                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#inst"></use>
                                        </svg>
                                    </div> 
                                    <span>Instagram</span>
                                </a>
                            </div> 
                        <?php endif; ?>
                    </div> 
                </div>
                <div class="kalendar__slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                                <?php $fb_url = get_field('url'); ?>
                                <div class="swiper-slide">
                                    <div class="slide__inner">
                                        <div class="slide__icon">
                                            <?php if (has_post_thumbnail( get_the_ID() ) ): ?>
                                                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), '350x350' ); ?> 
                                                <a href="<?= $fb_url; ?>" target="_blank"> 
                                                    <img class="slide__img" src="<?php echo $image[0]; ?>" alt="Image"/>
                                                </a>
                                            <?php endif; ?>
                                            <div class="slide__social">
                                                <?php if(get_field('type') == 'instagram'): ?>
                                                    <img class="social__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/inst.png" alt="Instagram icon"/>
                                                <?php else: ?>
                                                    <img class="social__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/face.png" alt="Facebook icon"/>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="slide__info">
                                            <a href="<?= $fb_url; ?>" target="_blank"> 
                                                <?= get_the_excerpt(); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>