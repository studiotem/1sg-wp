<?php if($template): ?>
    <div class="school-detail__info">
        <div class="info__container">
            <?php if($template['style']  == 'box'): ?>
                <div class="info__banner">
                    <div class="banner__container">
                        <?php if($image = $template['image']): ?>
                            <img class="patern__img" src="<?= $image['url']; ?>" alt="img"/>
                        <?php endif; ?>
                        <div class="banner__icon">
                            <svg class="icon icon-banner">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#banner"></use>
                            </svg>
                        </div>
                        <div class="banner__title"><?= $template['title']; ?></div>
                        <div class="banner__text"><?= $template['text']; ?></div>
                        <?php if($template['type'] == 'form'): ?>
                            <a class="about__link" href="#"><?= $template['form']['title']; ?></a>
                        <?php else: ?>
                            <?php if($button = $template['button']): ?>
                                <a class="banner__link" href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?= $button['title']; ?></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php else: ?>
                <div class="school-detail__about">
                    <?php if($image = $template['image']): ?>
                        <img class="about1__img" src="<?= $image['url']; ?>" alt="img"/>
                    <?php endif; ?>
                    <div class="about__container">
                        <div class="about__icon">
                            <svg class="icon icon-head">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#head"></use>
                            </svg>
                        </div>
                        <div class="about__title"><?= $template['title']; ?></div>
                        <div class="about__subtitle"><?= $template['text']; ?></div>
                        <?php if($template['type'] == 'form'): ?>
                            <a class="about__link about__modal" href="#"><?= $template['form']['title']; ?></a>
                        <?php else: ?>
                            <?php if($button = $template['button']): ?>
                                <a class="about__link" href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?= $button['title']; ?></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if($template['type'] == 'form'): ?>
        <div class="school-detail__modal">
            <div class="modal__container">
                <div class="modal__close">
                    <svg class="icon icon-close">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#close"></use>
                    </svg>
                </div>
                <div class="modal__block">
                    <div class="block__icon">
                        <svg class="icon icon-head">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#head"></use>
                        </svg>
                    </div>
                    <div class="modal__title"><?= $template['form']['form_title']; ?></div>
                    <div class="modal__text"><?= $template['form']['form_text']; ?></div>
                    <?= do_shortcode('[contact-form-7 id="'.$template['form']['form'].'" html_class="form-container modal__form"]'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>