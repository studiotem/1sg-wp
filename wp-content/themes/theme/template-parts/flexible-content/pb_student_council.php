<?php if($template): ?>
    <div class="rada__info">
        <img class="elem__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/elem-rada.png" alt="img"/>
        <div class="rada__container">
            <div class="rada__top">
                <?= $template['content']; ?>
                <?= apply_filters('the_content', get_the_content()); ?>
            </div>
            <?php if($members = $template['members']): ?>
                <?php foreach( $members as $member): ?>
                    <div class="rada__item">
                        <div class="item__top">
                            <?php if($photo = $member['photo']): ?>
                                <div class="top__image">
                                    <img class="person__img" src="<?= $photo['sizes']['thumbnail']; ?>" alt="<?= $photo['alt']; ?>"/>
                                </div>
                            <?php endif; ?>
                            <div class="top__text">
                                <div class="text__name"><?= $member['name']; ?></div>
                                <div class="text__subtitle"><?= $member['job']; ?></div>
                            </div>
                        </div>
                        <div class="item__information">
                            <p><?= nl2br($member['bio']); ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>