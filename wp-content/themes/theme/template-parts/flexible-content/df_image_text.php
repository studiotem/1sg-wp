<?php if($template): ?>
    <?php $button = $template['button']; ?>
    <?php if($template['size']  == 'default'): ?>
        <div class="posibility__info">
            <div class="info__container">
                <div class="info__block <?= $template['bg_color']; ?> <?= $template['position']; ?>">
                    <div class="info__item">
                        <div class="item__text">
                            <div class="item__title">
                                <?php if($button): ?><a href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?php endif; ?>
                                    <?= $template['title']; ?>
                                <?php if($button): ?></a><?php endif; ?>
                            </div>
                            <div class="item__subtitle"><?= $template['text']; ?></div>
                            <?php if($button): ?>
                                <a class="item__link" href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?= $button['title']; ?></a>
                            <?php endif; ?>
                        </div>
                        <?php if($image = $template['image']): ?>
                            <div class="item__icon">
                                <?php if($button): ?><a href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?php endif; ?>
                                    <img class="item__img" src="<?= $image['sizes']['large']; ?>" alt="<?= $image['alt']; ?>"/>
                                <?php if($button): ?></a><?php endif; ?>
                                <?php $key = isset($content_order[$template['acf_fc_layout']]) ? ($content_order[$template['acf_fc_layout']]-1) : false; ?>
                                <?php if($key && file_exists(get_template_directory().'/front/assets/svg/elemp'.$key.'.svg')): ?>
                                    <img class="elem<?= $key; ?>__img" src="<?= get_template_directory_uri(); ?>/front/assets/svg/elemp<?= $key; ?>.svg" alt="img">
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="posibility__adds">
            <div class="adds__container">
                <div class="adds__item <?= $template['position']; ?>">
                    <?php if($image = $template['image']): ?>
                        <div class="item__icon">
                            <?php if($button): ?><a href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?php endif; ?>
                                <img class="item__img" src="<?= $image['sizes']['large']; ?>" alt="<?= $image['alt']; ?>"/>
                            <?php if($button): ?></a><?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div class="item__text">
                        <div class="item__title">
                            <?php if($button): ?><a href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?php endif; ?>
                                <?= $template['title']; ?>
                            <?php if($button): ?></a><?php endif; ?>
                        </div>
                        <div class="item__subtitle"><?= $template['text']; ?></div>
                        <?php if($button): ?>
                            <a class="item__link" href="<?= $button['url']; ?>" target="<?= ($button['target']) ? $button['target'] : '_self'; ?>"><?= $button['title']; ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>