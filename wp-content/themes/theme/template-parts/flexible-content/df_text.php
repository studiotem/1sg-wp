<?php if($template): ?> 
	<?php if($template['style'] == 'centered'): ?> 
		<div class="content-box-text text-center padding-top-44 padding-bottom-74 padding-sm-bottom-20">
			<div class="container"> 
				<div class="process-text-01">
					<?= $template['text']; ?>
				</div>
			</div>
		</div> 
	<?php else: ?>
		<div class="blog-listing fullwidth">
			<div class="container">
				<div class="content-box-01 blog-post__text">
					<?= $template['text']; ?>
				</div> 
			</div>
		</div> 
	<?php endif; ?> 
<?php endif; ?>