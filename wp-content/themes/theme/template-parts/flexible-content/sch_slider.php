<?php if($template): ?>
    <div class="school-detail__hero">
        <?php if( $bg = $template['bg'] ): ?>
            <img class="patern__img" src="<?= $bg; ?>" alt="Background"/>
        <?php endif; ?>
        <div class="hero__container">
            <div class="hero__mobile-info">
                <div class="hero__top">
                    <ul class="breadcrumbs">
                        <?php if(function_exists('bcn_display')):
                            bcn_display();
                        endif; ?>
                    </ul>
                    <div class="hero__title"><?= $template['title']; ?></div>
                    <div class="hero__subtitle"><?= $template['subtitle']; ?></div>
                </div>
            </div>
            <?php if( $image = $template['image'] ): ?>
                <div class="hero__image">
                    <img class="hero-s1__img" src="<?= $image; ?>" alt="Image"/>
                </div>
            <?php endif; ?>
            <div class="hero__text">
                <div class="hero__top">
                    <ul class="breadcrumbs">
                        <?php if(function_exists('bcn_display')):
                            bcn_display();
                        endif; ?>
                    </ul>
                    <div class="hero__title"><?= $template['title']; ?></div>
                    <div class="hero__subtitle"><?= $template['subtitle']; ?></div>
                </div>
                <?php if( $benefits = $template['benefits'] ): ?>
                    <div class="hero__stats">
                        <?php foreach( $benefits as $benefit ): ?>
                            <div class="stat__row">
                                <div class="stat__icon">
                                    <img class="icon" src="<?= $benefit['icon']; ?>">
                                </div>
                                <div class="stat__info">
                                    <div class="info__title"><?= $benefit['title']; ?></div>
                                    <div class="info__text"><?= $benefit['desc']; ?></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
