<div class="block__item">
    <div class="item__icon">
        <svg class="icon icon-kalendar ">
            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#kalendar"></use>
        </svg>
    </div>
    <div class="item__text">
        <h3><?= get_the_title();?></h3>
        <?= the_content(); ?>
    </div>
</div>