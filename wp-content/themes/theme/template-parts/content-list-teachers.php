<div class="teacher__item">
    <a class="item__inner" href="<?= get_the_permalink(); ?>"> 
        <div class="teacher__icon"> 
            <?php if (has_post_thumbnail() ): ?>
                <img class="teacher__img" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'thumbnail'); ?>" alt="<?= get_the_title();?>">
            <?php endif; ?>
        </div>
        <div class="teacher__text"> 
            <div class="teacher__name"><?= get_the_title(); ?></div>
            <div class="teacher__status"><?= get_field('teacher_job'); ?></div>
        </div>
    </a>
</div> 