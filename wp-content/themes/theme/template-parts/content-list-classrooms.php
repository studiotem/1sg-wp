<div class="classes__item">
    <div class="classes__inner">
        <a class="item__photo" href="<?= get_the_permalink(); ?>">
            <?php if (has_post_thumbnail() ): ?>
                <img class="class__img" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), '380x290'); ?>" alt="<?= get_the_title();?>">
            <?php endif; ?>
        </a>
        <div class="classes__texts">
            <a class="item__title" href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a>
            <div class="item__text"><?= get_the_excerpt(); ?></div>
        </div>
    </div>
</div>