<a class="block__item <?= get_field('project_color'); ?>" href="<?= get_the_permalink(); ?>">
    <?php if (has_post_thumbnail() ): ?>
        <img class="patern" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" alt="<?= get_the_title();?>">
    <?php endif; ?>
    <?php if($categories = get_the_terms( get_the_ID(), 'projects_category' )): ?>
        <div class="block__icons">
            <?php foreach($categories as $category): ?>
                <div class="item__icon <?= get_field('color', $category); ?>">
                    <?= get_field('acronym', $category); ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="block__title"><?= __('Projekty', 'theme'); ?></div>
    <div class="block__name"><?= get_the_title(); ?></div>
    <div class="block__info"><?= get_field('short_description'); ?></div>
    <svg class="icon icon-arrow-link">
        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
    </svg>
</a>