<div class="slide__wrapp">
    <div class="slide__item">
        <a class="item__icon" href="<?php the_permalink(); ?>">
            <?php if (has_post_thumbnail() ): ?>
                <img class="news__img" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), '190x145'); ?>" alt="<?= get_the_title();?>">
            <?php endif; ?>
            <div class="item__day">
                <div class="day__number"><?= date_i18n('j', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                <div class="day__month"><?= date_i18n('M', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
                <div class="day__year"><?= date_i18n('Y', strtotime(get_the_date('Y-m-d H:i:s', get_the_ID()))); ?></div>
            </div>
        </a>
        <div class="item__info">
            <?php if($categories = get_the_category( get_the_ID() )): ?>
                <div class="item__categories">
                    <?php foreach($categories as $category): ?>
                        <a class="item__categoria <?= get_field('category_color', $category); ?>" href="<?= get_term_link($category); ?>">
                            <?= $category->name; ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <a class="item__title" href="<?php the_permalink(); ?>"><?= get_the_title();?></a>
            <div class="item__text"><?= get_the_excerpt(); ?></div>
        </div>
    </div>
</div>