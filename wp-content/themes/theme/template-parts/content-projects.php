<div class="swiper-slide">
    <a class="slide__inner" href="<?= get_the_permalink(); ?>">
        <div class="slide__photo">
            <?php if (has_post_thumbnail() ): ?>
                <img class="slide__img" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" alt="<?= get_the_title();?>">
            <?php endif; ?>
            <?php if($categories = get_the_terms( get_the_ID(), 'projects_category' )): ?>
                <?php foreach($categories as $category): ?>
                    <div class="item__category <?= get_field('color', $category); ?>">
                        <?= get_field('acronym', $category); ?>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="item__topic"><?= get_field('project_year'); ?></div>
        </div>
        <div class="slide__text">
            <div class="slide__top"><?= get_field('project_subtitle'); ?></div>
            <div class="slide__title">
                <h3><?= get_the_title(); ?></h3>
                <?= get_field('short_description'); ?>
            </div>
        </div>
    </a>
</div> 