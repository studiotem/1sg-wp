<div class="newspage__info">
    <div class="info__container">
        <?php if($categories = get_the_category( get_the_ID() )): ?>
            <div class="info__category">
                <?php foreach($categories as $category): ?>
                    <a class="category__item <?= get_field('category_color', $category); ?>" href="<?= get_term_link($category); ?>">
                        <?= $category->name; ?>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?> 
        <div class="info__block">
            <?= str_replace(['<blockquote>', '</blockquote>'], ['<div class="block__top">', '</div>'], apply_filters('the_content', get_the_content())); ?>
        </div>
        <?php if($blog_links = get_field('blog_links')): ?>
            <div class="info__links">
                <?php foreach($blog_links as $blog_link): ?>
                <a class="info__item" href="<?= $blog_link['url']['url']; ?>" target="<?= ($blog_link['url']['target']) ? $blog_link['url']['target'] : '_self'; ?>">
                    <div class="item__icon">
                        <svg class="icon icon-claim">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#claim"></use>
                        </svg>
                    </div>
                    <div class="item__text"><?= $blog_link['url']['title']; ?></div>
                    <div class="item__arrow">
                        <svg class="icon icon-arrow-link ">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                        </svg>
                    </div>
                </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if($blog_gallery = get_field('blog_gallery')): ?>
            <div class="info__gallery">
                <div class="gallery__title"><?= __('Fotogaléria', 'theme'); ?></div>
                <div class="gallery__block" id="lightgallery">
                    <?php foreach($blog_gallery as $blog_photo): ?>
                        <a class="gallery__item" href="<?= $blog_photo['sizes']['large']; ?>" data-fancybox="gallery">
                            <img class="gallery__img" src="<?= $blog_photo['sizes']['350x350']; ?>" alt="<?= $blog_photo['alt']; ?>"/>
                        </a> 
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>