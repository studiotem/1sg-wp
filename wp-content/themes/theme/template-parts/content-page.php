<?php 


$page_folders   = get_field('page_folders');
$page_links     = get_field('page_links');
$position       = get_field('position');
 
?>
<?php if($position == 'top' && ( $page_folders || $page_links ) ): ?>
    <div class="podstranka-b__info pb-0">
        <div class="info__container"> 
            <?php if($page_folders): ?>
                <div class="info__directory mb-0">
                    <?php foreach($page_folders as $page_folder): ?>
                        <div class="directory__item">
                            <a class="item__inner" href="<?= $page_folder['url']['url']; ?>" target="<?= $page_folder['url']['target']; ?>">
                                <div class="item__icon">
                                    <svg class="icon icon-directory">
                                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#directory"></use>
                                    </svg>
                                </div>
                                <div class="item__text"><?= $page_folder['url']['title']; ?></div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div> 
            <?php endif; ?>
            <?php if($page_links): ?>
                <div class="info__files">
                    <?php foreach($page_links as $page_link): ?>
                        <a class="info__item" href="<?= $page_link['url']['url']; ?>" target="<?= ($page_link['url']['target']) ? $page_link['url']['target'] : '_self'; ?>">
                            <div class="item__icon">
                                <svg class="icon icon-claim">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#claim"></use>
                                </svg>
                            </div>
                            <div class="item__text"><?= $page_link['url']['title']; ?></div>
                            <div class="item__arrow">
                                <svg class="icon icon-arrow-link">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                                </svg>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

<div class="podstranka-b__blog">
    <div class="blog__container">
        <?php if (has_post_thumbnail() ): ?>
            <div class="blog__image">
                <img class="blog__img" src="<?= get_the_post_thumbnail_url(get_the_ID(), '1120x380'); ?>" alt="<?= get_the_title();?>"/>
            </div>
        <?php endif; ?>
        <div class="blog__text <?php if(get_field('content_scroll')): echo 'students-table-wrapp'; endif; ?>">
            <?= str_replace(['<blockquote>', '</blockquote>'], ['<div class="block__top">', '</div>'], apply_filters('the_content', get_the_content())); ?>
        </div>
    </div>
</div>

<?php if( $position != 'top' && ( $page_folders || $page_links ) ): ?>
    <div class="podstranka-b__info">
        <div class="info__container">
            <?php if($page_folders): ?>
                <div class="info__directory">
                    <?php foreach($page_folders as $page_folder): ?>
                        <div class="directory__item">
                            <a class="item__inner" href="<?= $page_folder['url']['url']; ?>" target="<?= ($page_folder['url']['target']) ? $page_folder['url']['target'] : '_self'; ?>">
                                <div class="item__icon">
                                    <svg class="icon icon-directory">
                                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#directory"></use>
                                    </svg>
                                </div>
                                <div class="item__text"><?= $page_folder['url']['title']; ?></div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if($page_links): ?>
                <div class="info__files">
                    <?php foreach($page_links as $page_link): ?>
                        <a class="info__item" href="<?= $page_link['url']['url']; ?>" target="<?= ($page_link['url']['target']) ? $page_link['url']['target'] : '_self'; ?>">
                            <div class="item__icon">
                                <svg class="icon icon-claim">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#claim"></use>
                                </svg>
                            </div>
                            <div class="item__text"><?= $page_link['url']['title']; ?></div>
                            <div class="item__arrow">
                                <svg class="icon icon-arrow-link">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                                </svg>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>