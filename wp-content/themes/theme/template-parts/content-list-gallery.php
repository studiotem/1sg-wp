<div class="block__item">
    <div class="item__inner">
        <div class="gallery__photo">
            <?php if (has_post_thumbnail() ): ?> 
                <a href="<?php the_permalink(); ?>">
                    <img class="gallery__img" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), '350x350'); ?>" alt="<?= get_the_title();?>">
                </a>
            <?php endif; ?>
            <?php if($categories = get_the_terms( get_the_ID(), 'gallery_category' )): ?>
                <div class="gallery__category">
                    <?php foreach($categories as $category): ?>
                        <a class="category__item <?= get_field('category_color', $category); ?>" href="?gallery_category=<?= $category->slug; ?>&gallery_year=<?= (isset($_GET['gallery_year']) ? $_GET['gallery_year'] : ''); ?>">
                            <?= $category->name; ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="galery__info">
            <a class="item__title" href="<?php the_permalink(); ?>"><?= get_the_title();?></a>
            <div class="item__date"><?= get_the_date();?></div>
        </div>
    </div>
</div>