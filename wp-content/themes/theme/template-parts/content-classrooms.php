<div class="classes-page__content">
    <div class="content__container">
        <?php if (has_post_thumbnail() ): ?>
            <div class="content__photo">
                <img class="content__img" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" alt="<?= get_the_title();?>">
            </div>
        <?php endif; ?>
        <div class="content__text">
            <?= the_content(); ?>
       </div>
    </div>
</div>

<?php if($images = get_field('gallery_photos')): ?>
    <div class="class-page__info">
        <div class="info__container">
            <div class="info__block" id="lightgallery">
                <?php foreach($images as $image): ?>
                    <a class="block__item" href="<?= $image['sizes']['large']; ?>" data-fancybox="gallery">
                        <div class="item__iamge">
                            <img class="photo__img" src="<?= $image['sizes']['350x350']; ?>" alt="<?= $image['alt']; ?>"/>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>