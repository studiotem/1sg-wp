<?php get_header(); ?>

<?php $page_id = get_page_by_template('template/tpl-teachers.php'); ?>
<div class="teachers__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title($page_id); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </ul>
        <div class="hero__back">
            <?php
            $_referer   = wp_get_referer();
            $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
            ?>
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
    </div>
</div>

<?php get_template_part( 'template-parts/content', 'teachers' ); ?>

<?php get_footer(); ?> 