<?php get_header(); ?>

<div class="podstarnka-b__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title(); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')):
                bcn_display();
            endif; ?>
        </ul>
        <div class="hero__back">
            <?php
            $_referer   = wp_get_referer();
            $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
            ?>
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
            <?php if(get_field('share')): ?>
                <div class="hero__social">
                    <div class="social__block">
                        <a class="social__item" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>" target="_blank">
                            <svg class="icon icon-facebook">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#facebook"></use>
                            </svg>
                        </a>
                        <a class="social__item" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_the_permalink(); ?>" target="_blank">
                            <svg class="icon icon-linkedin">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#linkedin"></use>
                            </svg>
                        </a>
                        <a class="social__item" href="https://twitter.com/intent/tweet?url=<?= get_the_permalink(); ?>&text=<?= get_the_excerpt(); ?>" target="_blank">
                            <svg class="icon icon-twitter">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#twitter"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="social__text"><?= __('Zdieľať', 'theme'); ?></div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php
$content = get_field('content');
if ($content):
    $content_order = [];
    foreach ($content as $key => $template):
        if(!isset($content_order[$template['acf_fc_layout']])){
            $content_order[$template['acf_fc_layout']] = 0;
        }
        $content_order[$template['acf_fc_layout']]++;
        include get_template_directory() . '/template-parts/flexible-content/' . $template['acf_fc_layout'] . '.php';
    endforeach;
else:
    while ( have_posts() ) : the_post();
        get_template_part( 'template-parts/content', 'page' );
    endwhile;
endif; ?>

<?php get_footer(); ?>