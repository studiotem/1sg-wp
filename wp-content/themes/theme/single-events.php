<?php get_header(); ?>
<div class="newspage__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title(); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')):
                bcn_display();
            endif; ?>
        </ul>
        <div class="hero__block">
            <?php if (has_post_thumbnail() ): ?>
                <div class="hero__image">
                    <img class="hero__img" src="<?= get_the_post_thumbnail_url(get_the_ID(), '1120x380'); ?>" alt="<?= get_the_title();?>"/>
                </div>
            <?php endif; ?>
            <div class="hero__back">
                <?php
                $_referer   = wp_get_referer();
                $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
                ?>
                <a class="back__btn" href="<?= $referer; ?>">
                    <svg class="icon icon-arrow-link ">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                    </svg>
                    <span><?= __('Návrat', 'theme'); ?></span>
                </a>
                <div class="hero__social">
                    <div class="social__block">
                        <a class="social__item" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>" target="_blank">
                            <svg class="icon icon-facebook">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#facebook"></use>
                            </svg>
                        </a>
                        <a class="social__item" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_the_permalink(); ?>" target="_blank">
                            <svg class="icon icon-linkedin">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#linkedin"></use>
                            </svg>
                        </a>
                        <a class="social__item" href="https://twitter.com/intent/tweet?url=<?= get_the_permalink(); ?>&text=<?= get_the_excerpt(); ?>" target="_blank">
                            <svg class="icon icon-twitter">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#twitter"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="social__text"><?= __('Zdieľať', 'theme'); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php while ( have_posts() ) : the_post();
    get_template_part( 'template-parts/content', 'single-events' );
endwhile; ?>
<?php wp_reset_query(); ?>
 
<?php get_footer(); ?>