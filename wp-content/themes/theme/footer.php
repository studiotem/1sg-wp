                </div>
                <footer class="footer footer__wrap">
                    <div class="container">
                        <div class="footer__blocks">
                            <div class="footer__block">
                                <?php dynamic_sidebar( 'footer-1' ); ?>
                            </div>
                            <div class="footer__block">
                                <?php dynamic_sidebar( 'footer-2' ); ?>
                            </div>
                            <div class="footer__block">
                                <?php dynamic_sidebar( 'footer-3' ); ?>
                            </div>
                            <div class="footer__block">
                                <?php dynamic_sidebar( 'footer-4' ); ?>
                            </div>
                        </div>
                        <div class="footer__social">
                            <?php if($social_networks = get_field('social_networks', 'option')): ?>
                                <?php foreach($social_networks as $social_network): ?>
                                    <div class="social__wrapp">
                                        <a class="social__item" href="<?= $social_network['url']; ?>" target="_blank">
                                            <div class="item__icon">
                                                <svg class="icon icon-<?= $social_network['network']; ?>">
                                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#<?= $social_network['network']; ?>"></use>
                                                </svg>
                                            </div>
                                            <span><?= $social_network['title']; ?></span>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer__bottom">
                            <div class="footer__buttons">
                                <?php if($footer_login_teacher = get_field('footer_login_teacher', 'option')): ?>
                                    <a class="footer__btn" href="<?= $footer_login_teacher['url']; ?>" target="<?= ($footer_login_teacher['target']) ? $footer_login_teacher['target'] : '_self'; ?>">
                                        <svg class="icon icon-glasses">
                                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#glasses"></use>
                                        </svg>
                                        <span><?= $footer_login_teacher['title']; ?></span>
                                    </a>
                                <?php endif; ?>
                                <?php if($footer_login_student = get_field('footer_login_student', 'option')): ?>
                                    <a class="footer__btn" href="<?= $footer_login_student['url']; ?>" target="<?= ($footer_login_student['target']) ? $footer_login_student['target'] : '_self'; ?>">
                                        <svg class="icon icon-cap ">
                                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#cap"></use>
                                        </svg>
                                        <span><?= $footer_login_student['title']; ?></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="footer__copy">
                                <?= (get_bloginfo( 'description' )) ? get_bloginfo( 'description' ) . '  |  ' : ''; ?>&copy; <?= date( 'Y' ); ?> <br> Web na mieru od <a href="https://studiotem.com/" target="_blank" style="
    color: #fff;
    font-weight: 700;
">Studio TEM</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
		<?php wp_footer(); ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    </body>
</html> 