<?php

if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
    function wpex_mce_text_sizes( $initArray ){
        $initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 21px 22px 24px 28px 32px 36px";
        return $initArray;
    }
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );

function wpdocs_custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function get_excerpt($excerpt, $number='100'){
	$excerpt 	= strip_shortcodes($excerpt);
	$excerpt 	= strip_tags($excerpt);
	$_excerpt 	= $excerpt;
	if(mb_strlen($_excerpt)>=$number){
		$excerpt    = substr($excerpt, 0, $number);
		$excerpt    = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt    = $excerpt.'...';
	}
	return $excerpt;
}

function text_style($subject, $search, $class){
	return str_replace($search, '<span class="'.$class.'">'.$search.'</span>', $subject);
}

function truncate($text, $chars = 25) {
    if (strlen($text) <= $chars) {
        return $text;
    }
    $text = $text." ";
    $text = mb_substr($text, 0, $chars);
    $text = mb_substr($text, 0, strrpos($text,' '));
    $text = $text."...";
    return $text;
}

