<?php

function pagination($wp_query='', $big='999999999'){
	if($wp_query){
		$default_posts_per_page = $wp_query->max_num_pages;
	}else{
		global $wp_query;
		$default_posts_per_page = get_option( 'posts_per_page' );
	}
	$pages = paginate_links( array(
		'base' 			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' 		=> '?paged=%#%',
		'current' 		=> max( 1, get_query_var('paged') ),
		'total' 		=> $default_posts_per_page,
		'end_size'      => 1,
		'mid_size'      => 2,
		'prev_next'     => true,
		'prev_text'     => '<i class="fa fa-angle-left"></i>',
		'next_text'     => '<i class="fa fa-angle-right"></i>',
		'type'          => 'array',
		'add_args'      => false,
		'add_fragment'  => '',
	) );
	if( is_array( $pages ) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
	    	echo '<ul class="pagination-list">';
				foreach ( $pages as $page ) {
					echo '<li>'.$page.'</li>';
				}
			echo '</ul>';
	}
	wp_reset_postdata();
}

function prev_next_pagination($query = []){

    $next_link  = get_next_posts_link( '[link]', $query->max_num_pages );
    $prev_link  = get_previous_posts_link( '[link]', $query->max_num_pages );

    $next_class = ($next_link) ? '' : 'disable';
    $prev_class = ($prev_link) ? '' : 'disable';

    echo '<div class="info__pagination pagination-custom">';

    $prev  = '';
    $prev .= '<button class="pagination__button pagination-prev '.$prev_class.'" type="button">';
    $prev .= '<svg class="icon icon-arrow"><use href="'.get_template_directory_uri().'/front/assets/icon/symbol/sprite.svg#arrow"></use></svg>';
    $prev .= '</button>';
    if($prev_link){
        $prev = str_replace('[link]', $prev, $prev_link) ;
    }
    echo $prev;

    $next  = '';
    $next .= '<button class="pagination__button pagination-next '.$next_class.'" type="button">';
    $next .= '<svg class="icon icon-arrow"><use href="'.get_template_directory_uri().'/front/assets/icon/symbol/sprite.svg#arrow"></use></svg>';
    $next .= '</button>';
    if($next_link){
        $next = str_replace('[link]', $next, $next_link) ;
    }
    echo $next;

    echo '</div>';

}