<?php

add_filter('acf/settings/google_api_key', function () {
    return get_field('google_map', 'option');
});

function theme_acf_init() {
    acf_update_setting('google_api_key', get_field('google_map', 'option'));
}
add_action('acf/init', 'theme_acf_init');

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Šablóna',
        'menu_title'	=> 'Šablóna',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false,
        'position'		=> '50',
        'icon_url'		=> 'dashicons-screenoptions'
    ));
}
