<?php

function theme_widgets_init() {
    register_sidebar( array(
        'name'          => 'Footer column n.1',
        'id'            => 'footer-1',
        'before_widget' => '<aside id="%1$s" class="widget widget_store_links %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => 'Footer column n.2',
        'id'            => 'footer-2',
        'before_widget' => '<aside id="%1$s" class="widget widget_store_links %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => 'Footer column n.3',
        'id'            => 'footer-3',
        'before_widget' => '<aside id="%1$s" class="widget widget_store_links %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => 'Footer column n.4',
        'id'            => 'footer-4',
        'before_widget' => '<aside id="%1$s" class="widget widget_store_links %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'theme_widgets_init' );

function wpb_load_widget() {  
    register_widget( 'Theme_Menu' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

class Theme_Menu extends WP_Widget
{ 

	function __construct()
	{
		parent::__construct( 'Theme_Menu', esc_html__('Theme menu', 'theme'));
	}
 
	function widget($args, $instance)
	{
		extract( $args );
        $title      = apply_filters( 'widget_title', $instance['title'] );
        $menu_array = wp_get_nav_menu_items( $instance['menu'], array( 'order' => 'DESC' ) );
        if($menu_array): ?>
            <div class="block__title">
                <div class="icon-title">
                    <svg class="icon icon-arrow">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                    </svg>
                </div>
                <span><?= $title; ?></span>
            </div>
            <div class="block__links">
                <?php foreach($menu_array as $menu_item): ?>
                    <a class="footer__link" href="<?= $menu_item->url; ?>"><?= $menu_item->title; ?></a>
                <?php endforeach; ?>
            </div>
        <?php endif;
	}
	 
	function update($new_instance, $old_instance)
	{
		$instance           = $old_instance;
        $instance['title']  = strip_tags($new_instance['title']);
        $instance['menu']   = strip_tags($new_instance['menu']);
        return $instance;
	}
 
	function form($instance)
	{
        $title      = isset($instance['title'])	? esc_attr($instance['title']) : '';
		$menu       = isset($instance['menu'])	? esc_attr($instance['menu']) : '';
		$wp_menus   = wp_get_nav_menus();
		?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'theme'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('menu')); ?>"><?php esc_html_e('Menu:', 'theme'); ?></label>
            <select class="widefat" id="<?php echo esc_attr($this->get_field_id('menu')); ?>" name="<?php echo esc_attr($this->get_field_name('menu')); ?>">
                <?php if($wp_menus): ?>
                    <?php foreach($wp_menus as $wp_menu): ?>
                        <option <?php if($wp_menu->term_id == $menu) echo 'selected="selected"'; ?> value="<?= $wp_menu->term_id; ?>"><?= $wp_menu->name; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </p>
    <?php }

}
