<?php

/************************************************************************************/
/* Images */
/************************************************************************************/

add_image_size( '1120x380', 1120, 380, true );
add_image_size( '190x145', 190, 145, true );
add_image_size( '350x350', 350, 350, true );
add_image_size( '255x190', 255, 190, true );
add_image_size( '380x290', 380, 290, true );
add_image_size( 'half', 101, 101 );

function half_image_resize_dimensions( $payload, $orig_w, $orig_h, $dest_w, $dest_h, $crop ){
    if($dest_w === 101){
        $width	= $orig_w/2;
        $height = $orig_h/2;
        return array( 0, 0, 0, 0, $width, $height, $orig_w, $orig_h );
    } else {
        return $payload;
    }
}
add_filter( 'image_resize_dimensions', 'half_image_resize_dimensions', 10, 6 );

function wp_get_attachment_metadata_mine($data) {
	$res = $data;
	if (!isset($data['width']) || !isset($data['height'])) {
		$res = false;
	}
	return $res;
}
add_filter( 'wp_get_attachment_metadata' , 'wp_get_attachment_metadata_mine' );
 
function add_custom_video_field_to_attachment_fields_to_edit( $form_fields, $post ) {
    $video_field = get_post_meta($post->ID, 'video_field', true);
    $form_fields['video_field'] = array(
        'label' => 'Video link',
        'input' => 'text', 
        'value' => $video_field,
        'helps' => ''
    );
    return $form_fields;
}
add_filter('attachment_fields_to_edit', 'add_custom_video_field_to_attachment_fields_to_edit', null, 2); 
 
function save_custom_text_attachment_field($post, $attachment) {  
    if( isset($attachment['video_field']) ){  
        update_post_meta($post['ID'], 'video_field', sanitize_text_field( $attachment['video_field'] ) );  
    }else{
        delete_post_meta($post['ID'], 'video_field' );
    }
    return $post;  
}
add_filter('attachment_fields_to_save', 'save_custom_text_attachment_field', null, 2);

 