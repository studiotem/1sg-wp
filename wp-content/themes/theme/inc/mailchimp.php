<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
 
/**
 * Helper functions
 *
 * @package   InsightFramework
 */
if ( ! class_exists( 'Theme_Mailchimp' ) ) {

	class Theme_Mailchimp {

		private $api_key 		= "";
		private $datacenter 	= "";
		private $api_version 	= "";
		private $api_endpoint 	= "";
		private $format 		= "";
		private $verify_ssl 	= false;
		private $user_agent 	= "";
		private $debug 			= false;

		function __construct() {
			$api_key       = get_field( 'mailchimp_api_key', 'option' );
			$this->api_key = $api_key;
			if ( strlen( substr( strrchr( $api_key, '-' ), 1 ) ) ) {
				$this->datacenter = substr( strrchr( $api_key, '-' ), 1 );
			} else {
				$this->datacenter = "us1";
			}
			$this->api_version  = "2.0";
			$this->api_endpoint = "https://" . $this->datacenter . ".api.mailchimp.com/" . $this->api_version . "/";
			$this->format       = "json";
			$this->verify_ssl   = false;
			$this->user_agent   = "ThemeMove-Mailing/1.0";

			add_action( 'wp_ajax_nopriv_theme_ajax_subscribe',
				array(
					&$this,
					'subscribe_to_list',
				) );
			add_action( 'wp_ajax_theme_ajax_subscribe',
				array(
					&$this,
					'subscribe_to_list',
				) );
		}

		/**
		 *
		 *    Runs API query
		 *
		 * @param $query - string - endpoint and parameters. like "lists/subscribe"
		 * @param $data  - array - the data which'll be posted. must NOT be json decoded!
		 * @param $data  - array - optional - curl query timeout in seconds
		 *
		 * @return array - API response
		 */
		private function rest( $query, $data = array(), $timeout = 10 ) {
			if ( ! function_exists( 'curl_init' ) ) {
				$result = array(
					'status' => true,
					'name'   => 'curl_package_disabled',
				);

				return $result;
			}

			$url 			= $this->api_endpoint . $query . "." . $this->format; 
			$data['apikey'] = $this->api_key;
			$header			= array();
			$header[] 		= "Content-type: application/json"; 
			$ch 			= curl_init();
			
			curl_setopt( $ch, CURLOPT_URL, $url ); 
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
			 
			curl_setopt( $ch, CURLOPT_POST, true );  
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl );
			curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HEADER, false );
			curl_setopt( $ch, CURLOPT_USERAGENT, $this->user_agent );

			$result = curl_exec( $ch );

			if ( $this->debug ) {
				echo "<pre>result:<br>";
				print_r( json_decode( $result, true ) );
				echo "<br>curlinfo:<br>";
				print_r( curl_getinfo( $ch ) );
				echo "</pre>";
			}

			curl_close( $ch );

			return json_decode( $result, true );
		}

		/**
		 *
		 *    Subscribes the email address to the given list
		 *
		 * @param    $email   - string
		 * @param    $list_id - string - sth like "6edd80a499"
		 * @param    $optin   - boolean - optional
		 *
		 * @return array
		 */
		public function subscribe( $email, $optin = true ) {
			$list_id= get_field( 'mailchimp_audience_id', 'option' );
			$data 	= array(
				"id" 		=> $list_id,
				"email"		=> array(
					"email" => $email,
				),
				"merge_vars"=> array(
					"FNAME" => "",
					"LNAME" => "",
				),
				"double_optin" 	=> $optin,
				"send_welcome" 	=> $optin,
				"status" 		=> "subscribed", 
			);

			return $this->rest( "lists/subscribe", $data, 20 );
		}

		/**
		 *
		 *    Unsubscribes the email address to the given list
		 *
		 * @param    $email   - string 
		 * @param    $optout  - boolean - optional
		 *
		 * @return    $result - array
		 */
		public function unsubscribe( $email, $optout = false ) {
			$list_id= get_field( 'mailchimp_audience_id', 'option' );
			$data 	= array(
				"id"           	=> $list_id,
				"email"        	=> array(
					"email" 	=> $email,
				),
				"send_goodbye" 	=> $optout,
				"send_notify"  	=> $optout,
			);

			return $this->rest( "lists/unsubscribe", $data );
		}

		/**
		 *
		 *    Lists lists o_O 
		 *
		 * @return array $result
		 */
		public function get_lists( ) {
			$list_id= get_field( 'mailchimp_audience_id', 'option' );
			$data 	= array(
				"filters" 		=> array(
					"list_id" 	=> $list_id,
				),
			); 
			return $this->rest( "lists/list", $data, 20 );
		}
 
 		/**
		 *
		 *    Update subscriber status
		 *
		 * @param string $email - string"
		 * @param string $list_id - string"
		 * s
		 */
		public function update_subscribe($email)
		{   
			$list_id		= get_field( 'mailchimp_audience_id', 'option' );
			$memberId 		= md5(strtolower($email));   
			if ( strlen( substr( strrchr( $this->api_key, '-' ), 1 ) ) ) {
				$dataCenter = substr( strrchr( $this->api_key, '-' ), 1 );
			} else {
				$dataCenter = "us1";
			} 
		    $url	= 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . $memberId; 
		    $json	= json_encode([
		        'email_address' => $email,
		        'status'        => 'subscribed', 
		    ]); 
		    $ch 	= curl_init($url);
		    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->api_key);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			$result = curl_exec($ch); 
			
			if ( $this->debug ) {
				echo "<pre>result:<br>";
				print_r( json_decode( $result, true ) );
				echo "<br>curlinfo:<br>";
				print_r( curl_getinfo( $ch ) );
				echo "</pre>";
			}
			
		    curl_close($ch);  
			
		}
		
 		/**
		 *
		 *    Get subscriber
		 *
		 * @param string $email - string" 
		 * s
		 */
		public function get_subscriber($email)
		{   
			$list_id		= get_field( 'mailchimp_audience_id', 'option' );
			$memberId		= md5(strtolower($email));   
			if ( strlen( substr( strrchr( $this->api_key, '-' ), 1 ) ) ) {
				$dataCenter	= substr( strrchr( $this->api_key, '-' ), 1 );
			} else {
				$dataCenter = "us1";
			} 
		    $url	= 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . $memberId; 
		    $json	= json_encode([
		        'email_address' => $email, 
		    ]); 
		    $ch		= curl_init($url);
		    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->api_key);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 
			$result = curl_exec( $ch );
			
			if ( $this->debug ) {
				echo "<pre>result:<br>";
				print_r( json_decode( $result, true ) );
				echo "<br>curlinfo:<br>";
				print_r( curl_getinfo( $ch ) );
				echo "</pre>";
			}
			
			curl_close($ch); 
			return json_decode( $result, true ); 
		}
 
		public function subscribe_to_list() {

			$email   	= stripslashes( $_POST['email'] );
			$list_id	= get_field( 'mailchimp_audience_id', 'option' );
			$optin   	= stripslashes( $_POST['optin'] ); 
			
			$subscriber = $this->get_subscriber( $email, $list_id, $optin );
			if($subscriber && $subscriber['status'] == 'unsubscribed'){
				$this->update_subscribe($email, true);  
				$result = array('status' => '', 'email' => $email); 
			}else{ 
				$result = $this->subscribe( $email, $list_id, $optin );
			}
			  
			if ( empty( $result['status'] ) == false ) {
				switch ( $result['name'] ) {
					case 'Invalid_ApiKey':
						echo json_encode( array(
							'action_status' => false,
							'message'       => $result['error'],
							'data'       	=> $result,
						) );
						break;
					case 'List_DoesNotExist':
						echo json_encode( array(
							'action_status' => false,
							'message'       => $result['error'],
							'data'       	=> $result,
						) );
						break;
					case 'ValidationError':
						echo json_encode( array(
							'action_status' => false,
							'message'       => esc_html__( 'Zadajte platnú e-mailovú adresu.', 'theme' ),
							'data'       	=> $result,
						) );
						break;

					case 'List_AlreadySubscribed':
						echo json_encode( array(
							'action_status' => false,
							'message'       => esc_html__( 'Tento e-mail sa už prihlásil na odber newslettra.', 'theme' ),
							'data'       	=> $result,
						) );
						break;

					case 'curl_package_disabled':
						echo json_encode( array(
							'action_status' => false,
							'message'       => esc_html__( 'Curl is disabled. Please enable curl in server php.ini settings.', 'theme' ),
							'data'       	=> $result, 
						) );
						break;
				}
			} elseif ( isset( $result['email'] ) ) {
				echo json_encode( array(
					'action_status' => true,
					'message'       => str_replace(['[email]'], [$result['email']], get_field('mailchimp_message', 'option')),
					'data'       	=> $result, 
				) );
			}
			wp_die();
		}
		
	}
}

$mailchimp = new Theme_Mailchimp();
