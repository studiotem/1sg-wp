<?php
function add_menu_link_atts( $atts, $item, $args ) {
    if( isset($args->theme_location) && $args->theme_location == 'primary' ) {
		$atts['class'] = 'nav-link';
    }
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_link_atts', 10, 3 );

function Theme_Populate_Children($menu_array, $menu_item){
    $children = array();
    if (!empty($menu_array)) {
        foreach ($menu_array as $k => $m) {
            if ($m->menu_item_parent == $menu_item->ID) {
                unset($menu_array[$k]);
                $children[$m->ID]               = array();
                $children[$m->ID]['ID']         = $m->ID;
                $children[$m->ID]['title']      = $m->title;
                $children[$m->ID]['url']        = $m->url;
                $children[$m->ID]['class']      = implode(' ', $m->classes); 
                $children[$m->ID]['children']   = Theme_Populate_Children($menu_array, $m);
            }
        }
    };
    return $children;
}

function Theme_Get_Menu_Array($menu_name='Main Menu'){
    $locations  = get_nav_menu_locations();
    $menu       = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_array = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
    $menu       = array();
    if ($menu_array){
        foreach ($menu_array as $m) { 
            if (empty($m->menu_item_parent)) {
                $menu[$m->ID]               = array();
                $menu[$m->ID]['ID']         = $m->ID;
                $menu[$m->ID]['title']      = $m->title;
                $menu[$m->ID]['url']        = $m->url;
                $menu[$m->ID]['class']      = implode(' ', $m->classes); 
                $menu[$m->ID]['children']   = Theme_Populate_Children($menu_array, $m);
            }
        }
    }
    return $menu;
}

function Theme_Desktop_Menu(){
    $menu = Theme_Get_Menu_Array('primary'); ?>
    <div class="header__menu">
        <?php if($menu): ?>
            <?php foreach($menu as $menu_item): ?>
                <?php if($menu_item['children']): ?>
                    <div class="header__link-menu header__link-item">
                        <div class="link__trigger">
                            <span class="value"><?= $menu_item['title']; ?></span>
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                            <div class="link__menu__wrapp">
                                <div class="link__menu">
                                    <?php foreach($menu_item['children'] as $children_item): ?>
                                        <a class="hide__link" href="<?= $children_item['url']; ?>"><?= $children_item['title']; ?></a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <a class="header__link header__link-item <?= $menu_item['class']; ?>" href="<?= $menu_item['url']; ?>"><?= $menu_item['title']; ?></a>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?php if($email = get_field('email', 'option')): ?>
        <a class="header__social" href="mailto:<?= $email; ?>">
            <svg class="icon icon-mail">
                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#mail"></use>
            </svg>
            <span><?= $email; ?></span>
        </a>
    <?php endif; ?>
    <div class="header__burger">
        <svg class="icon icon-burger">
            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#burger"></use>
        </svg>
    </div>
<?php }

function Theme_Mobile_Menu(){
    $menu = Theme_Get_Menu_Array('primary'); ?>
    <div class="header-hide__menu">
        <div class="menu__close">
            <svg class="icon icon-close">
                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#close"></use>
            </svg>
        </div>
        <div class="hide__block">
            <?php if($menu): ?>
                <?php foreach($menu as $menu_item): ?>
                    <?php if($menu_item['children']): ?>
                        <div class="menu__link hide-header__trigger" data-target="<?= $menu_item['title']; ?>">
                            <span><?= $menu_item['title']; ?></span>
                            <svg class="icon icon-arrow">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                    <?php else: ?>
                        <a class="menu__link <?= $menu_item['class']; ?>" href="<?= $menu_item['url']; ?>">
                            <span><?= $menu_item['title']; ?></span>
                        </a>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <?php if($email = get_field('email', 'option')): ?>
            <a class="header__social" href="mailto:<?= $email; ?>">
                <svg class="icon icon-mail">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#mail"></use>
                </svg>
                <span><?= $email; ?></span>
            </a>
        <?php endif; ?>
    </div>
    <?php if($menu): ?>
        <?php foreach($menu as $menu_item): ?>
            <?php if($menu_item['children']): ?>
                <div class="add-header__menu" data-target="<?= $menu_item['title']; ?>">
                    <div class="menu__close">
                        <svg class="icon icon-close">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#close"></use>
                        </svg>
                    </div>
                    <div class="menu__back">
                        <svg class="icon icon-arrow">
                            <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                        </svg>
                        <span><?= $menu_item['title']; ?></span>
                    </div>
                    <div class="links__block">
                        <?php foreach($menu_item['children'] as $children_item): ?>
                            <a class="hide-add__link" href="<?= $children_item['url']; ?>"><?= $children_item['title']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
<?php }
