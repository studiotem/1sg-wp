<?php 

add_action( 'wpcf7_init', 'wpcf7_add_form_tag_current_url' );
function wpcf7_add_form_tag_current_url() {
    wpcf7_add_form_tag( 'page_title', 'wpcf7_page_title_form_tag_handler', array('name-attr' => true));
    wpcf7_add_form_tag( 'page_url', 'wpcf7_page_url_form_tag_handler', array('name-attr' => true));
}

function wpcf7_page_title_form_tag_handler( $tag ) {
    return '<input type="hidden" name="page_title" value="'.get_the_title().'" />';
}

function wpcf7_page_url_form_tag_handler( $tag ) {
    return '<input type="hidden" name="page_url" value="'.get_the_permalink().'" />';
}
 
add_filter( 'gutenberg_use_widgets_block_editor', '__return_false' ); 
add_filter( 'use_widgets_block_editor', '__return_false' );
 
add_filter('wpcf7_autop_or_not', '__return_false');
