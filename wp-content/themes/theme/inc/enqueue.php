<?php

/************************************************************************************/
/* Enqueue scripts and styles. */
/************************************************************************************/

/* Frontend */
function theme_scripts() {
    $ver = time();
    wp_register_script( 'theme-app', get_template_directory_uri() . '/front/assets/js/app.js', array('jquery'), $ver, true );
    wp_enqueue_script( 'theme-app' );
    wp_register_script( 'theme-main', get_template_directory_uri() . '/front/assets/js/main.js', array('jquery'), $ver, true );
    wp_enqueue_script( 'theme-main' );
    wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/front/assets/css/app.css', array(), $ver );
    wp_enqueue_style( 'theme-custom', get_template_directory_uri() . '/css/custom.css', array(), $ver );
}
add_action( 'wp_enqueue_scripts', 'theme_scripts', '999' );

/* Backend */
add_action( 'admin_enqueue_scripts', 'admin_scripts' );
function admin_scripts() {
	$dir = get_template_directory_uri();
	if ( IS_LOCALHOST ) {
		$version = time();
	} else {
		$the_theme = wp_get_theme();
		$version = $the_theme->get( 'Version' );
	}
	wp_enqueue_style( 'style_admin', $dir . '/style_admin.css', array(), $version );
}

/* Languages */
function child_theme_text_domain() {
	load_child_theme_textdomain( 'custom_theme', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'child_theme_text_domain' );
