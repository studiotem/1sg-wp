<?php

function validate_time($start, $end){
    if( date('H:i', strtotime($start)) != '00:00' || date('H:i', strtotime($end)) != '00:00' ){
        return true;
    }
    return false;
}

function eventDate($end, $start, $formated = false, $html = true){
    $end 	= strtotime($end);
    $start 	= strtotime($start);
    if(date('d.m.Y', $end) != date('d.m.Y', $start)){
        if(date('m', $end) != date('m', $start)){
            if($formated){
                if($html){
                    echo '<div class="edate"> <strong>'.date_i18n('j.', $start).'</strong> '.date_i18n('F', $start).' - <strong>'.date_i18n('j.', $end).'</strong> '.date_i18n('F', $end).' <span class="year">'.date_i18n('Y', $end).'</span></div>';
                }else{
                    echo date_i18n('j.', $start).' '.date_i18n('F', $start).' - '.date_i18n('j.', $end).' '.date_i18n('F', $end).' '.date_i18n('Y', $end);
                }
            }else{
                if($html){
                    echo '<strong class="edate">'.date_i18n('j. F', $start).' - '.date_i18n('j. F Y', $end).'</strong>';
                }else{
                    echo date_i18n('j. F', $start).' - '.date_i18n('j. F Y', $end);
                }
            }

        }else{
            if($formated){
                if($html){
                    echo '<div class="edate"> <strong>'.date_i18n('j', $start).' - '.date_i18n('j.', $end).'</strong> '.date_i18n('F', $end).' <span class="year">'.date_i18n('Y', $end).'</span> </div>';
                }else{
                    echo date_i18n('j.', $start).' - '.date_i18n('j.', $end).' '.date_i18n('F', $end).' '.date_i18n('Y', $end);
                }
            }else{
                if($html){
                    echo '<strong class="edate">'.date_i18n('j.', $start).' - '.date_i18n('j. F Y', $end).'</strong>';
                }else{
                    echo date_i18n('j.', $start).' - '.date_i18n('j. F Y', $end);
                }
            }
        }
    }else{
        if($formated){
            if($html){
                echo '<div class="edate"> <strong>'.date_i18n('j.', $start).'</strong> '.date_i18n('F', $start).' <span class="year">'.date_i18n('Y', $start).'</span> </div>';
            }else{
                echo date_i18n('j.', $start).' '.date_i18n('F', $start).' '.date_i18n('Y', $start);
            }
        }else{
            if($html){
                echo '<strong class="edate">'.date_i18n('j. F Y', $start).'</strong>';
            }else{
                echo date_i18n('j. F Y', $start);
            }
        }
    }
}

function eventTime($end, $start, $html=true){
    $end 	= strtotime($end);
    $start 	= strtotime($start);
    if(date('H:i', $start) != date('H:i', $end)){
        if($html){
            return '<strong class="etime">'.date('H:i', $start).' - '.date('H:i', $end).'</strong>';
        }else{
            return date('H:i', $start).' - '.date('H:i', $end);
        }
    }
    return;
}

function getDatesFromRange($start, $end, $format = 'Y-m-d H:i:s') {
    $array 		= array();
    $interval 	= new DateInterval('P1D');
    $realEnd 	= new DateTime($end);
    $realEnd->add($interval);
    $period 	= new DatePeriod(new DateTime($start), $interval, $realEnd);
    foreach($period as $date) {
        $array[]= $date->format($format);
    }
    return $array;
}

function events_first_last_date() {
    $args	= array(
        'numberposts'	=> -1,
        'posts_per_page'=> -1,
        'post_type'		=> 'events',
    );
    $dates  = [];
    $events = get_posts($args);
    if($events){
        foreach ($events as $event){
            $start          = strtotime(get_post_meta($event->ID, 'start', true));
            $end            = strtotime(get_post_meta($event->ID, 'end', true));
            $dates[$start]  = $start;
            $dates[$end]    = $end;
        }
    }
    return [ 'start' => min($dates), 'end' => max($dates)];
}