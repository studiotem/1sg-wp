<?php 
	
function custom_post_types(){
	return array(
		'courses', 
		'events', 
	);    
}

function custom_post_type() {   
	 
	$def_labels = array( 
        'parent_item_colon'   => __( 'Rodič', 'theme' ),
        'all_items'           => __( 'Všetko', 'theme' ),
        'view_item'           => __( 'Zobraziť', 'theme' ),
        'add_new_item'        => __( 'Pridať novú', 'theme' ),
        'add_new'             => __( 'Pridať', 'theme' ),
        'edit_item'           => __( 'Upraviť', 'theme' ),
        'update_item'         => __( 'Upraviť', 'theme' ),
        'search_items'        => __( 'Vyhľadať', 'theme' ),
        'not_found'           => __( 'Nenájdené', 'theme' ),
        'not_found_in_trash'  => __( 'Nenašlo sa v koši', 'theme' ),
    );

    //Sociálne siete
    $labels = array(
        'name'                => __( 'Sociálne siete', 'theme' ),
        'singular_name'       => __( 'Sociálne siete', 'theme' ),
        'menu_name'           => __( 'Sociálne siete', 'theme' ),
    );
    $labels = array_merge($def_labels, $labels);
    $args 	= array(
        'label'               => __( 'Sociálne siete', 'theme' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
    );
    register_post_type( 'social', $args );

	//Events
	$labels = array(
        'name'                => __( 'Udalosti', 'theme' ),
        'singular_name'       => __( 'Udalosti', 'theme' ),
        'menu_name'           => __( 'Udalosti', 'theme' ), 
    ); 
    $labels = array_merge($def_labels, $labels);
    $args 	= array(
        'label'               => __( 'Udalosti', 'theme' ), 
        'labels'              => $labels, 
        'supports'            => array( 'title', 'editor', 'thumbnail' ), 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true, 
        'show_in_menu'        => true,
        'rewrite'			  => array( 'slug' => get_option( 'events_cpt_base' ) ),
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'events', $args );
     
	//Teachers
	$labels = array(
        'name'                => __( 'Učitelia', 'theme' ),
        'singular_name'       => __( 'Učitelia', 'theme' ),
        'menu_name'           => __( 'Učitelia', 'theme' ),
    ); 
    $labels	= array_merge($def_labels, $labels);
    $args	= array(
        'label'               => __( 'Učitelia', 'theme' ),
        'labels'              => $labels, 
        'supports'            => array( 'title', 'editor', 'thumbnail' ), 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true, 
        'show_in_menu'        => true,
        'rewrite'			  => array( 'slug' => get_option( 'teachers_cpt_base' ) ),
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'teachers', $args );

	//Gallery
	$labels = array(
        'name'                => __( 'Galéria', 'theme' ),
        'singular_name'       => __( 'Galéria', 'theme' ),
        'menu_name'           => __( 'Galéria', 'theme' ), 
    ); 
    $labels	= array_merge($def_labels, $labels);
    $args	= array(
        'label'               => __( 'Galéria', 'theme' ), 
        'labels'              => $labels, 
        'supports'            => array( 'title', 'editor', 'thumbnail' ), 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true, 
        'show_in_menu'        => true,
        'rewrite'			  => array( 'slug' => get_option( 'gallery_cpt_base' ) ),
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'gallery', $args );

    //Projekty
    $labels = array(
        'name'                => __( 'Projekty', 'theme' ),
        'singular_name'       => __( 'Projekty', 'theme' ),
        'menu_name'           => __( 'Projekty', 'theme' ),
    );
    $labels = array_merge($def_labels, $labels);
    $args 	= array(
        'label'               => __( 'Projekty', 'theme' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'rewrite'			  => array( 'slug' => get_option( 'projects_cpt_base' ) ),
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'query_var'           => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'projects', $args );

    //Učebne
    $labels = array(
        'name'                => __( 'Učebne', 'theme' ),
        'singular_name'       => __( 'Učebne', 'theme' ),
        'menu_name'           => __( 'Učebne', 'theme' ),
    );
    $labels = array_merge($def_labels, $labels);
    $args 	= array(
        'label'               => __( 'Učebne', 'theme' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'rewrite'			  => array( 'slug' => get_option( 'classrooms_cpt_base' ) ),
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => false,
        'query_var'           => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'classrooms', $args );

}
add_action( 'init', 'custom_post_type', 0 );
 
function custom_taxonomy() {
	
	$deflabels = array( 
		'search_items' 		=> __( 'Search' ),
		'all_items' 		=> __( 'All' ),
		'parent_item' 		=> __( 'Parent' ),
		'parent_item_colon' => __( 'Parent:' ),
		'edit_item' 		=> __( 'Edit' ), 
		'update_item' 		=> __( 'Update' ),
		'add_new_item' 		=> __( 'Add New' ),
		'new_item_name' 	=> __( 'New Name' ), 
	); 	

	//Teachers category
	$labels = array(
		'name' 				=> __( 'Kategória' ),
		'singular_name'	 	=> __( 'Kategória' ), 
		'menu_name' 		=> __( 'Kategória' ),
	); 	
	register_taxonomy('teachers_category', array('teachers'), array(
		'hierarchical' 		=> true,
		'labels' 			=> array_merge($labels, $deflabels),
		'show_ui' 			=> true,
		'show_admin_column' => true,
		'query_var' 		=> false,
		'rewrite' 			=> array( 'slug' => 'teachers_category' ),
	));

    // Gallery category
    $labels = array(
        'name' 				=> __( 'Kategória' ),
        'singular_name'	 	=> __( 'Kategória' ),
        'menu_name' 		=> __( 'Kategória' ),
    );
    register_taxonomy('gallery_category', array('gallery'), array(
        'hierarchical' 		=> true,
        'labels' 			=> array_merge($labels, $deflabels),
        'show_ui' 			=> true,
        'show_admin_column' => true,
        'query_var' 		=> false,
        'rewrite' 			=> array( 'slug' => 'gallery_category' ),
    ));

    // Gallery year
    $labels = array(
        'name' 				=> __( 'Školský rok' ),
        'singular_name'	 	=> __( 'Školský rok' ),
        'menu_name' 		=> __( 'Školský rok' ),
    );
    register_taxonomy('gallery_year', array('gallery'), array(
        'hierarchical' 		=> true,
        'labels' 			=> array_merge($labels, $deflabels),
        'show_ui' 			=> true,
        'show_admin_column' => true,
        'query_var' 		=> false,
        'rewrite' 			=> array( 'slug' => 'gallery_year' ),
    ));
	 
	// Projects category
    $labels = array(
		'name' 				=> __( 'Kategória' ),
		'singular_name'	 	=> __( 'Kategória' ), 
		'menu_name' 		=> __( 'Kategória' ),
	); 	
	register_taxonomy('projects_category', array('projects'), array(
		'hierarchical' 		=> true,
		'labels' 			=> array_merge($labels, $deflabels),
		'show_ui' 			=> true,
		'show_admin_column' => true,
		'query_var' 		=> false,
		'rewrite' 			=> array( 'slug' => 'projects_category' ),
	));

}
add_action( 'init', 'custom_taxonomy', 0 );

function custom_load_permalinks()
{
	if( isset( $_POST['events_cpt_base'] ) )
	{
		update_option( 'events_cpt_base', ( $_POST['events_cpt_base'] ) );
	}
	if( isset( $_POST['teachers_cpt_base'] ) )
	{
		update_option( 'teachers_cpt_base', ( $_POST['teachers_cpt_base'] ) );
	}
	if( isset( $_POST['gallery_cpt_base'] ) )
	{
		update_option( 'gallery_cpt_base', ( $_POST['gallery_cpt_base'] ) );
	}
    if( isset( $_POST['projects_cpt_base'] ) )
    {
        update_option( 'projects_cpt_base', ( $_POST['projects_cpt_base'] ) );
    }
    if( isset( $_POST['classrooms_cpt_base'] ) )
    {
        update_option( 'classrooms_cpt_base', ( $_POST['classrooms_cpt_base'] ) );
    }
	add_settings_field( 'events_cpt_base', __( 'Slug pre archív udalostí' ), 'events_field_callback', 'permalink', 'optional' );
	add_settings_field( 'teachers_cpt_base', __( 'Slug pre archív profesorov' ), 'teachers_field_callback', 'permalink', 'optional' );
	add_settings_field( 'gallery_cpt_base', __( 'Slug pre archív galérii' ), 'gallery_field_callback', 'permalink', 'optional' );
    add_settings_field( 'projects_cpt_base', __( 'Slug pre archív projektov' ), 'projects_field_callback', 'permalink', 'optional' );
    add_settings_field( 'classrooms_cpt_base', __( 'Slug pre archív učební' ), 'classrooms_field_callback', 'permalink', 'optional' );
}

add_action( 'load-options-permalink.php', 'custom_load_permalinks' );

function events_field_callback()
{
	$cpt_base = get_option( 'events_cpt_base' );
	echo '<input type="text" value="' . esc_attr( ($cpt_base) ? $cpt_base : 'events' ) . '" name="events_cpt_base" id="events_cpt_base" class="regular-text code" />';
}

function teachers_field_callback()
{
	$cpt_base = get_option( 'teachers_cpt_base' );
	echo '<input type="text" value="' . esc_attr( ($cpt_base) ? $cpt_base : 'teachers' ) . '" name="teachers_cpt_base" id="teachers_cpt_base" class="regular-text code" />';
}

function gallery_field_callback()
{
	$cpt_base = get_option( 'gallery_cpt_base' );
	echo '<input type="text" value="' . esc_attr( ($cpt_base) ? $cpt_base : 'gallery' ) . '" name="gallery_cpt_base" id="gallery_cpt_base" class="regular-text code" />';
}

function projects_field_callback()
{
    $cpt_base = get_option( 'projects_cpt_base' );
    echo '<input type="text" value="' . esc_attr( ($cpt_base) ? $cpt_base : 'projects' ) . '" name="projects_cpt_base" id="projects_cpt_base" class="regular-text code" />';
}

function classrooms_field_callback()
{
    $cpt_base = get_option( 'classrooms_cpt_base' );
    echo '<input type="text" value="' . esc_attr( ($cpt_base) ? $cpt_base : 'classrooms' ) . '" name="classrooms_cpt_base" id="classrooms_cpt_base" class="regular-text code" />';
}

