<?php

/************************************************************************************/
/* Theme setup */
/************************************************************************************/

// Register Theme Features
function theme_support()  {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
	add_theme_support( 'title-tag' );
	add_editor_style( 'style_admin.css' );
    remove_action('wp_head', 'wp_generator');
    add_filter('show_admin_bar', '__return_false');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'generator');
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
    remove_action( 'wp_head', 'addFrontCss', 9999 );
    add_theme_support( 'custom-logo' );
    remove_filter('template_redirect', 'redirect_canonical');
    load_theme_textdomain( 'theme', get_template_directory() . '/languages' );
    register_nav_menus( array(
        'primary' => 'Primárne menu',
    ) );
    add_theme_support( 'post-formats', array() );
}
add_action( 'after_setup_theme', 'theme_support' );

/************************************************************************************/
/* Disable updates */
/************************************************************************************/
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );


/************************************************************************************/
/* Disable Author archives */
/************************************************************************************/
function wp_disable_author_archives() {
	if ( is_author() ) {
		global $wp_query;
		$wp_query->set_404();
		status_header(404);
	} else {
		redirect_canonical();
	}
}
remove_filter( 'template_redirect',  'redirect_canonical' );
add_action( 'template_redirect',  'wp_disable_author_archives' );


/************************************************************************************/
/* Remove autocomplete for password in wp-login.php */
/************************************************************************************/
function login_autocomplete_off() {
	echo '
		<script>
			document.getElementById( "user_pass" ).autocomplete = "off";
		</script>
	';
}
add_action( 'login_form', 'login_autocomplete_off' );


/************************************************************************************/
/* Dynamic web indexing */
/************************************************************************************/
if ( ENVIROMENT_IS_PRODUCTION ) {
	if ( get_option( 'blog_public' ) === '0' ) {
		update_option( 'blog_public', '1' );
	}
} else {
	if ( get_option( 'blog_public' ) === '1' ) {
		update_option( 'blog_public', '0' );
	}
}


/************************************************************************************/
/* SVG & OGG support */
/************************************************************************************/
function add_file_types_to_uploads( $file_types ){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
    $new_filetypes['ogg'] = 'application/oggl';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action( 'upload_mimes', 'add_file_types_to_uploads' );


/************************************************************************************/
/* Remove from admin menu */
/************************************************************************************/
function menu_remove() {
	// remove_menu_page( 'edit.php' );
	// remove_menu_page( 'edit-comments.php' );
}
add_action('admin_menu', 'menu_remove');


/************************************************************************************/
/* Unregister post tags */
/************************************************************************************/
function unregister_post_tags() {
	// unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'unregister_post_tags' );


function theme_force_https () {
	$whitelist = array('127.0.0.1', '::1');
	if ( !is_ssl() && !in_array($_SERVER['REMOTE_ADDR'], $whitelist) ) {
		wp_redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301 );
		exit();
	}
}
add_action ( 'template_redirect', 'theme_force_https', 1 );

function iconic_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}
add_action( 'wp_print_scripts', 'iconic_remove_password_strength', 10 );
