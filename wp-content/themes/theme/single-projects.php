<?php get_header(); ?>
<?php $page_id = get_page_by_template('template/tpl-projects.php'); ?>
<div class="project-detail__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title( $page_id ); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')):
                bcn_display();
            endif; ?>
        </ul>
        <div class="hero__block">
            <div class="hero__back">
                <?php
                $_referer   = wp_get_referer();
                $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
                ?>
                <a class="back__btn" href="<?= $referer; ?>">
                    <svg class="icon icon-arrow-link ">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                    </svg>
                    <span><?= __('Návrat', 'theme'); ?></span>
                </a>
                <span class="line"></span>
            </div>
        </div>
    </div>
</div>

<?php $args = array(
    'post_type'	    => 'projects',
    'posts_per_page'=> -1,
    'post_parent'   => get_the_id(),
    'orderby'       => 'post_date',
    'order'         => 'ASC',
);
$posts = new WP_Query( $args );
if ( $posts->have_posts() ) : ?>
    <div class="project-detail__projects">
        <div class="projects__container">
            <?php if($project_bg = get_field('project_bg')): ?>
                <div class="projects__patern">
                    <img class="patern" src="<?= $project_bg; ?>" alt="patern">
                </div>
            <?php endif; ?>
            <div class="project__slider">
                <div class="slider__row">
                    <div class="slider__title"><?= __('Vybrané projekty', 'theme'); ?></div>
                    <div class="project-slider__buttons">
                        <div class="slider-button button-prev">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                        <div class="slider-button button-next">
                            <svg class="icon icon-arrow ">
                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php while ( $posts->have_posts() ) : $posts->the_post();
                            get_template_part( 'template-parts/content-projects' );
                        endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>

<div class="project-detail__info">
    <div class="info__container">
        <h2><?= get_the_title(); ?></h2>
        <?php the_content(); ?>
    </div>
</div>
 
<?php get_footer(); ?> 