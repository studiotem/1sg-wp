<?php get_header(); ?>

<div class="photography__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_archive_title( 'gallery' ); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')):
                bcn_display();
            endif; ?>
        </ul>
        <div class="hero__block">
            <div class="hero__back">
                <?php
                $_referer   = wp_get_referer();
                $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
                ?>
                <a class="back__btn" href="<?= $referer; ?>">
                    <svg class="icon icon-arrow-link ">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                    </svg>
                    <span><?= __('Návrat', 'theme'); ?></span>
                </a>
                <span class="line"></span>
            </div>
        </div>
    </div>
</div>

<div class="photography__info">
    <div class="info__container">
        <div class="info__title"><?= get_the_title(); ?></div>
        <div class="info__date"><?= get_the_date(); ?></div>
        <div class="info__block lightgallery" id="lightgallery">
            <?php if($images = get_field('gallery_photos')): ?>
                <?php foreach($images as $image): ?> 
                    <?php $video_field = get_post_meta($image['ID'], 'video_field', true); ?>
                    <?php if($video_field): ?> 
                        <a class="block__item video-item" href="<?= $video_field; ?>" data-fancybox="gallery">
                            <div class="item__iamge">
                                <img class="photo__img" src="<?= $image['sizes']['350x350']; ?>" alt="<?= $image['alt']; ?>"/>
                            </div>
                        </a>
                    <?php else: ?> 
                        <a class="block__item" href="<?= $image['sizes']['large']; ?>" data-fancybox="gallery">
                            <div class="item__iamge">
                                <img class="photo__img" src="<?= $image['sizes']['350x350']; ?>" alt="<?= $image['alt']; ?>"/>
                            </div>
                        </a> 
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?php get_template_part( 'template-parts/content', 'none' );  ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?> 