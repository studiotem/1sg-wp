<?php
/*
* Template Name: Archív galérii
*/
?>    
<?php get_header(); ?>

<div class="gallery__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_archive_title( 'gallery' ); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')):
                bcn_display();
            endif; ?>
        </ul>
        <div class="hero__block">
            <div class="hero__back">
                <?php
                $_referer   = wp_get_referer();
                $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
                ?>
                <a class="back__btn" href="<?= $referer; ?>">
                    <svg class="icon icon-arrow-link ">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                    </svg>
                    <span><?= __('Návrat', 'theme'); ?></span>
                </a>
                <span class="line"></span>
            </div>
        </div>
    </div>
</div>

<div class="gallery__info">
    <div class="info__container">
        <?php
        $next_year_url      = '#';
        $next_year_class    = 'disable';
        $prev_year_url      = '#';
        $prev_year_class    = 'disable';
        $current_year       = 0;
        $years              = get_terms( array('taxonomy' => 'gallery_year') );
        if($years){
            foreach($years as $yearKey => $year){
                if(isset($_GET['gallery_year']) && $year->slug == $_GET['gallery_year']){
                    $current_year   = $yearKey;
                }elseif( (!isset($_GET['gallery_year']) || $_GET['gallery_year'] == '') && strpos($year->slug, '-'.date('Y')) !== false){
                    $current_year   = $yearKey;
                }
            }
        }
        $previous_year_key      = get_previous_key_array($years, $current_year);
        $next_year_key          = get_next_key_array($years, $current_year);
        if($next_year_key && isset($years[$next_year_key])) {
            $next_year_url      = '?gallery_category=' . (isset($_GET['gallery_category']) ? $_GET['gallery_category'] : '') . '&gallery_year=' . $years[$next_year_key]->slug;
            $next_year_class    = '';
        }
        if($previous_year_key && isset($years[$previous_year_key])) {
            $prev_year_url      = '?gallery_category=' . (isset($_GET['gallery_category']) ? $_GET['gallery_category'] : '') . '&gallery_year=' . $years[$previous_year_key]->slug;
            $prev_year_class    = '';
        }
        ?>
        <div class="info__count-year">
            <a href="<?= $prev_year_url; ?>">
                <button class="pagination-button button__prev <?= $prev_year_class; ?>" type="button">
                    <svg class="icon icon-arrow ">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                    </svg>
                </button>
            </a>
            <a href="<?= $next_year_url; ?>">
                <button class="pagination-button button__next <?= $next_year_class; ?>" type="button">
                    <svg class="icon icon-arrow ">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                    </svg>
                </button>
            </a>
            <div class="count__block">
                <div class="count__placeholder"><?= __('Školský rok', 'theme'); ?></div>
                <div class="count__years"><?= isset($years[$current_year]) ? $years[$current_year]->name : '-'; ?></div>
            </div>
        </div>
        <div class="selects__buttons">
            <div class="buttons__block">
                <?php $categories   = get_terms( array('taxonomy' => 'gallery_category') ); ?>
                <?php foreach($categories as $category): ?>
                    <label class="category__label">
                        <a href="?gallery_category=<?= $category->slug; ?>&gallery_year=<?= (isset($_GET['gallery_year']) ? $_GET['gallery_year'] : ''); ?>">
                            <input class="checkbox__category" type="checkbox" id="<?= $category->term_id; ?>" <?php if(isset($_GET['gallery_category']) && $category->slug == $_GET['gallery_category']): ?>checked="checked"<?php endif; ?>>
                            <span class="category__inner <?= get_field('category_color', $category); ?>">
                                <span class="icons">
                                    <svg class="icon icon-done">
                                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#done"></use>
                                    </svg>
                                </span>
                                <span class="value"><?= $category->name; ?></span>
                            </span>
                        </a>
                    </label>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
        $paged	    = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
        $args 	    = array(
            'post_type'	    => 'gallery',
            'paged'		    => $paged,
            'posts_per_page'=> -1,
        );
        $posts  = new WP_Query( query_filter($args) );
        if ( $posts->have_posts() ) : ?>
            <div class="info__block">
                <?php while ( $posts->have_posts() ) : $posts->the_post();
                    get_template_part( 'template-parts/content-list-gallery' );
                endwhile; ?>
            </div>
        <?php else :
            get_template_part( 'template-parts/content', 'none' );
        endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</div>

<?php get_footer(); ?>