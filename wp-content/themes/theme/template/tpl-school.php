<?php
/*
* Template Name: Detail školy
*/
?>   
<?php get_header(); ?>

<?php
$content = get_field('content');
if ($content):
    $content_order = [];
    foreach ($content as $key => $template):
        if(!isset($content_order[$template['acf_fc_layout']])){
            $content_order[$template['acf_fc_layout']] = 0;
        }
        $content_order[$template['acf_fc_layout']]++;
        include get_template_directory() . '/template-parts/flexible-content/' . $template['acf_fc_layout'] . '.php';
    endforeach;
else:
    while ( have_posts() ) : the_post();
        get_template_part( 'template-parts/content', 'page' );
    endwhile;
endif; ?>

<?php get_footer(); ?>