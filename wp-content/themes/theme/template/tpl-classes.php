<?php
/*
* Template Name: Triedy
*/
?>   
<?php get_header(); ?>
<div class="triedy__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title(); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </ul>
        <div class="hero__back">
            <?php
            $_referer   = wp_get_referer();
            $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
            ?>
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
    </div>
</div>

<div class="triedy__info">
    <div class="info__container">
        <?php if (has_post_thumbnail() ): ?>
            <div class="info__photo">
                <img class="info__img" src="<?= get_the_post_thumbnail_url(get_the_ID(), '1120x380'); ?>" alt="<?= get_the_title();?>"/>
            </div>
        <?php endif; ?>
        <div class="info__text">
            <?= str_replace(['<blockquote>', '</blockquote>'], ['<div class="block__top">', '</div>'], apply_filters('the_content', get_the_content())); ?>
        </div>
    </div>
</div>
<?php if ($schools = get_field('schools') ): ?>
    <div class="triedy__table">
        <div class="table__container">
            <?php foreach ($schools as $key=>$school ): ?>
                <div class="table__block <?= $school['class_color']; ?> <?php if($key == 0): ?>block__margin<?php endif; ?>">
                    <div class="block__icon">
                        <div class="icon">
                            <?= $school['class_acronym']; ?>
                        </div>
                    </div>
                    <div class="block__header">
                        <div class="block__title"><?= $school['school_title']; ?></div>
                    </div>
                    <div class="block__lessons">
                        <?php if ($years = $school['years'] ): ?>
                            <?php foreach ($years as $year ): ?>
                                <div class="lesson__item">
                                    <div class="item__number"><?= $year['year_title']; ?></div>
                                    <div class="item__category">
                                        <?php $year_classes = explode(',', $year['year_classes']); ?>
                                        <?php if ($year_classes): ?>
                                            <?php foreach ($year_classes as $year_class ): ?>
                                                <?php if ($year_class): ?>
                                                    <div class="lesson__category"><?= $year_class; ?></div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>