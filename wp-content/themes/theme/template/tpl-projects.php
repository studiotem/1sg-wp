<?php
/*
* Template Name: Archív projektov
*/
?>   
<?php get_header(); ?>
<?php $page_id = get_page_by_template('template/tpl-projects.php'); ?>
<div class="kalendar__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title($page_id); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </ul>
        <div class="hero__back">
            <?php
            $_referer   = wp_get_referer();
            $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
            ?>
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
    </div>
</div>
<div class="project__info">
    <div class="info__container">
        <?= apply_filters('the_content', get_the_content(null, false, $page_id)); ?>
        <?php $args = array(
            'post_type'	    => 'projects',
            'posts_per_page'=> -1,
            'post_parent'   => 0,
            'orderby'       => 'post_date',
            'order'         => 'ASC',
        );
        $posts = new WP_Query( $args );
        if ( $posts->have_posts() ) : ?>
            <div class="info__block">
                <?php while ( $posts->have_posts() ) : $posts->the_post();
                    get_template_part( 'template-parts/content-list-projects' );
                endwhile; ?>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</div>

<?php get_footer(); ?>