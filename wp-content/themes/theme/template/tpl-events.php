<?php
/*
* Template Name: Archív udalostí
*/
?>   
<?php get_header(); ?>

<?php $page_id = get_page_by_template('template/tpl-events.php'); ?>
<div class="kalendar__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title($page_id); ?></div> 
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </ul>
        <div class="hero__back">
            <?php
            $_referer   = wp_get_referer();
            $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
            ?>
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
    </div>
</div>
<?php
$dates          = events_first_last_date();
$months         = array_merge(array(date('Y-m-01')), getDatesFromRange( date('Y-m-d H:i:s', $dates['start']), date('Y-m-d H:i:s', $dates['end']), 'Y-m-01'));
$months         = array_values(array_unique($months));
sort($months);

$next_month_url     = '#';
$next_month_class   = 'disable';
$prev_month_url     = '#';
$prev_month_class   = 'disable';
$current_month      = 0;
if ($months) {
    foreach ($months as $monthKey => $month) {
        if (isset($_GET['month']) && $month == $_GET['month']) {
            $current_month = $monthKey;
        } elseif ((!isset($_GET['month']) || $_GET['month'] == '') && strpos($month, date('Y-m-01')) !== false) {
            $current_month = $monthKey;
        }
    }
}
$previous_month_key  = get_previous_key_array($months, $current_month);
$next_month_key      = get_next_key_array($months, $current_month);
if ($next_month_key && isset($months[$next_month_key])) {
    $next_month_url  = '?month=' . $months[$next_month_key];
    $next_month_class= '';
}
if ($previous_month_key && isset($months[$previous_month_key])) {
    $prev_month_url  = '?month=' . $months[$previous_month_key];
    $prev_month_class= '';
}
?>
<div class="kalendar__info">
    <img class="elem__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/elem-kalendar.png" alt="Shape"/>
    <div class="kalendar__container">
        <div class="kalendar__pagination">
            <a href="<?= $prev_month_url; ?>">
                <button class="pagination-button button__prev <?= $prev_month_class; ?>" type="button">
                    <svg class="icon icon-arrow">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                    </svg>
                </button>
            </a>
            <a href="<?= $next_month_url; ?>">
                <button class="pagination-button button__next <?= $next_month_class; ?>" type="button">
                    <svg class="icon icon-arrow">
                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow"></use>
                    </svg>
                </button>
            </a>
            <div class="pagination__title"><?= isset($months[$current_month]) ? date_i18n('F Y', strtotime($months[$current_month])) : '-'; ?></div>
        </div>
        <div class="kalendar__block">
            <?php
            $paged	= ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
            $args 	= array(
                'post_type'	    => 'events',
                'paged'		    => $paged,
                'posts_per_page'=> -1,
            );
            $posts = new WP_Query( query_filter($args) );
            if ( $posts->have_posts() ) : ?>
                <?php while ( $posts->have_posts() ) : $posts->the_post();
                    get_template_part( 'template-parts/content-events' );
                endwhile; ?>
            <?php else :
                get_template_part( 'template-parts/content', 'none' );
            endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>