<?php
/*
* Template Name: Kontakt
*/
?>   
<?php get_header(); ?>

<div class="kontakt__hero">
	<div class="hero__container">
		<div class="hero__title"><?= the_title(); ?></div>
		<ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')):
                bcn_display();
            endif; ?>
		</ul>
        <?php
        $_referer   = wp_get_referer();
        $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
        ?>
        <div class="hero__back">
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
	</div>
</div>
<div class="kontakt__info">
	<img class="elem__img" src="<?= get_template_directory_uri(); ?>/front/web/img/content/elem-kontakt.png" alt="img"/>
	<div class="info__container">
		<div class="info__map">
			<div
                class="map__container"
                id="map"
                data-marker="<?= get_template_directory_uri(); ?>/front/assets/svg/marker.svg"
                data-zoom="17"
                data-latitude="48.157444"
                data-longitude="17.143632"
            ></div>
            <?php if($contact_social = get_field('contact_social')): ?>
                <div class="map__social">
                    <p><?= __('Sledujte nás', 'theme'); ?></p>
                    <div class="map__row">
                        <?php foreach($contact_social as $item): ?>
                            <a class="map-social__item" href="<?= $item['url']; ?>" target="_blank">
                                <svg class="icon icon-<?= $item['icon']; ?>">
                                    <?php if($item['icon'] == 'facebook'): ?>
                                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#facebook"></use>
                                    <?php elseif($item['icon'] == 'instagram'): ?>
                                        <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#inst"></use>
                                    <?php endif; ?>
                                </svg>
                                <span><?= $item['title']; ?></span>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
		</div>
		<div class="info__texts">
            <?php if($contact_address = get_field('contact_address')): ?>
                <div class="text__item">
                    <div class="item__top">
                        <div class="top__icons">
                            <div class="top__icon">
                                <svg class="icon icon-marker">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#marker"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="top__title"><?= __('Adresa', 'theme'); ?></div>
                    </div>
                    <div class="item__content">
                        <?php foreach($contact_address as $item): ?>
                            <div class="content__block">
                                <?= $item['address']; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($contact_bank = get_field('contact_bank')): ?>
                <div class="text__item">
                    <div class="item__top">
                        <div class="top__icons">
                            <div class="top__icon">
                                <svg class="icon icon-bank ">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#bank"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="top__title"><?= __('Bankové spojenie', 'theme'); ?></div>
                    </div>
                    <div class="item__content">
                        <?php foreach($contact_bank as $item): ?>
                            <div class="content__block">
                                <?= $item['bank']; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($contact_contacts = get_field('contact_contacts')): ?>
                <div class="text__item kontakt__item">
                    <div class="item__top">
                        <div class="top__icons">
                            <div class="top__icon">
                                <svg class="icon icon-mail">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#mail"></use>
                                </svg>
                            </div>
                            <div class="top__icon">
                                <svg class="icon icon-phone">
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#phone"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="top__title"><?= __('Kontakty', 'theme'); ?></div>
                    </div>
                    <div class="item__content">
                        <?php foreach($contact_contacts as $item): ?>
                            <div class="content__block">
                                <h3><?= $item['name']; ?></h3>
                                <?php if($item['email']): ?>
                                    <a href="mailto:<?= $item['email']; ?>">
                                        <span class="icone">
                                            <svg class="icon icon-mail">
                                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#mail"></use>
                                            </svg>
                                        </span>
                                        <span class="value"><?= $item['email']; ?></span>
                                    </a>
                                <?php endif; ?>
                                <?php if($item['phone']): ?>
                                    <a href="tel:<?= $item['phone']; ?>">
                                        <span class="icone">
                                            <svg class="icon icon-phone">
                                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#phone"></use>
                                            </svg>
                                        </span>
                                        <span class="value"><?= $item['phone']; ?></span>
                                    </a> 
                                <?php endif; ?>
                                <?php if($item['other']): ?>
                                    <a href="#">
                                        <span class="icone">
                                            <svg class="icon icon-directory">
                                                <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#directory"></use>
                                            </svg>
                                        </span>
                                        <span class="value"><?= $item['other']; ?></span> 
                                    </a>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
		</div>
        <?php if($contact_social): ?>
            <div class="map__social hide">
                <p><?= __('Sledujte nás', 'theme'); ?></p>
                <div class="map__row">
                    <?php foreach($contact_social as $item): ?>
                        <a class="map-social__item" href="<?= $item['url']; ?>" target="_blank">
                            <svg class="icon icon-<?= $item['icon']; ?>">
                                <?php if($item['icon'] == 'facebook'): ?>
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#facebook"></use>
                                <?php elseif($item['icon'] == 'instagram'): ?>
                                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#inst"></use>
                                <?php endif; ?>
                            </svg>
                            <span><?= $item['title']; ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>