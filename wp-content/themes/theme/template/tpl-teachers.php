<?php
/*
* Template Name: Archív učiteľov
*/
?>   
<?php get_header(); ?>
<?php $page_id = get_page_by_template('template/tpl-teachers.php'); ?>
<div class="teachers__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title($page_id); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </ul>
        <div class="hero__back">
            <?php
            $_referer   = wp_get_referer();
            $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
            ?>
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
    </div>
</div>

<div class="teachers__content">
    <div class="content__container">
        <?php if (has_post_thumbnail($page_id) ): ?>
            <div class="teachers__photo">
                <img class="teachers__img" src="<?= get_the_post_thumbnail_url($page_id, '1120x380'); ?>" alt="<?= get_the_title($page_id);?>"/>
            </div>
        <?php endif; ?>
        <?= str_replace(['<blockquote>', '</blockquote>'], ['<div class="block__top">', '</div>'], apply_filters('the_content', get_the_content(null, false, $page_id))); ?>
        <?php
        $terms = get_terms( array(
            'taxonomy' 	=> 'teachers_category',
            'hide_empty'=> true,
        ) );
        if($terms):
            foreach($terms as $term): ?>
                <div class="teacher__block">
                    <div class="block__title"><?= $term->name; ?></div>
                    <div class="teacher__items">
                        <?php $args 	= array(
                            'post_type'		=> 'teachers',
                            'posts_per_page'=> -1, 
                            'tax_query' 	=> array(
                                array(
                                    'taxonomy' => 'teachers_category',
                                    'field'    => 'slug',
                                    'terms'    => $term->slug,
                                )
                            )
                        );
                        $posts = new WP_Query( $args );
                        if ( $posts->have_posts() ) : ?>
                            <?php  while ( $posts->have_posts() ) : $posts->the_post();
                                get_template_part( 'template-parts/content-list-teachers' );
                            endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>