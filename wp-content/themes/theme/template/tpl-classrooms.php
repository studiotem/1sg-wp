<?php
/*
* Template Name: Archív učební
*/
?>   
<?php get_header(); ?>
<?php $page_id = get_page_by_template('template/tpl-classrooms.php'); ?>
<div class="classes__hero">
    <div class="hero__container">
        <div class="hero__title"><?= get_the_title($page_id); ?></div>
        <ul class="breadcrumbs">
            <?php if(function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </ul>
        <div class="hero__back">
            <?php
            $_referer   = wp_get_referer();
            $referer    = ($_referer) ? $_referer : esc_url(home_url('/'));
            ?>
            <a class="back__btn" href="<?= $referer; ?>">
                <svg class="icon icon-arrow-link ">
                    <use href="<?= get_template_directory_uri(); ?>/front/assets/icon/symbol/sprite.svg#arrow-link"></use>
                </svg>
                <span><?= __('Návrat', 'theme'); ?></span>
            </a>
            <span class="line"></span>
        </div>
    </div>
</div>

<div class="classes__info">
    <div class="info__container">
        <div class="classes__block">
            <?php $args 	= array(
                'post_type'		=> 'classrooms',
                'posts_per_page'=> -1,
            );
            $posts = new WP_Query( $args );
            if ( $posts->have_posts() ) : ?>
                <?php  while ( $posts->have_posts() ) : $posts->the_post();
                    get_template_part( 'template-parts/content-list-classrooms' );
                endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>