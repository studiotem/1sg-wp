<form action="/" method="get"> 
	<div class="sidebar-search-box">
        <input type="search" name="s" id="search" value="<?php the_search_query(); ?>">
        <button type="submit"><i class="fa fa-search"></i></button>
    </div> 
</form>