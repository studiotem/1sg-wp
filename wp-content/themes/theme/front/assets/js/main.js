"use strict";

window.addEventListener('load', function () {
  /* SLIDE UP */
  var slideUp = function slideUp(target) {
    var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
    target.style.transitionProperty = 'height, margin, padding';
    target.style.transitionDuration = duration + 'ms';
    target.style.boxSizing = 'border-box';
    target.style.height = target.offsetHeight + 'px';
    target.offsetHeight;
    target.style.overflow = 'hidden';
    target.style.height = 0;
    target.style.paddingTop = 0;
    target.style.paddingBottom = 0;
    target.style.marginTop = 0;
    target.style.marginBottom = 0;
    window.setTimeout(() => {
      target.style.display = 'none';
      target.style.removeProperty('height');
      target.style.removeProperty('padding-top');
      target.style.removeProperty('padding-bottom');
      target.style.removeProperty('margin-top');
      target.style.removeProperty('margin-bottom');
      target.style.removeProperty('overflow');
      target.style.removeProperty('transition-duration');
      target.style.removeProperty('transition-property'); //alert("!")
    }, duration);
  };
  /* SLIDE DOWN */


  var slideDown = function slideDown(target) {
    var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
    target.style.removeProperty('display');
    var display = window.getComputedStyle(target).display;
    if (display === 'none') display = 'flex';
    target.style.display = display;
    var height = target.offsetHeight;
    target.style.overflow = 'hidden';
    target.style.height = 0;
    target.style.paddingTop = 0;
    target.style.paddingBottom = 0;
    target.style.marginTop = 0;
    target.style.marginBottom = 0;
    target.offsetHeight;
    target.style.boxSizing = 'border-box';
    target.style.transitionProperty = 'height, margin, padding';
    target.style.transitionDuration = duration + 'ms';
    target.style.height = height + 'px';
    target.style.removeProperty('padding-top');
    target.style.removeProperty('padding-bottom');
    target.style.removeProperty('margin-top');
    target.style.removeProperty('margin-bottom');
    window.setTimeout(() => {
      target.style.removeProperty('height');
      target.style.removeProperty('overflow');
      target.style.removeProperty('transition-duration');
      target.style.removeProperty('transition-property');
    }, duration);
  };
  /* TOOGGLE */


  var slideToggle = function slideToggle(target) {
    var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;

    if (window.getComputedStyle(target).display === 'none') {
      return slideDown(target, duration);
    } else {
      return slideUp(target, duration);
    }
  };

  (function ($) {
    $(document).ready(function () {
      svg4everybody({});
    });
  })(jQuery); // //header__menu


  var body = document.querySelector('body');
  var html = document.querySelector('html');
  var header = document.querySelector('.header__wrap');
  var scrolled = false;

  if (header) {
    window.addEventListener('scroll', function () {
      if (window.scrollY > 0) {
        header.classList.add('active');
        scrolled = true;
      } else {
        header.classList.remove('active');
        scrolled = false;
      }
    });
    window.addEventListener('load', function () {
      if (window.scrollY > 0) {
        header.classList.add('active');
      } else {
        header.classList.remove('active');
      }
    });
    var triggers = header.querySelectorAll('.link__trigger');
    var wrapp = document.querySelector('.hide__wrapper');

    if (triggers && wrapp) {
      triggers.forEach(trig => {
        trig.addEventListener('mouseenter', function () {
          wrapp.classList.add('active');
        });
        trig.addEventListener('mouseleave', function () {
          wrapp.classList.remove('active');
        });
      });
    } // let menues = header.querySelectorAll(".link__menu")
    // if(triggers && menues){
    //   triggers.forEach(trigger=>{
    //     trigger.addEventListener("mouseenter",function(){
    //       let parent=trigger.closest(".header__link-menu")
    //       let menuBlock=parent.querySelector(".link__menu")
    //       // close all
    //       if(!trigger.classList.contains("active")){
    //         triggers.forEach(el=>{
    //           if(el.classList.contains("active")){
    //             el.classList.remove("active")
    //           }
    //         })
    //         menues.forEach(block=>{
    //           if(block.classList.contains("active")){
    //             slideUp(block,400)
    //             block.classList.remove("active")
    //           }
    //         })
    //         wrapp.classList.remove("active")
    //         if(!scrolled){
    //           header.classList.remove("active")
    //         }
    //         body.classList.remove("lock")
    //         html.classList.remove("lock")
    //       }
    //       //open current
    //       if(!trigger.classList.contains("active")){
    //         slideDown(menuBlock,400)
    //         menuBlock.classList.add("active")
    //         trigger.classList.add("active")
    //         wrapp.classList.add("active")
    //         if(scrolled){
    //           header.classList.add("active")
    //         }else{
    //           if(window.scrollY == 0){
    //             header.classList.add("active")
    //           }else{
    //             header.classList.remove("active")
    //           }
    //         }
    //         body.classList.add("lock")
    //         html.classList.add("lock")
    //       }
    //     })
    //   })
    // }
    //  close on window click
    // const selectsInsideMenu=document.querySelector(".selects-inside")
    //   selectsInsideMenu && selectsInsideMenu.addEventListener('click', function (e) {
    //     const target = e.target
    //     menues.forEach(item=>{
    //       if (!target.closest('.link__menu') && item.classList.contains("active") && !target.closest('.link__trigger')) {
    //         slideUp(item,400)
    //         item.classList.remove("active")
    //         wrapp.classList.remove("active")
    //         if(!scrolled){
    //           header.classList.remove("active")
    //         }
    //         body.classList.remove("lock")
    //         html.classList.remove("lock")
    //         triggers.forEach(element => {
    //           element.classList.remove("active")
    //         });
    //   }
    //     })
    //   })

  } //header__menu end
  //footer add menu mobile


  var triggerFooter = document.querySelectorAll('.footer__blocks .footer__block .block__title');

  if (triggerFooter) {
    triggerFooter.forEach(trig => {
      var parent = trig.closest('.footer__block');
      var block = parent.querySelector('.block__links');
      trig.addEventListener('click', function () {
        if (window.innerWidth <= 750) {
          trig.classList.toggle('active');
          slideToggle(block, 400);

          if (!body.classList.contains('mobile')) {
            body.classList.add('mobile');
          }
        }
      });
      window.addEventListener('resize', function () {
        if (window.innerWidth > 750 && body.classList.contains('mobile')) {
          block.removeAttribute('style');
          trig.classList.remove('active');
        }
      });
    });
    window.addEventListener('resize', function () {
      if (window.innerWidth > 750) {
        body.classList.remove('mobile');
      }
    });
  } //footer add menu mobile end
  //burger


  var burger = document.querySelector('.header__burger');
  var menuHide = document.querySelector('.header-hide__menu');
  var triggersHideMenu = document.querySelectorAll('.hide-header__trigger');
  var hideMenuAdd = document.querySelectorAll('.add-header__menu');

  var closeMenu = function closeMenu() {
    menuHide.classList.remove('active');
    body.classList.remove('lock');
    html.classList.remove('lock');
    hideMenuAdd.forEach(addElem => {
      addElem.classList.remove('active');
    });
  };

  if (burger && menuHide && hideMenuAdd && triggersHideMenu) {
    var closeBurger = menuHide.querySelector('.menu__close');
    burger.addEventListener('click', function () {
      menuHide.classList.toggle('active');
      body.classList.toggle('lock');
      html.classList.toggle('lock');
    });
    closeBurger.addEventListener('click', function () {
      closeMenu();
    }); //adds

    triggersHideMenu.forEach(trigger => {
      trigger.addEventListener('click', function () {
        hideMenuAdd.forEach(add => {
          if (add.getAttribute('data-target') == trigger.getAttribute('data-target')) {
            add.classList.add('active');
          }
        });
      });
    });
    hideMenuAdd.forEach(el => {
      var closeBtn = el.querySelector('.menu__close');
      closeBtn.addEventListener('click', closeMenu);
      var backBtn = el.querySelector('.menu__back');
      backBtn.addEventListener('click', function () {
        el.classList.remove('active');
      });
    });
    window.addEventListener('resize', function () {
      if (window.innerWidth > 1024 && menuHide.classList.contains('active')) {
        closeMenu();
      }
    });
  } //burger end
  //hero mainpage swiper


  var swiper = new Swiper('.mainpage__hero .hero__slider .swiper-container', {
    loop: true,
    pagination: {
      el: '.mainpage__hero .hero__slider .swiper-pagination',
      type: 'progressbar'
    },
    autoplay: {
      delay: 4000,
      disableOnInteraction: false
    },
    navigation: {
      nextEl: '.mainpage__hero .hero__slider .swiper-button-next',
      prevEl: '.mainpage__hero .hero__slider .swiper-button-prev'
    },
    speed: 1000
  });

  if (swiper) {
    swiper.on('slideChange', function () {
      setTimeout(function () {
        var activeSlide = document.querySelector('.mainpage__hero  .swiper-slide-active');

        if (activeSlide) {
          var pagination = document.querySelector('.mainpage__hero  .swiper-pagination');
          var fill = pagination.querySelector('span');
          pagination.style.backgroundColor = activeSlide.getAttribute('data-color');
          fill.style.backgroundColor = activeSlide.getAttribute('data-fill');
        }
      }, 50);
    });
  } //hero mainpage swiper end
  //mainpage news swiper


  var swiperNews = new Swiper('.mainpage__news .news__slider .swiper-container', {
    slidesPerView: 1,
    slidesPerColumn: 1,
    speed: 1000,
    navigation: {
      nextEl: '.mainpage__news .news__slider .swiper-button-next',
      prevEl: '.mainpage__news .news__slider .swiper-button-prev'
    },
    breakpoints: {
      1025: {
        slidesPerView: 2,
        slidesPerColumn: 2
      },
      701: {
        slidesPerView: 1,
        slidesPerColumn: 2
      }
    }
  }); //mainpage news swiper end
  //mainpage info swiper
  //mainpage info swiper

  var swiperGalery = new Swiper(".mainpage__info .info__slider .swiper-container", {
    slidesPerView: 1.15,
    navigation: {
      nextEl: ".mainpage__info .info__container .swiper-button-next",
      prevEl: ".mainpage__info .info__container .swiper-button-prev"
    },
    speed: 1000,
    spaceBetween: 26,
    breakpoints: {
      1351: {
        slidesPerView: 3.4,
        spaceBetween: 36
      },
      1025: {
        spaceBetween: 36
      },
      801: {
        slidesPerView: 3,
        spaceBetween: 26
      },
      601: {
        slidesPerView: 2,
        spaceBetween: 26
      },
      440: {
        slidesPerView: 1.5,
        spaceBetween: 26
      }
    }
  }); //mainpage info swiper end
  //mainpage kalendar swiper

  var swiperKalendar = new Swiper(".mainpage__info .kalendar__slider .swiper-container", {
    slidesPerView: 1.3,
    navigation: {
      nextEl: ".mainpage__info .kalendar__container .swiper-button-next",
      prevEl: ".mainpage__info .kalendar__container .swiper-button-prev"
    },
    speed: 1000,
    spaceBetween: 26,
    breakpoints: {
      1351: {
        slidesPerView: 4.5,
        spaceBetween: 36
      },
      1025: {
        spaceBetween: 36,
        slidesPerView: 3.3
      },
      801: {
        slidesPerView: 3,
        spaceBetween: 26
      },
      601: {
        slidesPerView: 2.5,
        spaceBetween: 26
      },
      540: {
        slidesPerView: 1.5,
        spaceBetween: 26
      }
    }
  }); //mainpage kalendar swiper end
  //mainpage social swiper

  var swiperSocial = new Swiper(".mainpage__social  .kalendar__slider .swiper-container", {
    slidesPerView: 1.3,
    navigation: {
      nextEl: ".mainpage__social  .kalendar__container .swiper-button-next",
      prevEl: ".mainpage__social  .kalendar__container .swiper-button-prev"
    },
    speed: 1000,
    spaceBetween: 26,
    breakpoints: {
      1351: {
        slidesPerView: 4.5,
        spaceBetween: 36
      },
      1025: {
        spaceBetween: 36,
        slidesPerView: 3.3
      },
      801: {
        slidesPerView: 3,
        spaceBetween: 26
      },
      601: {
        slidesPerView: 2.5,
        spaceBetween: 26
      },
      540: {
        slidesPerView: 1.5,
        spaceBetween: 26
      }
    }
  }); //mainpage social swiper end

  var swiperMarks = new Swiper('.mainpage-marks  .marks__slider .swiper-container', {
    navigation: {
      nextEl: '.mainpage-marks .swiper-button-next',
      prevEl: '.mainpage-marks .swiper-button-prev'
    },
    speed: 1000,
    spaceBetween: 26,
    centerInsufficientSlides: true,
    breakpoints: {
      1024: {
        slidesPerView: 5.5
      },
      992: {
        slidesPerView: 3.5
      },
      576: {
        slidesPerView: 3.5
      },
      425: {
        slidesPerView: 2
      }
    }
  }); //mainpage info swiper end
  //mainpage kalendar swiper

  var swiperKalendar = new Swiper('.mainpage__info .kalendar__slider .swiper-container', {
    slidesPerView: 1.3,
    navigation: {
      nextEl: '.mainpage__info .kalendar__container .swiper-button-next',
      prevEl: '.mainpage__info .kalendar__container .swiper-button-prev'
    },
    speed: 1000,
    spaceBetween: 26,
    breakpoints: {
      1351: {
        slidesPerView: 4.5,
        spaceBetween: 36
      },
      1025: {
        spaceBetween: 36,
        slidesPerView: 3.3
      },
      801: {
        slidesPerView: 3,
        spaceBetween: 26
      },
      601: {
        slidesPerView: 2.5,
        spaceBetween: 26
      },
      540: {
        slidesPerView: 1.5,
        spaceBetween: 26
      }
    }
  }); //mainpage kalendar swiper end
  //mainpage social swiper

  var swiperSocial = new Swiper('.mainpage__social  .kalendar__slider .swiper-container', {
    slidesPerView: 1.3,
    navigation: {
      nextEl: '.mainpage__social  .kalendar__container .swiper-button-next',
      prevEl: '.mainpage__social  .kalendar__container .swiper-button-prev'
    },
    speed: 1000,
    spaceBetween: 26,
    breakpoints: {
      1351: {
        slidesPerView: 4.5,
        spaceBetween: 36
      },
      1025: {
        spaceBetween: 36,
        slidesPerView: 3.3
      },
      801: {
        slidesPerView: 3,
        spaceBetween: 26
      },
      601: {
        slidesPerView: 2.5,
        spaceBetween: 26
      },
      540: {
        slidesPerView: 1.5,
        spaceBetween: 26
      }
    }
  }); //mainpage social swiper end
  //cookies

  /*
  function setCookie(name,value,days) {
  var expires = "";
  if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  }
  function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
  }
    let cookiesBlock=document.querySelector(".cookies")
  let cookies_sumbited = getCookie('cookies_submited')
  if(cookiesBlock && !cookies_sumbited){
    setTimeout(()=>{
      cookiesBlock.classList.remove("no-cookies")
    },1000)
    let cookieBtn=document.querySelector(".cookies__btn")
    cookieBtn.addEventListener("click",function(){
      setCookie('cookies_submited', true, 0)
      cookiesBlock.classList.add("no-cookies")
    })
  }
  */
  //cookies end
  // contact checkbox

  var checkboxParent = document.querySelectorAll('.form-checkbox');

  if (checkboxParent) {
    checkboxParent.forEach(item => {
      var checkboxHtml = $('.wpcf7-list-item-label').html();
      $('.wpcf7-list-item-label').remove();
      var newHtml = '' + '<span class="check-retangle">' + ' <svg class="icon icon-done ">' + '   <use href="' + location.protocol + '//' + location.host + '/wp-content/themes/theme/front/assets/icon/symbol/sprite.svg#done"></use>' + ' </svg>' + '</span>' + '<span class="check__text">' + checkboxHtml + '</span>';
      $(newHtml).insertAfter('.form-checkbox .wpcf7-form-control-wrap');
      var checkSquere = item.querySelector('.check-retangle');
      var checkText = item.querySelector('.check__text');
      var checkWrapper = item.querySelector('.wpcf7-form-control-wrap');
      var linkLaw = checkText.querySelector('a');
      checkText.addEventListener('click', checkBox);
      checkSquere.addEventListener('click', checkBox);

      function checkBox(e) {
        if (linkLaw && e.target == linkLaw) {} else {
          item.classList.toggle('active');
          checkWrapper.classList.toggle('check');

          if (item.classList.contains('active')) {
            item.classList.remove('error');
          }
        }
      }
    });
  } //contact checkbox end
  //validate

  /*
  const forms = document.querySelectorAll(".form-container")
  let mailForm = document.querySelector(".mail-form")
    forms && forms.forEach(item => {
    let email = item.querySelector(".mail")
    if (email) {
      let parent = email.closest(".form-item")
      let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
      email.addEventListener("keyup", function (ev) {
        ev.stopPropagation();
        let value = email.value;
        if (value.match(pattern)) {
          parent.classList.remove("error__email")
        } else {
          parent.classList.add("error__email")
        }
        if (value.length < 1) {
          parent.classList.remove("error__email")
          }
      })
    }
      item.addEventListener('submit', function (e) {
      e.preventDefault();
  
      let valide = true;
      const inputs = item.querySelectorAll('.form-required');
      let checkbox=item.querySelector(".form-checkbox input")
      let checkboxParent=item.querySelector(".form-checkbox")
      if(checkbox){
        valide= false
        if(checkboxParent.classList.contains("active")){
          valide=true
        }else{
          checkboxParent.classList.add("error")
          valide=false
        }
      }
        inputs.forEach(element => {
          if (mailForm) {
          let email = item.querySelector(".mail")
          let formItem = email.closest(".form-item")
          if (formItem.classList.contains("error__email")) {
            valide = false
          }
        }
          // let radio=item.querySelector(".radio__parent")
        // if(radio && !radio.classList.contains("checked")){
        //   valide=false
        //   radio.classList.add("error")
  
        // }
  
        const parent = element.closest('.form-item');
        const form = element.closest(".folow__form")
        if (element.value.trim() === '') {
          parent.classList.add('error');
          if(parent.classList.contains("error-company")){
            parent.classList.remove("error")
          }
          if (form) {
            form.classList.add("error")
          }
          valide = false;
        } else {
          parent.classList.remove('error');
          if (form) {
            form.classList.remove("error")
          }
        }
      })
  
      if (valide) {
        fetch('https://jsonplaceholder.typicode.com/posts', {
          method: 'POST',
          body: new FormData(item)
        }).then(function (response) {
          if (response.ok) {
            return response.json();
          }
          return Promise.reject(response);
        }).then(function (data) {
          })["catch"](function (error) {
          console.warn(error);
        });
  
        inputs.forEach(element => {
          element.value = "";
        })
  
        }
    })
  })
  
  const inputs = document.querySelectorAll('.form-item');
  for (let i = 0; i < inputs.length; i++) {
  inputs[i].addEventListener("input", function () {
    let form = inputs[i].closest(".folow__form")
    if (form) {
      form.classList.remove("error")
    }
    inputs[i].classList.remove("error");
  })
  }
  */
  // validate end
  //school-detail popup


  var triggerPopupSchool = document.querySelector('.school-detail__about .about__link.about__modal');
  var popupSchool = document.querySelector('.school-detail__modal');

  if (triggerPopupSchool && popupSchool) {
    triggerPopupSchool.addEventListener('click', function (e) {
      e.preventDefault();
      var promise = new Promise((resolve, reject) => {
        resolve(popupSchool.style.display = 'block');
      });
      promise.then(() => {
        setTimeout(function () {
          popupSchool.classList.add('active');
          body.classList.add('lock');
          html.classList.add('lock');
        }, 50);
      });
    });
    var closePopSchool = popupSchool.querySelector('.modal__close');

    if (closePopSchool) {
      closePopSchool.addEventListener('click', function () {
        var promise = new Promise((resolve, reject) => {
          resolve(popupSchool.classList.remove('active'));
        });
        promise.then(() => {
          setTimeout(function () {
            popupSchool.style.display = 'none';
            body.classList.remove('lock');
            html.classList.remove('lock');
          }, 550);
        });
      });
    }
  } //school-detail popup end
  //posibility slider


  var swiperPosibility = new Swiper('.posibility__class .swiper-container', {
    slidesPerView: 1.25,
    navigation: {
      nextEl: '.posibility__class .swiper-button-nextp',
      prevEl: '.posibility__class .swiper-button-prevp'
    },
    speed: 1000,
    spaceBetween: 26,
    breakpoints: {
      1351: {
        slidesPerView: 2.7,
        spaceBetween: 32
      },
      1025: {
        spaceBetween: 32,
        slidesPerView: 2.2
      },
      851: {
        slidesPerView: 3.2,
        spaceBetween: 32
      },
      691: {
        slidesPerView: 2.5,
        spaceBetween: 32
      },
      540: {
        slidesPerView: 1.5,
        spaceBetween: 32
      }
    }
  }); //posibility slider end
  //news slider

  var swiperSocial = new Swiper('.news__social .kalendar__slider .swiper-container', {
    slidesPerView: 1.3,
    navigation: {
      nextEl: '.news__social .kalendar__container .swiper-button-next',
      prevEl: '.news__social .kalendar__container .swiper-button-prev'
    },
    speed: 1000,
    spaceBetween: 26,
    breakpoints: {
      1351: {
        slidesPerView: 4.5,
        spaceBetween: 36
      },
      1025: {
        spaceBetween: 36,
        slidesPerView: 3.3
      },
      801: {
        slidesPerView: 3,
        spaceBetween: 26
      },
      601: {
        slidesPerView: 2.5,
        spaceBetween: 26
      },
      540: {
        slidesPerView: 1.5,
        spaceBetween: 26
      }
    }
  }); //news slider end
  //contact map

  var mapContainerCon = document.getElementById('map');

  if (mapContainerCon) {
    var latitude = $('#map').data('latitude');
    var longitude = $('#map').data('longitude');
    var marker = $('#map').data('marker');
    var zoom = $('#map').data('zoom');

    var initMap = () => {
      var mymap = L.map('map').setView([latitude, longitude], zoom);
      var icon = L.icon({
        iconUrl: marker,
        iconSize: [80, 80]
      });
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);
      L.marker([latitude, longitude], {
        icon: icon
      }).addTo(mymap);
      mymap.scrollWheelZoom.disable();
    };

    mapContainerCon && initMap();
  } //contact map end
  //news slider


  var swiperProject = new Swiper('.project-detail__projects .project__slider .swiper-container', {
    slidesPerView: 1,
    navigation: {
      nextEl: '.project-detail__projects .project__slider .button-next',
      prevEl: '.project-detail__projects .project__slider .button-prev'
    },
    speed: 1000,
    spaceBetween: 0,
    breakpoints: {
      1251: {
        slidesPerView: 1.7,
        spaceBetween: 7
      },
      1025: {
        spaceBetween: 7,
        slidesPerView: 1.3
      },
      801: {
        slidesPerView: 1.1,
        spaceBetween: 0
      },
      601: {
        slidesPerView: 1.5,
        spaceBetween: 0
      },
      570: {
        slidesPerView: 1.1,
        spaceBetween: 0
      },
      350: {
        slidesPerView: 1.1,
        spaceBetween: 0
      }
    }
  }); //news slider end
  // datepicker

  $(function () {
    $('#datepicker-od').datepicker({
      dateFormat: 'dd-mm-yy',
      duration: 'fast',
      onSelect: () => {
        document.getElementById('datepicker-od').dispatchEvent(new Event('change'));
      }
    });
  });
  $(function () {
    $('#datepicker-do').datepicker({
      dateFormat: 'dd-mm-yy',
      duration: 'fast',
      onSelect: () => {
        document.getElementById('datepicker-do').dispatchEvent(new Event('change'));
      }
    });
  });
  var inputsNews = document.querySelectorAll('.news__selects .select__input');

  if (inputsNews) {
    inputsNews.forEach(input => {
      input.addEventListener('change', function () {
        var parent = input.closest('.form__item');

        if (!parent.classList.contains('active')) {
          parent.classList.add('active');
        }

        var close = parent.querySelector('.select__close');
        close && close.addEventListener('click', function () {
          parent.classList.remove('active');
          input.value = '';
        });
      });
    });
  }

  var triggersInputsNews = this.document.querySelectorAll('.news__selects .form-trigger'); // lightgallery
  // let lightgalleryBoxes = document.querySelectorAll(".lightgallery")
  // if(lightgalleryBoxes){
  //   lightgalleryBoxes.forEach(lgBox=>{
  //     $(lgBox).lightGallery({    
  //       videojs: true,
  //       videojsOptions: {
  //         controls:true
  //       },
  //       speed: 500,
  //     });
  //   })
  // }
  // init gallery fancybox https://fancyapps.com/fancybox/getting-started/

  var fancyboxItems = document.querySelectorAll('[data-fancybox]');

  if (fancyboxItems) {
    Fancybox.bind('[data-fancybox]', {// Your custom options
    });
  } // lightgallery end
  // Create mobile link on news pages on news item


  if (window.innerWidth < 991) {
    var $slideItems = document.querySelectorAll('.slide__item');
    $slideItems.forEach($sliderItem => {
      $sliderItem.addEventListener('click', event => {
        var $slideTitle = $sliderItem.querySelector('.item__title');
        var slideTitleValue = $slideTitle.getAttribute('href');
        window.location.assign(slideTitleValue);
      });
    });
  }
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOlsid2luZG93IiwiYWRkRXZlbnRMaXN0ZW5lciIsInNsaWRlVXAiLCJ0YXJnZXQiLCJkdXJhdGlvbiIsInN0eWxlIiwidHJhbnNpdGlvblByb3BlcnR5IiwidHJhbnNpdGlvbkR1cmF0aW9uIiwiYm94U2l6aW5nIiwiaGVpZ2h0Iiwib2Zmc2V0SGVpZ2h0Iiwib3ZlcmZsb3ciLCJwYWRkaW5nVG9wIiwicGFkZGluZ0JvdHRvbSIsIm1hcmdpblRvcCIsIm1hcmdpbkJvdHRvbSIsInNldFRpbWVvdXQiLCJkaXNwbGF5IiwicmVtb3ZlUHJvcGVydHkiLCJzbGlkZURvd24iLCJnZXRDb21wdXRlZFN0eWxlIiwic2xpZGVUb2dnbGUiLCIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsInN2ZzRldmVyeWJvZHkiLCJqUXVlcnkiLCJib2R5IiwicXVlcnlTZWxlY3RvciIsImh0bWwiLCJoZWFkZXIiLCJzY3JvbGxlZCIsInNjcm9sbFkiLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmUiLCJ0cmlnZ2VycyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJ3cmFwcCIsImZvckVhY2giLCJ0cmlnIiwidHJpZ2dlckZvb3RlciIsInBhcmVudCIsImNsb3Nlc3QiLCJibG9jayIsImlubmVyV2lkdGgiLCJ0b2dnbGUiLCJjb250YWlucyIsInJlbW92ZUF0dHJpYnV0ZSIsImJ1cmdlciIsIm1lbnVIaWRlIiwidHJpZ2dlcnNIaWRlTWVudSIsImhpZGVNZW51QWRkIiwiY2xvc2VNZW51IiwiYWRkRWxlbSIsImNsb3NlQnVyZ2VyIiwidHJpZ2dlciIsImdldEF0dHJpYnV0ZSIsImVsIiwiY2xvc2VCdG4iLCJiYWNrQnRuIiwic3dpcGVyIiwiU3dpcGVyIiwibG9vcCIsInBhZ2luYXRpb24iLCJ0eXBlIiwiYXV0b3BsYXkiLCJkZWxheSIsImRpc2FibGVPbkludGVyYWN0aW9uIiwibmF2aWdhdGlvbiIsIm5leHRFbCIsInByZXZFbCIsInNwZWVkIiwib24iLCJhY3RpdmVTbGlkZSIsImZpbGwiLCJiYWNrZ3JvdW5kQ29sb3IiLCJzd2lwZXJOZXdzIiwic2xpZGVzUGVyVmlldyIsInNsaWRlc1BlckNvbHVtbiIsImJyZWFrcG9pbnRzIiwic3dpcGVyR2FsZXJ5Iiwic3BhY2VCZXR3ZWVuIiwic3dpcGVyS2FsZW5kYXIiLCJzd2lwZXJTb2NpYWwiLCJzd2lwZXJNYXJrcyIsImNlbnRlckluc3VmZmljaWVudFNsaWRlcyIsImNoZWNrYm94UGFyZW50IiwiaXRlbSIsImNoZWNrYm94SHRtbCIsIm5ld0h0bWwiLCJsb2NhdGlvbiIsInByb3RvY29sIiwiaG9zdCIsImluc2VydEFmdGVyIiwiY2hlY2tTcXVlcmUiLCJjaGVja1RleHQiLCJjaGVja1dyYXBwZXIiLCJsaW5rTGF3IiwiY2hlY2tCb3giLCJlIiwidHJpZ2dlclBvcHVwU2Nob29sIiwicG9wdXBTY2hvb2wiLCJwcmV2ZW50RGVmYXVsdCIsInByb21pc2UiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsInRoZW4iLCJjbG9zZVBvcFNjaG9vbCIsInN3aXBlclBvc2liaWxpdHkiLCJtYXBDb250YWluZXJDb24iLCJnZXRFbGVtZW50QnlJZCIsImxhdGl0dWRlIiwiZGF0YSIsImxvbmdpdHVkZSIsIm1hcmtlciIsInpvb20iLCJpbml0TWFwIiwibXltYXAiLCJMIiwibWFwIiwic2V0VmlldyIsImljb24iLCJpY29uVXJsIiwiaWNvblNpemUiLCJ0aWxlTGF5ZXIiLCJhZGRUbyIsInNjcm9sbFdoZWVsWm9vbSIsImRpc2FibGUiLCJzd2lwZXJQcm9qZWN0IiwiZGF0ZXBpY2tlciIsImRhdGVGb3JtYXQiLCJvblNlbGVjdCIsImRpc3BhdGNoRXZlbnQiLCJFdmVudCIsImlucHV0c05ld3MiLCJpbnB1dCIsImNsb3NlIiwidmFsdWUiLCJ0cmlnZ2Vyc0lucHV0c05ld3MiLCJmYW5jeWJveEl0ZW1zIiwiRmFuY3lib3giLCJiaW5kIiwiJHNsaWRlSXRlbXMiLCIkc2xpZGVySXRlbSIsImV2ZW50IiwiJHNsaWRlVGl0bGUiLCJzbGlkZVRpdGxlVmFsdWUiLCJhc3NpZ24iXSwibWFwcGluZ3MiOiI7O0FBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsTUFBeEIsRUFBZ0MsWUFBWTtBQUMxQztBQUNBLE1BQU1DLE9BQU8sR0FBRyxTQUFWQSxPQUFVLENBQUNDLE1BQUQsRUFBNEI7QUFBQSxRQUFuQkMsUUFBbUIsdUVBQVIsR0FBUTtBQUMxQ0QsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFDLGtCQUFiLEdBQWtDLHlCQUFsQztBQUNBSCxJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYUUsa0JBQWIsR0FBa0NILFFBQVEsR0FBRyxJQUE3QztBQUNBRCxJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYUcsU0FBYixHQUF5QixZQUF6QjtBQUNBTCxJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYUksTUFBYixHQUFzQk4sTUFBTSxDQUFDTyxZQUFQLEdBQXNCLElBQTVDO0FBQ0FQLElBQUFBLE1BQU0sQ0FBQ08sWUFBUDtBQUNBUCxJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYU0sUUFBYixHQUF3QixRQUF4QjtBQUNBUixJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYUksTUFBYixHQUFzQixDQUF0QjtBQUNBTixJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYU8sVUFBYixHQUEwQixDQUExQjtBQUNBVCxJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYVEsYUFBYixHQUE2QixDQUE3QjtBQUNBVixJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYVMsU0FBYixHQUF5QixDQUF6QjtBQUNBWCxJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYVUsWUFBYixHQUE0QixDQUE1QjtBQUNBZixJQUFBQSxNQUFNLENBQUNnQixVQUFQLENBQWtCLE1BQU07QUFDdEJiLE1BQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhWSxPQUFiLEdBQXVCLE1BQXZCO0FBQ0FkLE1BQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhYSxjQUFiLENBQTRCLFFBQTVCO0FBQ0FmLE1BQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhYSxjQUFiLENBQTRCLGFBQTVCO0FBQ0FmLE1BQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhYSxjQUFiLENBQTRCLGdCQUE1QjtBQUNBZixNQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYWEsY0FBYixDQUE0QixZQUE1QjtBQUNBZixNQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYWEsY0FBYixDQUE0QixlQUE1QjtBQUNBZixNQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYWEsY0FBYixDQUE0QixVQUE1QjtBQUNBZixNQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYWEsY0FBYixDQUE0QixxQkFBNUI7QUFDQWYsTUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFhLGNBQWIsQ0FBNEIscUJBQTVCLEVBVHNCLENBVXRCO0FBQ0QsS0FYRCxFQVdHZCxRQVhIO0FBWUQsR0F4QkQ7QUEwQkE7OztBQUNBLE1BQU1lLFNBQVMsR0FBRyxTQUFaQSxTQUFZLENBQUNoQixNQUFELEVBQTRCO0FBQUEsUUFBbkJDLFFBQW1CLHVFQUFSLEdBQVE7QUFDNUNELElBQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhYSxjQUFiLENBQTRCLFNBQTVCO0FBQ0EsUUFBSUQsT0FBTyxHQUFHakIsTUFBTSxDQUFDb0IsZ0JBQVAsQ0FBd0JqQixNQUF4QixFQUFnQ2MsT0FBOUM7QUFDQSxRQUFJQSxPQUFPLEtBQUssTUFBaEIsRUFBd0JBLE9BQU8sR0FBRyxNQUFWO0FBQ3hCZCxJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYVksT0FBYixHQUF1QkEsT0FBdkI7QUFDQSxRQUFJUixNQUFNLEdBQUdOLE1BQU0sQ0FBQ08sWUFBcEI7QUFDQVAsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFNLFFBQWIsR0FBd0IsUUFBeEI7QUFDQVIsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFJLE1BQWIsR0FBc0IsQ0FBdEI7QUFDQU4sSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFPLFVBQWIsR0FBMEIsQ0FBMUI7QUFDQVQsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFRLGFBQWIsR0FBNkIsQ0FBN0I7QUFDQVYsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFTLFNBQWIsR0FBeUIsQ0FBekI7QUFDQVgsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFVLFlBQWIsR0FBNEIsQ0FBNUI7QUFDQVosSUFBQUEsTUFBTSxDQUFDTyxZQUFQO0FBQ0FQLElBQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhRyxTQUFiLEdBQXlCLFlBQXpCO0FBQ0FMLElBQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhQyxrQkFBYixHQUFrQyx5QkFBbEM7QUFDQUgsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFFLGtCQUFiLEdBQWtDSCxRQUFRLEdBQUcsSUFBN0M7QUFDQUQsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFJLE1BQWIsR0FBc0JBLE1BQU0sR0FBRyxJQUEvQjtBQUNBTixJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYWEsY0FBYixDQUE0QixhQUE1QjtBQUNBZixJQUFBQSxNQUFNLENBQUNFLEtBQVAsQ0FBYWEsY0FBYixDQUE0QixnQkFBNUI7QUFDQWYsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFhLGNBQWIsQ0FBNEIsWUFBNUI7QUFDQWYsSUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFhLGNBQWIsQ0FBNEIsZUFBNUI7QUFDQWxCLElBQUFBLE1BQU0sQ0FBQ2dCLFVBQVAsQ0FBa0IsTUFBTTtBQUN0QmIsTUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFhLGNBQWIsQ0FBNEIsUUFBNUI7QUFDQWYsTUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFhLGNBQWIsQ0FBNEIsVUFBNUI7QUFDQWYsTUFBQUEsTUFBTSxDQUFDRSxLQUFQLENBQWFhLGNBQWIsQ0FBNEIscUJBQTVCO0FBQ0FmLE1BQUFBLE1BQU0sQ0FBQ0UsS0FBUCxDQUFhYSxjQUFiLENBQTRCLHFCQUE1QjtBQUNELEtBTEQsRUFLR2QsUUFMSDtBQU1ELEdBM0JEO0FBNkJBOzs7QUFDQSxNQUFNaUIsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBQ2xCLE1BQUQsRUFBNEI7QUFBQSxRQUFuQkMsUUFBbUIsdUVBQVIsR0FBUTs7QUFDOUMsUUFBSUosTUFBTSxDQUFDb0IsZ0JBQVAsQ0FBd0JqQixNQUF4QixFQUFnQ2MsT0FBaEMsS0FBNEMsTUFBaEQsRUFBd0Q7QUFDdEQsYUFBT0UsU0FBUyxDQUFDaEIsTUFBRCxFQUFTQyxRQUFULENBQWhCO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBT0YsT0FBTyxDQUFDQyxNQUFELEVBQVNDLFFBQVQsQ0FBZDtBQUNEO0FBQ0YsR0FORDs7QUFRQSxHQUFDLFVBQVVrQixDQUFWLEVBQWE7QUFDWkEsSUFBQUEsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzVCQyxNQUFBQSxhQUFhLENBQUMsRUFBRCxDQUFiO0FBQ0QsS0FGRDtBQUdELEdBSkQsRUFJR0MsTUFKSCxFQW5FMEMsQ0F5RTFDOzs7QUFFQSxNQUFJQyxJQUFJLEdBQUdKLFFBQVEsQ0FBQ0ssYUFBVCxDQUF1QixNQUF2QixDQUFYO0FBQ0EsTUFBSUMsSUFBSSxHQUFHTixRQUFRLENBQUNLLGFBQVQsQ0FBdUIsTUFBdkIsQ0FBWDtBQUNBLE1BQUlFLE1BQU0sR0FBR1AsUUFBUSxDQUFDSyxhQUFULENBQXVCLGVBQXZCLENBQWI7QUFDQSxNQUFJRyxRQUFRLEdBQUcsS0FBZjs7QUFDQSxNQUFJRCxNQUFKLEVBQVk7QUFDVjlCLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBWTtBQUM1QyxVQUFJRCxNQUFNLENBQUNnQyxPQUFQLEdBQWlCLENBQXJCLEVBQXdCO0FBQ3RCRixRQUFBQSxNQUFNLENBQUNHLFNBQVAsQ0FBaUJDLEdBQWpCLENBQXFCLFFBQXJCO0FBQ0FILFFBQUFBLFFBQVEsR0FBRyxJQUFYO0FBQ0QsT0FIRCxNQUdPO0FBQ0xELFFBQUFBLE1BQU0sQ0FBQ0csU0FBUCxDQUFpQkUsTUFBakIsQ0FBd0IsUUFBeEI7QUFDQUosUUFBQUEsUUFBUSxHQUFHLEtBQVg7QUFDRDtBQUNGLEtBUkQ7QUFVQS9CLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsTUFBeEIsRUFBZ0MsWUFBWTtBQUMxQyxVQUFJRCxNQUFNLENBQUNnQyxPQUFQLEdBQWlCLENBQXJCLEVBQXdCO0FBQ3RCRixRQUFBQSxNQUFNLENBQUNHLFNBQVAsQ0FBaUJDLEdBQWpCLENBQXFCLFFBQXJCO0FBQ0QsT0FGRCxNQUVPO0FBQ0xKLFFBQUFBLE1BQU0sQ0FBQ0csU0FBUCxDQUFpQkUsTUFBakIsQ0FBd0IsUUFBeEI7QUFDRDtBQUNGLEtBTkQ7QUFRQSxRQUFJQyxRQUFRLEdBQUdOLE1BQU0sQ0FBQ08sZ0JBQVAsQ0FBd0IsZ0JBQXhCLENBQWY7QUFDQSxRQUFJQyxLQUFLLEdBQUdmLFFBQVEsQ0FBQ0ssYUFBVCxDQUF1QixnQkFBdkIsQ0FBWjs7QUFDQSxRQUFJUSxRQUFRLElBQUlFLEtBQWhCLEVBQXVCO0FBQ3JCRixNQUFBQSxRQUFRLENBQUNHLE9BQVQsQ0FBa0JDLElBQUQsSUFBVTtBQUN6QkEsUUFBQUEsSUFBSSxDQUFDdkMsZ0JBQUwsQ0FBc0IsWUFBdEIsRUFBb0MsWUFBWTtBQUM5Q3FDLFVBQUFBLEtBQUssQ0FBQ0wsU0FBTixDQUFnQkMsR0FBaEIsQ0FBb0IsUUFBcEI7QUFDRCxTQUZEO0FBSUFNLFFBQUFBLElBQUksQ0FBQ3ZDLGdCQUFMLENBQXNCLFlBQXRCLEVBQW9DLFlBQVk7QUFDOUNxQyxVQUFBQSxLQUFLLENBQUNMLFNBQU4sQ0FBZ0JFLE1BQWhCLENBQXVCLFFBQXZCO0FBQ0QsU0FGRDtBQUdELE9BUkQ7QUFTRCxLQS9CUyxDQWdDVjtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDRCxHQTNMeUMsQ0E2TDFDO0FBRUE7OztBQUVBLE1BQUlNLGFBQWEsR0FBR2xCLFFBQVEsQ0FBQ2MsZ0JBQVQsQ0FBMEIsOENBQTFCLENBQXBCOztBQUVBLE1BQUlJLGFBQUosRUFBbUI7QUFDakJBLElBQUFBLGFBQWEsQ0FBQ0YsT0FBZCxDQUF1QkMsSUFBRCxJQUFVO0FBQzlCLFVBQUlFLE1BQU0sR0FBR0YsSUFBSSxDQUFDRyxPQUFMLENBQWEsZ0JBQWIsQ0FBYjtBQUNBLFVBQUlDLEtBQUssR0FBR0YsTUFBTSxDQUFDZCxhQUFQLENBQXFCLGVBQXJCLENBQVo7QUFDQVksTUFBQUEsSUFBSSxDQUFDdkMsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsWUFBWTtBQUN6QyxZQUFJRCxNQUFNLENBQUM2QyxVQUFQLElBQXFCLEdBQXpCLEVBQThCO0FBQzVCTCxVQUFBQSxJQUFJLENBQUNQLFNBQUwsQ0FBZWEsTUFBZixDQUFzQixRQUF0QjtBQUNBekIsVUFBQUEsV0FBVyxDQUFDdUIsS0FBRCxFQUFRLEdBQVIsQ0FBWDs7QUFDQSxjQUFJLENBQUNqQixJQUFJLENBQUNNLFNBQUwsQ0FBZWMsUUFBZixDQUF3QixRQUF4QixDQUFMLEVBQXdDO0FBQ3RDcEIsWUFBQUEsSUFBSSxDQUFDTSxTQUFMLENBQWVDLEdBQWYsQ0FBbUIsUUFBbkI7QUFDRDtBQUNGO0FBQ0YsT0FSRDtBQVNBbEMsTUFBQUEsTUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFlBQUlELE1BQU0sQ0FBQzZDLFVBQVAsR0FBb0IsR0FBcEIsSUFBMkJsQixJQUFJLENBQUNNLFNBQUwsQ0FBZWMsUUFBZixDQUF3QixRQUF4QixDQUEvQixFQUFrRTtBQUNoRUgsVUFBQUEsS0FBSyxDQUFDSSxlQUFOLENBQXNCLE9BQXRCO0FBQ0FSLFVBQUFBLElBQUksQ0FBQ1AsU0FBTCxDQUFlRSxNQUFmLENBQXNCLFFBQXRCO0FBQ0Q7QUFDRixPQUxEO0FBTUQsS0FsQkQ7QUFtQkFuQyxJQUFBQSxNQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsVUFBSUQsTUFBTSxDQUFDNkMsVUFBUCxHQUFvQixHQUF4QixFQUE2QjtBQUMzQmxCLFFBQUFBLElBQUksQ0FBQ00sU0FBTCxDQUFlRSxNQUFmLENBQXNCLFFBQXRCO0FBQ0Q7QUFDRixLQUpEO0FBS0QsR0E1TnlDLENBOE4xQztBQUVBOzs7QUFFQSxNQUFJYyxNQUFNLEdBQUcxQixRQUFRLENBQUNLLGFBQVQsQ0FBdUIsaUJBQXZCLENBQWI7QUFDQSxNQUFJc0IsUUFBUSxHQUFHM0IsUUFBUSxDQUFDSyxhQUFULENBQXVCLG9CQUF2QixDQUFmO0FBQ0EsTUFBSXVCLGdCQUFnQixHQUFHNUIsUUFBUSxDQUFDYyxnQkFBVCxDQUEwQix1QkFBMUIsQ0FBdkI7QUFDQSxNQUFJZSxXQUFXLEdBQUc3QixRQUFRLENBQUNjLGdCQUFULENBQTBCLG1CQUExQixDQUFsQjs7QUFFQSxNQUFJZ0IsU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBWTtBQUMxQkgsSUFBQUEsUUFBUSxDQUFDakIsU0FBVCxDQUFtQkUsTUFBbkIsQ0FBMEIsUUFBMUI7QUFDQVIsSUFBQUEsSUFBSSxDQUFDTSxTQUFMLENBQWVFLE1BQWYsQ0FBc0IsTUFBdEI7QUFDQU4sSUFBQUEsSUFBSSxDQUFDSSxTQUFMLENBQWVFLE1BQWYsQ0FBc0IsTUFBdEI7QUFDQWlCLElBQUFBLFdBQVcsQ0FBQ2IsT0FBWixDQUFxQmUsT0FBRCxJQUFhO0FBQy9CQSxNQUFBQSxPQUFPLENBQUNyQixTQUFSLENBQWtCRSxNQUFsQixDQUF5QixRQUF6QjtBQUNELEtBRkQ7QUFHRCxHQVBEOztBQVFBLE1BQUljLE1BQU0sSUFBSUMsUUFBVixJQUFzQkUsV0FBdEIsSUFBcUNELGdCQUF6QyxFQUEyRDtBQUN6RCxRQUFJSSxXQUFXLEdBQUdMLFFBQVEsQ0FBQ3RCLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBbEI7QUFDQXFCLElBQUFBLE1BQU0sQ0FBQ2hELGdCQUFQLENBQXdCLE9BQXhCLEVBQWlDLFlBQVk7QUFDM0NpRCxNQUFBQSxRQUFRLENBQUNqQixTQUFULENBQW1CYSxNQUFuQixDQUEwQixRQUExQjtBQUNBbkIsTUFBQUEsSUFBSSxDQUFDTSxTQUFMLENBQWVhLE1BQWYsQ0FBc0IsTUFBdEI7QUFDQWpCLE1BQUFBLElBQUksQ0FBQ0ksU0FBTCxDQUFlYSxNQUFmLENBQXNCLE1BQXRCO0FBQ0QsS0FKRDtBQU1BUyxJQUFBQSxXQUFXLENBQUN0RCxnQkFBWixDQUE2QixPQUE3QixFQUFzQyxZQUFZO0FBQ2hEb0QsTUFBQUEsU0FBUztBQUNWLEtBRkQsRUFSeUQsQ0FZekQ7O0FBQ0FGLElBQUFBLGdCQUFnQixDQUFDWixPQUFqQixDQUEwQmlCLE9BQUQsSUFBYTtBQUNwQ0EsTUFBQUEsT0FBTyxDQUFDdkQsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1Q21ELFFBQUFBLFdBQVcsQ0FBQ2IsT0FBWixDQUFxQkwsR0FBRCxJQUFTO0FBQzNCLGNBQUlBLEdBQUcsQ0FBQ3VCLFlBQUosQ0FBaUIsYUFBakIsS0FBbUNELE9BQU8sQ0FBQ0MsWUFBUixDQUFxQixhQUFyQixDQUF2QyxFQUE0RTtBQUMxRXZCLFlBQUFBLEdBQUcsQ0FBQ0QsU0FBSixDQUFjQyxHQUFkLENBQWtCLFFBQWxCO0FBQ0Q7QUFDRixTQUpEO0FBS0QsT0FORDtBQU9ELEtBUkQ7QUFTQWtCLElBQUFBLFdBQVcsQ0FBQ2IsT0FBWixDQUFxQm1CLEVBQUQsSUFBUTtBQUMxQixVQUFJQyxRQUFRLEdBQUdELEVBQUUsQ0FBQzlCLGFBQUgsQ0FBaUIsY0FBakIsQ0FBZjtBQUNBK0IsTUFBQUEsUUFBUSxDQUFDMUQsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUNvRCxTQUFuQztBQUNBLFVBQUlPLE9BQU8sR0FBR0YsRUFBRSxDQUFDOUIsYUFBSCxDQUFpQixhQUFqQixDQUFkO0FBQ0FnQyxNQUFBQSxPQUFPLENBQUMzRCxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDeUQsUUFBQUEsRUFBRSxDQUFDekIsU0FBSCxDQUFhRSxNQUFiLENBQW9CLFFBQXBCO0FBQ0QsT0FGRDtBQUdELEtBUEQ7QUFTQW5DLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBWTtBQUM1QyxVQUFJRCxNQUFNLENBQUM2QyxVQUFQLEdBQW9CLElBQXBCLElBQTRCSyxRQUFRLENBQUNqQixTQUFULENBQW1CYyxRQUFuQixDQUE0QixRQUE1QixDQUFoQyxFQUF1RTtBQUNyRU0sUUFBQUEsU0FBUztBQUNWO0FBQ0YsS0FKRDtBQUtELEdBblJ5QyxDQXFSMUM7QUFFQTs7O0FBRUEsTUFBSVEsTUFBTSxHQUFHLElBQUlDLE1BQUosQ0FBVyxpREFBWCxFQUE4RDtBQUN6RUMsSUFBQUEsSUFBSSxFQUFFLElBRG1FO0FBRXpFQyxJQUFBQSxVQUFVLEVBQUU7QUFDVk4sTUFBQUEsRUFBRSxFQUFFLGtEQURNO0FBRVZPLE1BQUFBLElBQUksRUFBRTtBQUZJLEtBRjZEO0FBTXpFQyxJQUFBQSxRQUFRLEVBQUU7QUFDUkMsTUFBQUEsS0FBSyxFQUFFLElBREM7QUFFUkMsTUFBQUEsb0JBQW9CLEVBQUU7QUFGZCxLQU4rRDtBQVV6RUMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLE1BQU0sRUFBRSxtREFERTtBQUVWQyxNQUFBQSxNQUFNLEVBQUU7QUFGRSxLQVY2RDtBQWN6RUMsSUFBQUEsS0FBSyxFQUFFO0FBZGtFLEdBQTlELENBQWI7O0FBZ0JBLE1BQUlYLE1BQUosRUFBWTtBQUNWQSxJQUFBQSxNQUFNLENBQUNZLEVBQVAsQ0FBVSxhQUFWLEVBQXlCLFlBQVk7QUFDbkN6RCxNQUFBQSxVQUFVLENBQUMsWUFBWTtBQUNyQixZQUFJMEQsV0FBVyxHQUFHbkQsUUFBUSxDQUFDSyxhQUFULENBQXVCLHVDQUF2QixDQUFsQjs7QUFDQSxZQUFJOEMsV0FBSixFQUFpQjtBQUNmLGNBQUlWLFVBQVUsR0FBR3pDLFFBQVEsQ0FBQ0ssYUFBVCxDQUF1QixxQ0FBdkIsQ0FBakI7QUFDQSxjQUFJK0MsSUFBSSxHQUFHWCxVQUFVLENBQUNwQyxhQUFYLENBQXlCLE1BQXpCLENBQVg7QUFDQW9DLFVBQUFBLFVBQVUsQ0FBQzNELEtBQVgsQ0FBaUJ1RSxlQUFqQixHQUFtQ0YsV0FBVyxDQUFDakIsWUFBWixDQUF5QixZQUF6QixDQUFuQztBQUNBa0IsVUFBQUEsSUFBSSxDQUFDdEUsS0FBTCxDQUFXdUUsZUFBWCxHQUE2QkYsV0FBVyxDQUFDakIsWUFBWixDQUF5QixXQUF6QixDQUE3QjtBQUNEO0FBQ0YsT0FSUyxFQVFQLEVBUk8sQ0FBVjtBQVNELEtBVkQ7QUFXRCxHQXJUeUMsQ0F1VDFDO0FBRUE7OztBQUVBLE1BQUlvQixVQUFVLEdBQUcsSUFBSWYsTUFBSixDQUFXLGlEQUFYLEVBQThEO0FBQzdFZ0IsSUFBQUEsYUFBYSxFQUFFLENBRDhEO0FBRTdFQyxJQUFBQSxlQUFlLEVBQUUsQ0FGNEQ7QUFHN0VQLElBQUFBLEtBQUssRUFBRSxJQUhzRTtBQUk3RUgsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLE1BQU0sRUFBRSxtREFERTtBQUVWQyxNQUFBQSxNQUFNLEVBQUU7QUFGRSxLQUppRTtBQVE3RVMsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsQ0FEWDtBQUVKQyxRQUFBQSxlQUFlLEVBQUU7QUFGYixPQURLO0FBS1gsV0FBSztBQUNIRCxRQUFBQSxhQUFhLEVBQUUsQ0FEWjtBQUVIQyxRQUFBQSxlQUFlLEVBQUU7QUFGZDtBQUxNO0FBUmdFLEdBQTlELENBQWpCLENBM1QwQyxDQStVMUM7QUFFQTtBQUVGOztBQUVBLE1BQUlFLFlBQVksR0FBRyxJQUFJbkIsTUFBSixDQUFXLGlEQUFYLEVBQThEO0FBQy9FZ0IsSUFBQUEsYUFBYSxFQUFFLElBRGdFO0FBRS9FVCxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsTUFBTSxFQUFFLHNEQURFO0FBRVZDLE1BQUFBLE1BQU0sRUFBRTtBQUZFLEtBRm1FO0FBTS9FQyxJQUFBQSxLQUFLLEVBQUMsSUFOeUU7QUFPL0VVLElBQUFBLFlBQVksRUFBRSxFQVBpRTtBQVEvRUYsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsR0FEWDtBQUVKSSxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQURLO0FBS1gsWUFBSztBQUNIQSxRQUFBQSxZQUFZLEVBQUM7QUFEVixPQUxNO0FBUVgsV0FBSTtBQUNGSixRQUFBQSxhQUFhLEVBQUUsQ0FEYjtBQUVGSSxRQUFBQSxZQUFZLEVBQUU7QUFGWixPQVJPO0FBWVgsV0FBSTtBQUNGSixRQUFBQSxhQUFhLEVBQUUsQ0FEYjtBQUVGSSxRQUFBQSxZQUFZLEVBQUU7QUFGWixPQVpPO0FBZ0JYLFdBQUk7QUFDRkosUUFBQUEsYUFBYSxFQUFFLEdBRGI7QUFFRkksUUFBQUEsWUFBWSxFQUFFO0FBRlo7QUFoQk87QUFSa0UsR0FBOUQsQ0FBbkIsQ0FyVjRDLENBcVg1QztBQUdBOztBQUVBLE1BQUlDLGNBQWMsR0FBRyxJQUFJckIsTUFBSixDQUFXLHFEQUFYLEVBQWtFO0FBQ3JGZ0IsSUFBQUEsYUFBYSxFQUFFLEdBRHNFO0FBRXJGVCxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsTUFBTSxFQUFFLDBEQURFO0FBRVZDLE1BQUFBLE1BQU0sRUFBRTtBQUZFLEtBRnlFO0FBTXJGQyxJQUFBQSxLQUFLLEVBQUMsSUFOK0U7QUFPckZVLElBQUFBLFlBQVksRUFBRSxFQVB1RTtBQVFyRkYsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsR0FEWDtBQUVKSSxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQURLO0FBS1gsWUFBSztBQUNIQSxRQUFBQSxZQUFZLEVBQUMsRUFEVjtBQUVISixRQUFBQSxhQUFhLEVBQUU7QUFGWixPQUxNO0FBU1gsV0FBSTtBQUNGQSxRQUFBQSxhQUFhLEVBQUUsQ0FEYjtBQUVGSSxRQUFBQSxZQUFZLEVBQUU7QUFGWixPQVRPO0FBYVgsV0FBSTtBQUNGSixRQUFBQSxhQUFhLEVBQUUsR0FEYjtBQUVGSSxRQUFBQSxZQUFZLEVBQUU7QUFGWixPQWJPO0FBaUJYLFdBQUk7QUFDRkosUUFBQUEsYUFBYSxFQUFFLEdBRGI7QUFFRkksUUFBQUEsWUFBWSxFQUFFO0FBRlo7QUFqQk87QUFSd0UsR0FBbEUsQ0FBckIsQ0ExWDRDLENBMlo1QztBQUdBOztBQUVBLE1BQUlFLFlBQVksR0FBRyxJQUFJdEIsTUFBSixDQUFXLHdEQUFYLEVBQXFFO0FBQ3RGZ0IsSUFBQUEsYUFBYSxFQUFFLEdBRHVFO0FBRXRGVCxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsTUFBTSxFQUFFLDZEQURFO0FBRVZDLE1BQUFBLE1BQU0sRUFBRTtBQUZFLEtBRjBFO0FBTXRGQyxJQUFBQSxLQUFLLEVBQUMsSUFOZ0Y7QUFPdEZVLElBQUFBLFlBQVksRUFBRSxFQVB3RTtBQVF0RkYsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsR0FEWDtBQUVKSSxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQURLO0FBS1gsWUFBSztBQUNIQSxRQUFBQSxZQUFZLEVBQUMsRUFEVjtBQUVISixRQUFBQSxhQUFhLEVBQUU7QUFGWixPQUxNO0FBU1gsV0FBSTtBQUNGQSxRQUFBQSxhQUFhLEVBQUUsQ0FEYjtBQUVGSSxRQUFBQSxZQUFZLEVBQUU7QUFGWixPQVRPO0FBYVgsV0FBSTtBQUNGSixRQUFBQSxhQUFhLEVBQUUsR0FEYjtBQUVGSSxRQUFBQSxZQUFZLEVBQUU7QUFGWixPQWJPO0FBaUJYLFdBQUk7QUFDRkosUUFBQUEsYUFBYSxFQUFFLEdBRGI7QUFFRkksUUFBQUEsWUFBWSxFQUFFO0FBRlo7QUFqQk87QUFSeUUsR0FBckUsQ0FBbkIsQ0FoYTRDLENBaWM1Qzs7QUFFQSxNQUFJRyxXQUFXLEdBQUcsSUFBSXZCLE1BQUosQ0FBVyxtREFBWCxFQUFnRTtBQUNoRk8sSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLE1BQU0sRUFBRSxxQ0FERTtBQUVWQyxNQUFBQSxNQUFNLEVBQUU7QUFGRSxLQURvRTtBQUtoRkMsSUFBQUEsS0FBSyxFQUFFLElBTHlFO0FBTWhGVSxJQUFBQSxZQUFZLEVBQUUsRUFOa0U7QUFPaEZJLElBQUFBLHdCQUF3QixFQUFFLElBUHNEO0FBUWhGTixJQUFBQSxXQUFXLEVBQUU7QUFDWCxZQUFNO0FBQ0pGLFFBQUFBLGFBQWEsRUFBRTtBQURYLE9BREs7QUFJWCxXQUFLO0FBQ0hBLFFBQUFBLGFBQWEsRUFBRTtBQURaLE9BSk07QUFPWCxXQUFLO0FBQ0hBLFFBQUFBLGFBQWEsRUFBRTtBQURaLE9BUE07QUFVWCxXQUFLO0FBQ0hBLFFBQUFBLGFBQWEsRUFBRTtBQURaO0FBVk07QUFSbUUsR0FBaEUsQ0FBbEIsQ0FuYzRDLENBMmQxQztBQUVBOztBQUVBLE1BQUlLLGNBQWMsR0FBRyxJQUFJckIsTUFBSixDQUFXLHFEQUFYLEVBQWtFO0FBQ3JGZ0IsSUFBQUEsYUFBYSxFQUFFLEdBRHNFO0FBRXJGVCxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsTUFBTSxFQUFFLDBEQURFO0FBRVZDLE1BQUFBLE1BQU0sRUFBRTtBQUZFLEtBRnlFO0FBTXJGQyxJQUFBQSxLQUFLLEVBQUUsSUFOOEU7QUFPckZVLElBQUFBLFlBQVksRUFBRSxFQVB1RTtBQVFyRkYsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsR0FEWDtBQUVKSSxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQURLO0FBS1gsWUFBTTtBQUNKQSxRQUFBQSxZQUFZLEVBQUUsRUFEVjtBQUVKSixRQUFBQSxhQUFhLEVBQUU7QUFGWCxPQUxLO0FBU1gsV0FBSztBQUNIQSxRQUFBQSxhQUFhLEVBQUUsQ0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQVRNO0FBYVgsV0FBSztBQUNISixRQUFBQSxhQUFhLEVBQUUsR0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQWJNO0FBaUJYLFdBQUs7QUFDSEosUUFBQUEsYUFBYSxFQUFFLEdBRFo7QUFFSEksUUFBQUEsWUFBWSxFQUFFO0FBRlg7QUFqQk07QUFSd0UsR0FBbEUsQ0FBckIsQ0EvZDBDLENBK2YxQztBQUVBOztBQUVBLE1BQUlFLFlBQVksR0FBRyxJQUFJdEIsTUFBSixDQUFXLHdEQUFYLEVBQXFFO0FBQ3RGZ0IsSUFBQUEsYUFBYSxFQUFFLEdBRHVFO0FBRXRGVCxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsTUFBTSxFQUFFLDZEQURFO0FBRVZDLE1BQUFBLE1BQU0sRUFBRTtBQUZFLEtBRjBFO0FBTXRGQyxJQUFBQSxLQUFLLEVBQUUsSUFOK0U7QUFPdEZVLElBQUFBLFlBQVksRUFBRSxFQVB3RTtBQVF0RkYsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsR0FEWDtBQUVKSSxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQURLO0FBS1gsWUFBTTtBQUNKQSxRQUFBQSxZQUFZLEVBQUUsRUFEVjtBQUVKSixRQUFBQSxhQUFhLEVBQUU7QUFGWCxPQUxLO0FBU1gsV0FBSztBQUNIQSxRQUFBQSxhQUFhLEVBQUUsQ0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQVRNO0FBYVgsV0FBSztBQUNISixRQUFBQSxhQUFhLEVBQUUsR0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQWJNO0FBaUJYLFdBQUs7QUFDSEosUUFBQUEsYUFBYSxFQUFFLEdBRFo7QUFFSEksUUFBQUEsWUFBWSxFQUFFO0FBRlg7QUFqQk07QUFSeUUsR0FBckUsQ0FBbkIsQ0FuZ0IwQyxDQW1pQjFDO0FBR0E7O0FBQ0E7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUU7QUFFQTs7QUFFQSxNQUFJSyxjQUFjLEdBQUdoRSxRQUFRLENBQUNjLGdCQUFULENBQTBCLGdCQUExQixDQUFyQjs7QUFDQSxNQUFJa0QsY0FBSixFQUFvQjtBQUNsQkEsSUFBQUEsY0FBYyxDQUFDaEQsT0FBZixDQUF3QmlELElBQUQsSUFBVTtBQUMvQixVQUFJQyxZQUFZLEdBQUduRSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0Qk8sSUFBNUIsRUFBbkI7QUFDQVAsTUFBQUEsQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJhLE1BQTVCO0FBQ0EsVUFBSXVELE9BQU8sR0FDVCxLQUNBLCtCQURBLEdBRUEsZ0NBRkEsR0FHQSxnQkFIQSxHQUlBQyxRQUFRLENBQUNDLFFBSlQsR0FLQSxJQUxBLEdBTUFELFFBQVEsQ0FBQ0UsSUFOVCxHQU9BLDJFQVBBLEdBUUEsU0FSQSxHQVNBLFNBVEEsR0FVQSw0QkFWQSxHQVdBSixZQVhBLEdBWUEsU0FiRjtBQWNBbkUsTUFBQUEsQ0FBQyxDQUFDb0UsT0FBRCxDQUFELENBQVdJLFdBQVgsQ0FBdUIseUNBQXZCO0FBRUEsVUFBSUMsV0FBVyxHQUFHUCxJQUFJLENBQUM1RCxhQUFMLENBQW1CLGlCQUFuQixDQUFsQjtBQUNBLFVBQUlvRSxTQUFTLEdBQUdSLElBQUksQ0FBQzVELGFBQUwsQ0FBbUIsY0FBbkIsQ0FBaEI7QUFDQSxVQUFJcUUsWUFBWSxHQUFHVCxJQUFJLENBQUM1RCxhQUFMLENBQW1CLDBCQUFuQixDQUFuQjtBQUNBLFVBQUlzRSxPQUFPLEdBQUdGLFNBQVMsQ0FBQ3BFLGFBQVYsQ0FBd0IsR0FBeEIsQ0FBZDtBQUVBb0UsTUFBQUEsU0FBUyxDQUFDL0YsZ0JBQVYsQ0FBMkIsT0FBM0IsRUFBb0NrRyxRQUFwQztBQUVBSixNQUFBQSxXQUFXLENBQUM5RixnQkFBWixDQUE2QixPQUE3QixFQUFzQ2tHLFFBQXRDOztBQUVBLGVBQVNBLFFBQVQsQ0FBa0JDLENBQWxCLEVBQXFCO0FBQ25CLFlBQUlGLE9BQU8sSUFBSUUsQ0FBQyxDQUFDakcsTUFBRixJQUFZK0YsT0FBM0IsRUFBb0MsQ0FDbkMsQ0FERCxNQUNPO0FBQ0xWLFVBQUFBLElBQUksQ0FBQ3ZELFNBQUwsQ0FBZWEsTUFBZixDQUFzQixRQUF0QjtBQUNBbUQsVUFBQUEsWUFBWSxDQUFDaEUsU0FBYixDQUF1QmEsTUFBdkIsQ0FBOEIsT0FBOUI7O0FBQ0EsY0FBSTBDLElBQUksQ0FBQ3ZELFNBQUwsQ0FBZWMsUUFBZixDQUF3QixRQUF4QixDQUFKLEVBQXVDO0FBQ3JDeUMsWUFBQUEsSUFBSSxDQUFDdkQsU0FBTCxDQUFlRSxNQUFmLENBQXNCLE9BQXRCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0YsS0F0Q0Q7QUF1Q0QsR0F0bkJ5QyxDQXduQjFDO0FBRUE7O0FBQ0E7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWdCRTtBQUVBOzs7QUFFQSxNQUFJa0Usa0JBQWtCLEdBQUc5RSxRQUFRLENBQUNLLGFBQVQsQ0FBdUIsaURBQXZCLENBQXpCO0FBQ0EsTUFBSTBFLFdBQVcsR0FBRy9FLFFBQVEsQ0FBQ0ssYUFBVCxDQUF1Qix1QkFBdkIsQ0FBbEI7O0FBRUEsTUFBSXlFLGtCQUFrQixJQUFJQyxXQUExQixFQUF1QztBQUNyQ0QsSUFBQUEsa0JBQWtCLENBQUNwRyxnQkFBbkIsQ0FBb0MsT0FBcEMsRUFBNkMsVUFBVW1HLENBQVYsRUFBYTtBQUN4REEsTUFBQUEsQ0FBQyxDQUFDRyxjQUFGO0FBQ0EsVUFBTUMsT0FBTyxHQUFHLElBQUlDLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDL0NELFFBQUFBLE9BQU8sQ0FBRUosV0FBVyxDQUFDakcsS0FBWixDQUFrQlksT0FBbEIsR0FBNEIsT0FBOUIsQ0FBUDtBQUNELE9BRmUsQ0FBaEI7QUFHQXVGLE1BQUFBLE9BQU8sQ0FBQ0ksSUFBUixDQUFhLE1BQU07QUFDakI1RixRQUFBQSxVQUFVLENBQUMsWUFBWTtBQUNyQnNGLFVBQUFBLFdBQVcsQ0FBQ3JFLFNBQVosQ0FBc0JDLEdBQXRCLENBQTBCLFFBQTFCO0FBQ0FQLFVBQUFBLElBQUksQ0FBQ00sU0FBTCxDQUFlQyxHQUFmLENBQW1CLE1BQW5CO0FBQ0FMLFVBQUFBLElBQUksQ0FBQ0ksU0FBTCxDQUFlQyxHQUFmLENBQW1CLE1BQW5CO0FBQ0QsU0FKUyxFQUlQLEVBSk8sQ0FBVjtBQUtELE9BTkQ7QUFPRCxLQVpEO0FBY0EsUUFBSTJFLGNBQWMsR0FBR1AsV0FBVyxDQUFDMUUsYUFBWixDQUEwQixlQUExQixDQUFyQjs7QUFDQSxRQUFJaUYsY0FBSixFQUFvQjtBQUNsQkEsTUFBQUEsY0FBYyxDQUFDNUcsZ0JBQWYsQ0FBZ0MsT0FBaEMsRUFBeUMsWUFBWTtBQUNuRCxZQUFNdUcsT0FBTyxHQUFHLElBQUlDLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDL0NELFVBQUFBLE9BQU8sQ0FBQ0osV0FBVyxDQUFDckUsU0FBWixDQUFzQkUsTUFBdEIsQ0FBNkIsUUFBN0IsQ0FBRCxDQUFQO0FBQ0QsU0FGZSxDQUFoQjtBQUdBcUUsUUFBQUEsT0FBTyxDQUFDSSxJQUFSLENBQWEsTUFBTTtBQUNqQjVGLFVBQUFBLFVBQVUsQ0FBQyxZQUFZO0FBQ3JCc0YsWUFBQUEsV0FBVyxDQUFDakcsS0FBWixDQUFrQlksT0FBbEIsR0FBNEIsTUFBNUI7QUFDQVUsWUFBQUEsSUFBSSxDQUFDTSxTQUFMLENBQWVFLE1BQWYsQ0FBc0IsTUFBdEI7QUFDQU4sWUFBQUEsSUFBSSxDQUFDSSxTQUFMLENBQWVFLE1BQWYsQ0FBc0IsTUFBdEI7QUFDRCxXQUpTLEVBSVAsR0FKTyxDQUFWO0FBS0QsU0FORDtBQU9ELE9BWEQ7QUFZRDtBQUNGLEdBdnhCeUMsQ0F5eEIxQztBQUVBOzs7QUFFQSxNQUFJMkUsZ0JBQWdCLEdBQUcsSUFBSWhELE1BQUosQ0FBVyxzQ0FBWCxFQUFtRDtBQUN4RWdCLElBQUFBLGFBQWEsRUFBRSxJQUR5RDtBQUV4RVQsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLE1BQU0sRUFBRSx5Q0FERTtBQUVWQyxNQUFBQSxNQUFNLEVBQUU7QUFGRSxLQUY0RDtBQU14RUMsSUFBQUEsS0FBSyxFQUFFLElBTmlFO0FBT3hFVSxJQUFBQSxZQUFZLEVBQUUsRUFQMEQ7QUFReEVGLElBQUFBLFdBQVcsRUFBRTtBQUNYLFlBQU07QUFDSkYsUUFBQUEsYUFBYSxFQUFFLEdBRFg7QUFFSkksUUFBQUEsWUFBWSxFQUFFO0FBRlYsT0FESztBQUtYLFlBQU07QUFDSkEsUUFBQUEsWUFBWSxFQUFFLEVBRFY7QUFFSkosUUFBQUEsYUFBYSxFQUFFO0FBRlgsT0FMSztBQVNYLFdBQUs7QUFDSEEsUUFBQUEsYUFBYSxFQUFFLEdBRFo7QUFFSEksUUFBQUEsWUFBWSxFQUFFO0FBRlgsT0FUTTtBQWFYLFdBQUs7QUFDSEosUUFBQUEsYUFBYSxFQUFFLEdBRFo7QUFFSEksUUFBQUEsWUFBWSxFQUFFO0FBRlgsT0FiTTtBQWlCWCxXQUFLO0FBQ0hKLFFBQUFBLGFBQWEsRUFBRSxHQURaO0FBRUhJLFFBQUFBLFlBQVksRUFBRTtBQUZYO0FBakJNO0FBUjJELEdBQW5ELENBQXZCLENBN3hCMEMsQ0E2ekIxQztBQUVBOztBQUVBLE1BQUlFLFlBQVksR0FBRyxJQUFJdEIsTUFBSixDQUFXLG1EQUFYLEVBQWdFO0FBQ2pGZ0IsSUFBQUEsYUFBYSxFQUFFLEdBRGtFO0FBRWpGVCxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsTUFBTSxFQUFFLHdEQURFO0FBRVZDLE1BQUFBLE1BQU0sRUFBRTtBQUZFLEtBRnFFO0FBTWpGQyxJQUFBQSxLQUFLLEVBQUUsSUFOMEU7QUFPakZVLElBQUFBLFlBQVksRUFBRSxFQVBtRTtBQVFqRkYsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsR0FEWDtBQUVKSSxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQURLO0FBS1gsWUFBTTtBQUNKQSxRQUFBQSxZQUFZLEVBQUUsRUFEVjtBQUVKSixRQUFBQSxhQUFhLEVBQUU7QUFGWCxPQUxLO0FBU1gsV0FBSztBQUNIQSxRQUFBQSxhQUFhLEVBQUUsQ0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQVRNO0FBYVgsV0FBSztBQUNISixRQUFBQSxhQUFhLEVBQUUsR0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQWJNO0FBaUJYLFdBQUs7QUFDSEosUUFBQUEsYUFBYSxFQUFFLEdBRFo7QUFFSEksUUFBQUEsWUFBWSxFQUFFO0FBRlg7QUFqQk07QUFSb0UsR0FBaEUsQ0FBbkIsQ0FqMEIwQyxDQWkyQjFDO0FBRUE7O0FBRUEsTUFBSTZCLGVBQWUsR0FBR3hGLFFBQVEsQ0FBQ3lGLGNBQVQsQ0FBd0IsS0FBeEIsQ0FBdEI7O0FBRUEsTUFBSUQsZUFBSixFQUFxQjtBQUNuQixRQUFJRSxRQUFRLEdBQUczRixDQUFDLENBQUMsTUFBRCxDQUFELENBQVU0RixJQUFWLENBQWUsVUFBZixDQUFmO0FBQ0EsUUFBSUMsU0FBUyxHQUFHN0YsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVNEYsSUFBVixDQUFlLFdBQWYsQ0FBaEI7QUFDQSxRQUFJRSxNQUFNLEdBQUc5RixDQUFDLENBQUMsTUFBRCxDQUFELENBQVU0RixJQUFWLENBQWUsUUFBZixDQUFiO0FBQ0EsUUFBSUcsSUFBSSxHQUFHL0YsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVNEYsSUFBVixDQUFlLE1BQWYsQ0FBWDs7QUFDQSxRQUFJSSxPQUFPLEdBQUcsTUFBTTtBQUNsQixVQUFJQyxLQUFLLEdBQUdDLENBQUMsQ0FBQ0MsR0FBRixDQUFNLEtBQU4sRUFBYUMsT0FBYixDQUFxQixDQUFDVCxRQUFELEVBQVdFLFNBQVgsQ0FBckIsRUFBNENFLElBQTVDLENBQVo7QUFDQSxVQUFJTSxJQUFJLEdBQUdILENBQUMsQ0FBQ0csSUFBRixDQUFPO0FBQ2hCQyxRQUFBQSxPQUFPLEVBQUVSLE1BRE87QUFFaEJTLFFBQUFBLFFBQVEsRUFBRSxDQUFDLEVBQUQsRUFBSyxFQUFMO0FBRk0sT0FBUCxDQUFYO0FBSUFMLE1BQUFBLENBQUMsQ0FBQ00sU0FBRixDQUFZLG9EQUFaLEVBQWtFQyxLQUFsRSxDQUF3RVIsS0FBeEU7QUFDQUMsTUFBQUEsQ0FBQyxDQUFDSixNQUFGLENBQVMsQ0FBQ0gsUUFBRCxFQUFXRSxTQUFYLENBQVQsRUFBZ0M7QUFDOUJRLFFBQUFBLElBQUksRUFBRUE7QUFEd0IsT0FBaEMsRUFFR0ksS0FGSCxDQUVTUixLQUZUO0FBR0FBLE1BQUFBLEtBQUssQ0FBQ1MsZUFBTixDQUFzQkMsT0FBdEI7QUFDRCxLQVhEOztBQWFBbEIsSUFBQUEsZUFBZSxJQUFJTyxPQUFPLEVBQTFCO0FBQ0QsR0ExM0J5QyxDQTQzQjFDO0FBRUE7OztBQUVBLE1BQUlZLGFBQWEsR0FBRyxJQUFJcEUsTUFBSixDQUFXLDhEQUFYLEVBQTJFO0FBQzdGZ0IsSUFBQUEsYUFBYSxFQUFFLENBRDhFO0FBRTdGVCxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsTUFBTSxFQUFFLHlEQURFO0FBRVZDLE1BQUFBLE1BQU0sRUFBRTtBQUZFLEtBRmlGO0FBTTdGQyxJQUFBQSxLQUFLLEVBQUUsSUFOc0Y7QUFPN0ZVLElBQUFBLFlBQVksRUFBRSxDQVArRTtBQVE3RkYsSUFBQUEsV0FBVyxFQUFFO0FBQ1gsWUFBTTtBQUNKRixRQUFBQSxhQUFhLEVBQUUsR0FEWDtBQUVKSSxRQUFBQSxZQUFZLEVBQUU7QUFGVixPQURLO0FBS1gsWUFBTTtBQUNKQSxRQUFBQSxZQUFZLEVBQUUsQ0FEVjtBQUVKSixRQUFBQSxhQUFhLEVBQUU7QUFGWCxPQUxLO0FBU1gsV0FBSztBQUNIQSxRQUFBQSxhQUFhLEVBQUUsR0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQVRNO0FBYVgsV0FBSztBQUNISixRQUFBQSxhQUFhLEVBQUUsR0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWCxPQWJNO0FBaUJYLFdBQUs7QUFDSEosUUFBQUEsYUFBYSxFQUFFLEdBRFo7QUFFSEksUUFBQUEsWUFBWSxFQUFFO0FBRlgsT0FqQk07QUFxQlgsV0FBSztBQUNISixRQUFBQSxhQUFhLEVBQUUsR0FEWjtBQUVISSxRQUFBQSxZQUFZLEVBQUU7QUFGWDtBQXJCTTtBQVJnRixHQUEzRSxDQUFwQixDQWg0QjBDLENBbzZCMUM7QUFFQTs7QUFFQTVELEVBQUFBLENBQUMsQ0FBQyxZQUFZO0FBQ1pBLElBQUFBLENBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CNkcsVUFBcEIsQ0FBK0I7QUFDN0JDLE1BQUFBLFVBQVUsRUFBRSxVQURpQjtBQUU3QmhJLE1BQUFBLFFBQVEsRUFBRSxNQUZtQjtBQUc3QmlJLE1BQUFBLFFBQVEsRUFBRSxNQUFNO0FBQ2Q5RyxRQUFBQSxRQUFRLENBQUN5RixjQUFULENBQXdCLGVBQXhCLEVBQXlDc0IsYUFBekMsQ0FBdUQsSUFBSUMsS0FBSixDQUFVLFFBQVYsQ0FBdkQ7QUFDRDtBQUw0QixLQUEvQjtBQU9ELEdBUkEsQ0FBRDtBQVVBakgsRUFBQUEsQ0FBQyxDQUFDLFlBQVk7QUFDWkEsSUFBQUEsQ0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0I2RyxVQUFwQixDQUErQjtBQUM3QkMsTUFBQUEsVUFBVSxFQUFFLFVBRGlCO0FBRTdCaEksTUFBQUEsUUFBUSxFQUFFLE1BRm1CO0FBRzdCaUksTUFBQUEsUUFBUSxFQUFFLE1BQU07QUFDZDlHLFFBQUFBLFFBQVEsQ0FBQ3lGLGNBQVQsQ0FBd0IsZUFBeEIsRUFBeUNzQixhQUF6QyxDQUF1RCxJQUFJQyxLQUFKLENBQVUsUUFBVixDQUF2RDtBQUNEO0FBTDRCLEtBQS9CO0FBT0QsR0FSQSxDQUFEO0FBVUEsTUFBSUMsVUFBVSxHQUFHakgsUUFBUSxDQUFDYyxnQkFBVCxDQUEwQiwrQkFBMUIsQ0FBakI7O0FBRUEsTUFBSW1HLFVBQUosRUFBZ0I7QUFDZEEsSUFBQUEsVUFBVSxDQUFDakcsT0FBWCxDQUFvQmtHLEtBQUQsSUFBVztBQUM1QkEsTUFBQUEsS0FBSyxDQUFDeEksZ0JBQU4sQ0FBdUIsUUFBdkIsRUFBaUMsWUFBWTtBQUMzQyxZQUFJeUMsTUFBTSxHQUFHK0YsS0FBSyxDQUFDOUYsT0FBTixDQUFjLGFBQWQsQ0FBYjs7QUFDQSxZQUFJLENBQUNELE1BQU0sQ0FBQ1QsU0FBUCxDQUFpQmMsUUFBakIsQ0FBMEIsUUFBMUIsQ0FBTCxFQUEwQztBQUN4Q0wsVUFBQUEsTUFBTSxDQUFDVCxTQUFQLENBQWlCQyxHQUFqQixDQUFxQixRQUFyQjtBQUNEOztBQUNELFlBQUl3RyxLQUFLLEdBQUdoRyxNQUFNLENBQUNkLGFBQVAsQ0FBcUIsZ0JBQXJCLENBQVo7QUFDQThHLFFBQUFBLEtBQUssSUFDSEEsS0FBSyxDQUFDekksZ0JBQU4sQ0FBdUIsT0FBdkIsRUFBZ0MsWUFBWTtBQUMxQ3lDLFVBQUFBLE1BQU0sQ0FBQ1QsU0FBUCxDQUFpQkUsTUFBakIsQ0FBd0IsUUFBeEI7QUFDQXNHLFVBQUFBLEtBQUssQ0FBQ0UsS0FBTixHQUFjLEVBQWQ7QUFDRCxTQUhELENBREY7QUFLRCxPQVhEO0FBWUQsS0FiRDtBQWNEOztBQUVELE1BQUlDLGtCQUFrQixHQUFHLEtBQUtySCxRQUFMLENBQWNjLGdCQUFkLENBQStCLDhCQUEvQixDQUF6QixDQS84QjBDLENBaTlCNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTs7QUFFQSxNQUFNd0csYUFBYSxHQUFHdEgsUUFBUSxDQUFDYyxnQkFBVCxDQUEwQixpQkFBMUIsQ0FBdEI7O0FBQ0EsTUFBSXdHLGFBQUosRUFBbUI7QUFDakJDLElBQUFBLFFBQVEsQ0FBQ0MsSUFBVCxDQUFjLGlCQUFkLEVBQWlDLENBQy9CO0FBRCtCLEtBQWpDO0FBR0QsR0F2K0IyQyxDQXcrQjVDO0FBR0U7OztBQUNBLE1BQUkvSSxNQUFNLENBQUM2QyxVQUFQLEdBQW9CLEdBQXhCLEVBQTZCO0FBQzNCLFFBQU1tRyxXQUFXLEdBQUd6SCxRQUFRLENBQUNjLGdCQUFULENBQTBCLGNBQTFCLENBQXBCO0FBRUEyRyxJQUFBQSxXQUFXLENBQUN6RyxPQUFaLENBQXFCMEcsV0FBRCxJQUFpQjtBQUNuQ0EsTUFBQUEsV0FBVyxDQUFDaEosZ0JBQVosQ0FBNkIsT0FBN0IsRUFBdUNpSixLQUFELElBQVc7QUFDL0MsWUFBTUMsV0FBVyxHQUFHRixXQUFXLENBQUNySCxhQUFaLENBQTBCLGNBQTFCLENBQXBCO0FBQ0EsWUFBTXdILGVBQWUsR0FBR0QsV0FBVyxDQUFDMUYsWUFBWixDQUF5QixNQUF6QixDQUF4QjtBQUVBekQsUUFBQUEsTUFBTSxDQUFDMkYsUUFBUCxDQUFnQjBELE1BQWhCLENBQXVCRCxlQUF2QjtBQUNELE9BTEQ7QUFNRCxLQVBEO0FBUUQ7QUFDRixDQXgvQkQiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24gKCkge1xyXG4gIC8qIFNMSURFIFVQICovXHJcbiAgY29uc3Qgc2xpZGVVcCA9ICh0YXJnZXQsIGR1cmF0aW9uID0gNTAwKSA9PiB7XHJcbiAgICB0YXJnZXQuc3R5bGUudHJhbnNpdGlvblByb3BlcnR5ID0gJ2hlaWdodCwgbWFyZ2luLCBwYWRkaW5nJztcclxuICAgIHRhcmdldC5zdHlsZS50cmFuc2l0aW9uRHVyYXRpb24gPSBkdXJhdGlvbiArICdtcyc7XHJcbiAgICB0YXJnZXQuc3R5bGUuYm94U2l6aW5nID0gJ2JvcmRlci1ib3gnO1xyXG4gICAgdGFyZ2V0LnN0eWxlLmhlaWdodCA9IHRhcmdldC5vZmZzZXRIZWlnaHQgKyAncHgnO1xyXG4gICAgdGFyZ2V0Lm9mZnNldEhlaWdodDtcclxuICAgIHRhcmdldC5zdHlsZS5vdmVyZmxvdyA9ICdoaWRkZW4nO1xyXG4gICAgdGFyZ2V0LnN0eWxlLmhlaWdodCA9IDA7XHJcbiAgICB0YXJnZXQuc3R5bGUucGFkZGluZ1RvcCA9IDA7XHJcbiAgICB0YXJnZXQuc3R5bGUucGFkZGluZ0JvdHRvbSA9IDA7XHJcbiAgICB0YXJnZXQuc3R5bGUubWFyZ2luVG9wID0gMDtcclxuICAgIHRhcmdldC5zdHlsZS5tYXJnaW5Cb3R0b20gPSAwO1xyXG4gICAgd2luZG93LnNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICB0YXJnZXQuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdoZWlnaHQnKTtcclxuICAgICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdwYWRkaW5nLXRvcCcpO1xyXG4gICAgICB0YXJnZXQuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ3BhZGRpbmctYm90dG9tJyk7XHJcbiAgICAgIHRhcmdldC5zdHlsZS5yZW1vdmVQcm9wZXJ0eSgnbWFyZ2luLXRvcCcpO1xyXG4gICAgICB0YXJnZXQuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ21hcmdpbi1ib3R0b20nKTtcclxuICAgICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdvdmVyZmxvdycpO1xyXG4gICAgICB0YXJnZXQuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ3RyYW5zaXRpb24tZHVyYXRpb24nKTtcclxuICAgICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCd0cmFuc2l0aW9uLXByb3BlcnR5Jyk7XHJcbiAgICAgIC8vYWxlcnQoXCIhXCIpXHJcbiAgICB9LCBkdXJhdGlvbik7XHJcbiAgfTtcclxuXHJcbiAgLyogU0xJREUgRE9XTiAqL1xyXG4gIGNvbnN0IHNsaWRlRG93biA9ICh0YXJnZXQsIGR1cmF0aW9uID0gNTAwKSA9PiB7XHJcbiAgICB0YXJnZXQuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ2Rpc3BsYXknKTtcclxuICAgIGxldCBkaXNwbGF5ID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUodGFyZ2V0KS5kaXNwbGF5O1xyXG4gICAgaWYgKGRpc3BsYXkgPT09ICdub25lJykgZGlzcGxheSA9ICdmbGV4JztcclxuICAgIHRhcmdldC5zdHlsZS5kaXNwbGF5ID0gZGlzcGxheTtcclxuICAgIGxldCBoZWlnaHQgPSB0YXJnZXQub2Zmc2V0SGVpZ2h0O1xyXG4gICAgdGFyZ2V0LnN0eWxlLm92ZXJmbG93ID0gJ2hpZGRlbic7XHJcbiAgICB0YXJnZXQuc3R5bGUuaGVpZ2h0ID0gMDtcclxuICAgIHRhcmdldC5zdHlsZS5wYWRkaW5nVG9wID0gMDtcclxuICAgIHRhcmdldC5zdHlsZS5wYWRkaW5nQm90dG9tID0gMDtcclxuICAgIHRhcmdldC5zdHlsZS5tYXJnaW5Ub3AgPSAwO1xyXG4gICAgdGFyZ2V0LnN0eWxlLm1hcmdpbkJvdHRvbSA9IDA7XHJcbiAgICB0YXJnZXQub2Zmc2V0SGVpZ2h0O1xyXG4gICAgdGFyZ2V0LnN0eWxlLmJveFNpemluZyA9ICdib3JkZXItYm94JztcclxuICAgIHRhcmdldC5zdHlsZS50cmFuc2l0aW9uUHJvcGVydHkgPSAnaGVpZ2h0LCBtYXJnaW4sIHBhZGRpbmcnO1xyXG4gICAgdGFyZ2V0LnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IGR1cmF0aW9uICsgJ21zJztcclxuICAgIHRhcmdldC5zdHlsZS5oZWlnaHQgPSBoZWlnaHQgKyAncHgnO1xyXG4gICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdwYWRkaW5nLXRvcCcpO1xyXG4gICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdwYWRkaW5nLWJvdHRvbScpO1xyXG4gICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdtYXJnaW4tdG9wJyk7XHJcbiAgICB0YXJnZXQuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ21hcmdpbi1ib3R0b20nKTtcclxuICAgIHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdoZWlnaHQnKTtcclxuICAgICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCdvdmVyZmxvdycpO1xyXG4gICAgICB0YXJnZXQuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ3RyYW5zaXRpb24tZHVyYXRpb24nKTtcclxuICAgICAgdGFyZ2V0LnN0eWxlLnJlbW92ZVByb3BlcnR5KCd0cmFuc2l0aW9uLXByb3BlcnR5Jyk7XHJcbiAgICB9LCBkdXJhdGlvbik7XHJcbiAgfTtcclxuXHJcbiAgLyogVE9PR0dMRSAqL1xyXG4gIGNvbnN0IHNsaWRlVG9nZ2xlID0gKHRhcmdldCwgZHVyYXRpb24gPSA1MDApID0+IHtcclxuICAgIGlmICh3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSh0YXJnZXQpLmRpc3BsYXkgPT09ICdub25lJykge1xyXG4gICAgICByZXR1cm4gc2xpZGVEb3duKHRhcmdldCwgZHVyYXRpb24pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHNsaWRlVXAodGFyZ2V0LCBkdXJhdGlvbik7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgKGZ1bmN0aW9uICgkKSB7XHJcbiAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHN2ZzRldmVyeWJvZHkoe30pO1xyXG4gICAgfSk7XHJcbiAgfSkoalF1ZXJ5KTtcclxuXHJcbiAgLy8gLy9oZWFkZXJfX21lbnVcclxuXHJcbiAgbGV0IGJvZHkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdib2R5Jyk7XHJcbiAgbGV0IGh0bWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdodG1sJyk7XHJcbiAgbGV0IGhlYWRlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXJfX3dyYXAnKTtcclxuICBsZXQgc2Nyb2xsZWQgPSBmYWxzZTtcclxuICBpZiAoaGVhZGVyKSB7XHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAod2luZG93LnNjcm9sbFkgPiAwKSB7XHJcbiAgICAgICAgaGVhZGVyLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG4gICAgICAgIHNjcm9sbGVkID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBoZWFkZXIuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcbiAgICAgICAgc2Nyb2xsZWQgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGlmICh3aW5kb3cuc2Nyb2xsWSA+IDApIHtcclxuICAgICAgICBoZWFkZXIuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaGVhZGVyLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBsZXQgdHJpZ2dlcnMgPSBoZWFkZXIucXVlcnlTZWxlY3RvckFsbCgnLmxpbmtfX3RyaWdnZXInKTtcclxuICAgIGxldCB3cmFwcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oaWRlX193cmFwcGVyJyk7XHJcbiAgICBpZiAodHJpZ2dlcnMgJiYgd3JhcHApIHtcclxuICAgICAgdHJpZ2dlcnMuZm9yRWFjaCgodHJpZykgPT4ge1xyXG4gICAgICAgIHRyaWcuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VlbnRlcicsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgIHdyYXBwLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0cmlnLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICB3cmFwcC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICAvLyBsZXQgbWVudWVzID0gaGVhZGVyLnF1ZXJ5U2VsZWN0b3JBbGwoXCIubGlua19fbWVudVwiKVxyXG5cclxuICAgIC8vIGlmKHRyaWdnZXJzICYmIG1lbnVlcyl7XHJcblxyXG4gICAgLy8gICB0cmlnZ2Vycy5mb3JFYWNoKHRyaWdnZXI9PntcclxuICAgIC8vICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWVudGVyXCIsZnVuY3Rpb24oKXtcclxuICAgIC8vICAgICAgIGxldCBwYXJlbnQ9dHJpZ2dlci5jbG9zZXN0KFwiLmhlYWRlcl9fbGluay1tZW51XCIpXHJcbiAgICAvLyAgICAgICBsZXQgbWVudUJsb2NrPXBhcmVudC5xdWVyeVNlbGVjdG9yKFwiLmxpbmtfX21lbnVcIilcclxuICAgIC8vICAgICAgIC8vIGNsb3NlIGFsbFxyXG4gICAgLy8gICAgICAgaWYoIXRyaWdnZXIuY2xhc3NMaXN0LmNvbnRhaW5zKFwiYWN0aXZlXCIpKXtcclxuICAgIC8vICAgICAgICAgdHJpZ2dlcnMuZm9yRWFjaChlbD0+e1xyXG4gICAgLy8gICAgICAgICAgIGlmKGVsLmNsYXNzTGlzdC5jb250YWlucyhcImFjdGl2ZVwiKSl7XHJcbiAgICAvLyAgICAgICAgICAgICBlbC5jbGFzc0xpc3QucmVtb3ZlKFwiYWN0aXZlXCIpXHJcbiAgICAvLyAgICAgICAgICAgfVxyXG4gICAgLy8gICAgICAgICB9KVxyXG5cclxuICAgIC8vICAgICAgICAgbWVudWVzLmZvckVhY2goYmxvY2s9PntcclxuICAgIC8vICAgICAgICAgICBpZihibG9jay5jbGFzc0xpc3QuY29udGFpbnMoXCJhY3RpdmVcIikpe1xyXG4gICAgLy8gICAgICAgICAgICAgc2xpZGVVcChibG9jayw0MDApXHJcbiAgICAvLyAgICAgICAgICAgICBibG9jay5jbGFzc0xpc3QucmVtb3ZlKFwiYWN0aXZlXCIpXHJcbiAgICAvLyAgICAgICAgICAgfVxyXG4gICAgLy8gICAgICAgICB9KVxyXG5cclxuICAgIC8vICAgICAgICAgd3JhcHAuY2xhc3NMaXN0LnJlbW92ZShcImFjdGl2ZVwiKVxyXG4gICAgLy8gICAgICAgICBpZighc2Nyb2xsZWQpe1xyXG4gICAgLy8gICAgICAgICAgIGhlYWRlci5jbGFzc0xpc3QucmVtb3ZlKFwiYWN0aXZlXCIpXHJcbiAgICAvLyAgICAgICAgIH1cclxuICAgIC8vICAgICAgICAgYm9keS5jbGFzc0xpc3QucmVtb3ZlKFwibG9ja1wiKVxyXG4gICAgLy8gICAgICAgICBodG1sLmNsYXNzTGlzdC5yZW1vdmUoXCJsb2NrXCIpXHJcbiAgICAvLyAgICAgICB9XHJcblxyXG4gICAgLy8gICAgICAgLy9vcGVuIGN1cnJlbnRcclxuICAgIC8vICAgICAgIGlmKCF0cmlnZ2VyLmNsYXNzTGlzdC5jb250YWlucyhcImFjdGl2ZVwiKSl7XHJcbiAgICAvLyAgICAgICAgIHNsaWRlRG93bihtZW51QmxvY2ssNDAwKVxyXG4gICAgLy8gICAgICAgICBtZW51QmxvY2suY2xhc3NMaXN0LmFkZChcImFjdGl2ZVwiKVxyXG4gICAgLy8gICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5hZGQoXCJhY3RpdmVcIilcclxuICAgIC8vICAgICAgICAgd3JhcHAuY2xhc3NMaXN0LmFkZChcImFjdGl2ZVwiKVxyXG4gICAgLy8gICAgICAgICBpZihzY3JvbGxlZCl7XHJcbiAgICAvLyAgICAgICAgICAgaGVhZGVyLmNsYXNzTGlzdC5hZGQoXCJhY3RpdmVcIilcclxuXHJcbiAgICAvLyAgICAgICAgIH1lbHNle1xyXG4gICAgLy8gICAgICAgICAgIGlmKHdpbmRvdy5zY3JvbGxZID09IDApe1xyXG4gICAgLy8gICAgICAgICAgICAgaGVhZGVyLmNsYXNzTGlzdC5hZGQoXCJhY3RpdmVcIilcclxuICAgIC8vICAgICAgICAgICB9ZWxzZXtcclxuICAgIC8vICAgICAgICAgICAgIGhlYWRlci5jbGFzc0xpc3QucmVtb3ZlKFwiYWN0aXZlXCIpXHJcbiAgICAvLyAgICAgICAgICAgfVxyXG4gICAgLy8gICAgICAgICB9XHJcbiAgICAvLyAgICAgICAgIGJvZHkuY2xhc3NMaXN0LmFkZChcImxvY2tcIilcclxuICAgIC8vICAgICAgICAgaHRtbC5jbGFzc0xpc3QuYWRkKFwibG9ja1wiKVxyXG4gICAgLy8gICAgICAgfVxyXG5cclxuICAgIC8vICAgICB9KVxyXG5cclxuICAgIC8vICAgfSlcclxuICAgIC8vIH1cclxuXHJcbiAgICAvLyAgY2xvc2Ugb24gd2luZG93IGNsaWNrXHJcbiAgICAvLyBjb25zdCBzZWxlY3RzSW5zaWRlTWVudT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnNlbGVjdHMtaW5zaWRlXCIpXHJcbiAgICAvLyAgIHNlbGVjdHNJbnNpZGVNZW51ICYmIHNlbGVjdHNJbnNpZGVNZW51LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuICAgIC8vICAgICBjb25zdCB0YXJnZXQgPSBlLnRhcmdldFxyXG4gICAgLy8gICAgIG1lbnVlcy5mb3JFYWNoKGl0ZW09PntcclxuICAgIC8vICAgICAgIGlmICghdGFyZ2V0LmNsb3Nlc3QoJy5saW5rX19tZW51JykgJiYgaXRlbS5jbGFzc0xpc3QuY29udGFpbnMoXCJhY3RpdmVcIikgJiYgIXRhcmdldC5jbG9zZXN0KCcubGlua19fdHJpZ2dlcicpKSB7XHJcbiAgICAvLyAgICAgICAgIHNsaWRlVXAoaXRlbSw0MDApXHJcbiAgICAvLyAgICAgICAgIGl0ZW0uY2xhc3NMaXN0LnJlbW92ZShcImFjdGl2ZVwiKVxyXG4gICAgLy8gICAgICAgICB3cmFwcC5jbGFzc0xpc3QucmVtb3ZlKFwiYWN0aXZlXCIpXHJcbiAgICAvLyAgICAgICAgIGlmKCFzY3JvbGxlZCl7XHJcbiAgICAvLyAgICAgICAgICAgaGVhZGVyLmNsYXNzTGlzdC5yZW1vdmUoXCJhY3RpdmVcIilcclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgICAgICBib2R5LmNsYXNzTGlzdC5yZW1vdmUoXCJsb2NrXCIpXHJcbiAgICAvLyAgICAgICAgIGh0bWwuY2xhc3NMaXN0LnJlbW92ZShcImxvY2tcIilcclxuICAgIC8vICAgICAgICAgdHJpZ2dlcnMuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgIC8vICAgICAgICAgICBlbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJhY3RpdmVcIilcclxuICAgIC8vICAgICAgICAgfSk7XHJcbiAgICAvLyAgIH1cclxuICAgIC8vICAgICB9KVxyXG4gICAgLy8gICB9KVxyXG4gIH1cclxuXHJcbiAgLy9oZWFkZXJfX21lbnUgZW5kXHJcblxyXG4gIC8vZm9vdGVyIGFkZCBtZW51IG1vYmlsZVxyXG5cclxuICBsZXQgdHJpZ2dlckZvb3RlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5mb290ZXJfX2Jsb2NrcyAuZm9vdGVyX19ibG9jayAuYmxvY2tfX3RpdGxlJyk7XHJcblxyXG4gIGlmICh0cmlnZ2VyRm9vdGVyKSB7XHJcbiAgICB0cmlnZ2VyRm9vdGVyLmZvckVhY2goKHRyaWcpID0+IHtcclxuICAgICAgbGV0IHBhcmVudCA9IHRyaWcuY2xvc2VzdCgnLmZvb3Rlcl9fYmxvY2snKTtcclxuICAgICAgbGV0IGJsb2NrID0gcGFyZW50LnF1ZXJ5U2VsZWN0b3IoJy5ibG9ja19fbGlua3MnKTtcclxuICAgICAgdHJpZy5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPD0gNzUwKSB7XHJcbiAgICAgICAgICB0cmlnLmNsYXNzTGlzdC50b2dnbGUoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgc2xpZGVUb2dnbGUoYmxvY2ssIDQwMCk7XHJcbiAgICAgICAgICBpZiAoIWJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKCdtb2JpbGUnKSkge1xyXG4gICAgICAgICAgICBib2R5LmNsYXNzTGlzdC5hZGQoJ21vYmlsZScpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gNzUwICYmIGJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKCdtb2JpbGUnKSkge1xyXG4gICAgICAgICAgYmxvY2sucmVtb3ZlQXR0cmlidXRlKCdzdHlsZScpO1xyXG4gICAgICAgICAgdHJpZy5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPiA3NTApIHtcclxuICAgICAgICBib2R5LmNsYXNzTGlzdC5yZW1vdmUoJ21vYmlsZScpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vZm9vdGVyIGFkZCBtZW51IG1vYmlsZSBlbmRcclxuXHJcbiAgLy9idXJnZXJcclxuXHJcbiAgbGV0IGJ1cmdlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXJfX2J1cmdlcicpO1xyXG4gIGxldCBtZW51SGlkZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZWFkZXItaGlkZV9fbWVudScpO1xyXG4gIGxldCB0cmlnZ2Vyc0hpZGVNZW51ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmhpZGUtaGVhZGVyX190cmlnZ2VyJyk7XHJcbiAgbGV0IGhpZGVNZW51QWRkID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmFkZC1oZWFkZXJfX21lbnUnKTtcclxuXHJcbiAgbGV0IGNsb3NlTWVudSA9IGZ1bmN0aW9uICgpIHtcclxuICAgIG1lbnVIaWRlLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpO1xyXG4gICAgYm9keS5jbGFzc0xpc3QucmVtb3ZlKCdsb2NrJyk7XHJcbiAgICBodG1sLmNsYXNzTGlzdC5yZW1vdmUoJ2xvY2snKTtcclxuICAgIGhpZGVNZW51QWRkLmZvckVhY2goKGFkZEVsZW0pID0+IHtcclxuICAgICAgYWRkRWxlbS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcclxuICAgIH0pO1xyXG4gIH07XHJcbiAgaWYgKGJ1cmdlciAmJiBtZW51SGlkZSAmJiBoaWRlTWVudUFkZCAmJiB0cmlnZ2Vyc0hpZGVNZW51KSB7XHJcbiAgICBsZXQgY2xvc2VCdXJnZXIgPSBtZW51SGlkZS5xdWVyeVNlbGVjdG9yKCcubWVudV9fY2xvc2UnKTtcclxuICAgIGJ1cmdlci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgbWVudUhpZGUuY2xhc3NMaXN0LnRvZ2dsZSgnYWN0aXZlJyk7XHJcbiAgICAgIGJvZHkuY2xhc3NMaXN0LnRvZ2dsZSgnbG9jaycpO1xyXG4gICAgICBodG1sLmNsYXNzTGlzdC50b2dnbGUoJ2xvY2snKTtcclxuICAgIH0pO1xyXG5cclxuICAgIGNsb3NlQnVyZ2VyLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICBjbG9zZU1lbnUoKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vYWRkc1xyXG4gICAgdHJpZ2dlcnNIaWRlTWVudS5mb3JFYWNoKCh0cmlnZ2VyKSA9PiB7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaGlkZU1lbnVBZGQuZm9yRWFjaCgoYWRkKSA9PiB7XHJcbiAgICAgICAgICBpZiAoYWRkLmdldEF0dHJpYnV0ZSgnZGF0YS10YXJnZXQnKSA9PSB0cmlnZ2VyLmdldEF0dHJpYnV0ZSgnZGF0YS10YXJnZXQnKSkge1xyXG4gICAgICAgICAgICBhZGQuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgICBoaWRlTWVudUFkZC5mb3JFYWNoKChlbCkgPT4ge1xyXG4gICAgICBsZXQgY2xvc2VCdG4gPSBlbC5xdWVyeVNlbGVjdG9yKCcubWVudV9fY2xvc2UnKTtcclxuICAgICAgY2xvc2VCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBjbG9zZU1lbnUpO1xyXG4gICAgICBsZXQgYmFja0J0biA9IGVsLnF1ZXJ5U2VsZWN0b3IoJy5tZW51X19iYWNrJyk7XHJcbiAgICAgIGJhY2tCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgZWwuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gMTAyNCAmJiBtZW51SGlkZS5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpKSB7XHJcbiAgICAgICAgY2xvc2VNZW51KCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLy9idXJnZXIgZW5kXHJcblxyXG4gIC8vaGVybyBtYWlucGFnZSBzd2lwZXJcclxuXHJcbiAgdmFyIHN3aXBlciA9IG5ldyBTd2lwZXIoJy5tYWlucGFnZV9faGVybyAuaGVyb19fc2xpZGVyIC5zd2lwZXItY29udGFpbmVyJywge1xyXG4gICAgbG9vcDogdHJ1ZSxcclxuICAgIHBhZ2luYXRpb246IHtcclxuICAgICAgZWw6ICcubWFpbnBhZ2VfX2hlcm8gLmhlcm9fX3NsaWRlciAuc3dpcGVyLXBhZ2luYXRpb24nLFxyXG4gICAgICB0eXBlOiAncHJvZ3Jlc3NiYXInXHJcbiAgICB9LFxyXG4gICAgYXV0b3BsYXk6IHtcclxuICAgICAgZGVsYXk6IDQwMDAsXHJcbiAgICAgIGRpc2FibGVPbkludGVyYWN0aW9uOiBmYWxzZVxyXG4gICAgfSxcclxuICAgIG5hdmlnYXRpb246IHtcclxuICAgICAgbmV4dEVsOiAnLm1haW5wYWdlX19oZXJvIC5oZXJvX19zbGlkZXIgLnN3aXBlci1idXR0b24tbmV4dCcsXHJcbiAgICAgIHByZXZFbDogJy5tYWlucGFnZV9faGVybyAuaGVyb19fc2xpZGVyIC5zd2lwZXItYnV0dG9uLXByZXYnXHJcbiAgICB9LFxyXG4gICAgc3BlZWQ6IDEwMDBcclxuICB9KTtcclxuICBpZiAoc3dpcGVyKSB7XHJcbiAgICBzd2lwZXIub24oJ3NsaWRlQ2hhbmdlJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBsZXQgYWN0aXZlU2xpZGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubWFpbnBhZ2VfX2hlcm8gIC5zd2lwZXItc2xpZGUtYWN0aXZlJyk7XHJcbiAgICAgICAgaWYgKGFjdGl2ZVNsaWRlKSB7XHJcbiAgICAgICAgICBsZXQgcGFnaW5hdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tYWlucGFnZV9faGVybyAgLnN3aXBlci1wYWdpbmF0aW9uJyk7XHJcbiAgICAgICAgICBsZXQgZmlsbCA9IHBhZ2luYXRpb24ucXVlcnlTZWxlY3Rvcignc3BhbicpO1xyXG4gICAgICAgICAgcGFnaW5hdGlvbi5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSBhY3RpdmVTbGlkZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtY29sb3InKTtcclxuICAgICAgICAgIGZpbGwuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gYWN0aXZlU2xpZGUuZ2V0QXR0cmlidXRlKCdkYXRhLWZpbGwnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sIDUwKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLy9oZXJvIG1haW5wYWdlIHN3aXBlciBlbmRcclxuXHJcbiAgLy9tYWlucGFnZSBuZXdzIHN3aXBlclxyXG5cclxuICB2YXIgc3dpcGVyTmV3cyA9IG5ldyBTd2lwZXIoJy5tYWlucGFnZV9fbmV3cyAubmV3c19fc2xpZGVyIC5zd2lwZXItY29udGFpbmVyJywge1xyXG4gICAgc2xpZGVzUGVyVmlldzogMSxcclxuICAgIHNsaWRlc1BlckNvbHVtbjogMSxcclxuICAgIHNwZWVkOiAxMDAwLFxyXG4gICAgbmF2aWdhdGlvbjoge1xyXG4gICAgICBuZXh0RWw6ICcubWFpbnBhZ2VfX25ld3MgLm5ld3NfX3NsaWRlciAuc3dpcGVyLWJ1dHRvbi1uZXh0JyxcclxuICAgICAgcHJldkVsOiAnLm1haW5wYWdlX19uZXdzIC5uZXdzX19zbGlkZXIgLnN3aXBlci1idXR0b24tcHJldidcclxuICAgIH0sXHJcbiAgICBicmVha3BvaW50czoge1xyXG4gICAgICAxMDI1OiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMixcclxuICAgICAgICBzbGlkZXNQZXJDb2x1bW46IDJcclxuICAgICAgfSxcclxuICAgICAgNzAxOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMSxcclxuICAgICAgICBzbGlkZXNQZXJDb2x1bW46IDJcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICAvL21haW5wYWdlIG5ld3Mgc3dpcGVyIGVuZFxyXG5cclxuICAvL21haW5wYWdlIGluZm8gc3dpcGVyXHJcblxyXG4vL21haW5wYWdlIGluZm8gc3dpcGVyXHJcblxyXG52YXIgc3dpcGVyR2FsZXJ5ID0gbmV3IFN3aXBlcihcIi5tYWlucGFnZV9faW5mbyAuaW5mb19fc2xpZGVyIC5zd2lwZXItY29udGFpbmVyXCIsIHtcclxuICBzbGlkZXNQZXJWaWV3OiAxLjE1LFxyXG4gIG5hdmlnYXRpb246IHtcclxuICAgIG5leHRFbDogXCIubWFpbnBhZ2VfX2luZm8gLmluZm9fX2NvbnRhaW5lciAuc3dpcGVyLWJ1dHRvbi1uZXh0XCIsXHJcbiAgICBwcmV2RWw6IFwiLm1haW5wYWdlX19pbmZvIC5pbmZvX19jb250YWluZXIgLnN3aXBlci1idXR0b24tcHJldlwiLFxyXG4gIH0sXHJcbiAgc3BlZWQ6MTAwMCxcclxuICBzcGFjZUJldHdlZW46IDI2LFxyXG4gIGJyZWFrcG9pbnRzOiB7XHJcbiAgICAxMzUxOiB7XHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDMuNCxcclxuICAgICAgc3BhY2VCZXR3ZWVuOiAzNixcclxuICAgIH0sXHJcbiAgICAxMDI1OntcclxuICAgICAgc3BhY2VCZXR3ZWVuOjM2LFxyXG4gICAgfSxcclxuICAgIDgwMTp7XHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDMsXHJcbiAgICAgIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgICB9LFxyXG4gICAgNjAxOntcclxuICAgICAgc2xpZGVzUGVyVmlldzogMixcclxuICAgICAgc3BhY2VCZXR3ZWVuOiAyNixcclxuICAgIH0sXHJcbiAgICA0NDA6e1xyXG4gICAgICBzbGlkZXNQZXJWaWV3OiAxLjUsXHJcbiAgICAgIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgICB9XHJcblxyXG59XHJcbn0pO1xyXG5cclxuLy9tYWlucGFnZSBpbmZvIHN3aXBlciBlbmRcclxuXHJcblxyXG4vL21haW5wYWdlIGthbGVuZGFyIHN3aXBlclxyXG5cclxudmFyIHN3aXBlckthbGVuZGFyID0gbmV3IFN3aXBlcihcIi5tYWlucGFnZV9faW5mbyAua2FsZW5kYXJfX3NsaWRlciAuc3dpcGVyLWNvbnRhaW5lclwiLCB7XHJcbiAgc2xpZGVzUGVyVmlldzogMS4zLFxyXG4gIG5hdmlnYXRpb246IHtcclxuICAgIG5leHRFbDogXCIubWFpbnBhZ2VfX2luZm8gLmthbGVuZGFyX19jb250YWluZXIgLnN3aXBlci1idXR0b24tbmV4dFwiLFxyXG4gICAgcHJldkVsOiBcIi5tYWlucGFnZV9faW5mbyAua2FsZW5kYXJfX2NvbnRhaW5lciAuc3dpcGVyLWJ1dHRvbi1wcmV2XCIsXHJcbiAgfSxcclxuICBzcGVlZDoxMDAwLFxyXG4gIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgYnJlYWtwb2ludHM6IHtcclxuICAgIDEzNTE6IHtcclxuICAgICAgc2xpZGVzUGVyVmlldzogNC41LFxyXG4gICAgICBzcGFjZUJldHdlZW46IDM2LFxyXG4gICAgfSxcclxuICAgIDEwMjU6e1xyXG4gICAgICBzcGFjZUJldHdlZW46MzYsXHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDMuMyxcclxuICAgIH0sXHJcbiAgICA4MDE6e1xyXG4gICAgICBzbGlkZXNQZXJWaWV3OiAzLFxyXG4gICAgICBzcGFjZUJldHdlZW46IDI2LFxyXG4gICAgfSxcclxuICAgIDYwMTp7XHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDIuNSxcclxuICAgICAgc3BhY2VCZXR3ZWVuOiAyNixcclxuICAgIH0sXHJcbiAgICA1NDA6e1xyXG4gICAgICBzbGlkZXNQZXJWaWV3OiAxLjUsXHJcbiAgICAgIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgICB9XHJcblxyXG59XHJcbn0pO1xyXG5cclxuLy9tYWlucGFnZSBrYWxlbmRhciBzd2lwZXIgZW5kXHJcblxyXG5cclxuLy9tYWlucGFnZSBzb2NpYWwgc3dpcGVyXHJcblxyXG52YXIgc3dpcGVyU29jaWFsID0gbmV3IFN3aXBlcihcIi5tYWlucGFnZV9fc29jaWFsICAua2FsZW5kYXJfX3NsaWRlciAuc3dpcGVyLWNvbnRhaW5lclwiLCB7XHJcbiAgc2xpZGVzUGVyVmlldzogMS4zLFxyXG4gIG5hdmlnYXRpb246IHtcclxuICAgIG5leHRFbDogXCIubWFpbnBhZ2VfX3NvY2lhbCAgLmthbGVuZGFyX19jb250YWluZXIgLnN3aXBlci1idXR0b24tbmV4dFwiLFxyXG4gICAgcHJldkVsOiBcIi5tYWlucGFnZV9fc29jaWFsICAua2FsZW5kYXJfX2NvbnRhaW5lciAuc3dpcGVyLWJ1dHRvbi1wcmV2XCIsXHJcbiAgfSxcclxuICBzcGVlZDoxMDAwLFxyXG4gIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgYnJlYWtwb2ludHM6IHtcclxuICAgIDEzNTE6IHtcclxuICAgICAgc2xpZGVzUGVyVmlldzogNC41LFxyXG4gICAgICBzcGFjZUJldHdlZW46IDM2LFxyXG4gICAgfSxcclxuICAgIDEwMjU6e1xyXG4gICAgICBzcGFjZUJldHdlZW46MzYsXHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDMuMyxcclxuICAgIH0sXHJcbiAgICA4MDE6e1xyXG4gICAgICBzbGlkZXNQZXJWaWV3OiAzLFxyXG4gICAgICBzcGFjZUJldHdlZW46IDI2LFxyXG4gICAgfSxcclxuICAgIDYwMTp7XHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDIuNSxcclxuICAgICAgc3BhY2VCZXR3ZWVuOiAyNixcclxuICAgIH0sXHJcbiAgICA1NDA6e1xyXG4gICAgICBzbGlkZXNQZXJWaWV3OiAxLjUsXHJcbiAgICAgIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgICB9XHJcblxyXG59XHJcbn0pO1xyXG5cclxuLy9tYWlucGFnZSBzb2NpYWwgc3dpcGVyIGVuZFxyXG5cclxudmFyIHN3aXBlck1hcmtzID0gbmV3IFN3aXBlcignLm1haW5wYWdlLW1hcmtzICAubWFya3NfX3NsaWRlciAuc3dpcGVyLWNvbnRhaW5lcicsIHtcclxuICBuYXZpZ2F0aW9uOiB7XHJcbiAgICBuZXh0RWw6ICcubWFpbnBhZ2UtbWFya3MgLnN3aXBlci1idXR0b24tbmV4dCcsXHJcbiAgICBwcmV2RWw6ICcubWFpbnBhZ2UtbWFya3MgLnN3aXBlci1idXR0b24tcHJldidcclxuICB9LFxyXG4gIHNwZWVkOiAxMDAwLFxyXG4gIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgY2VudGVySW5zdWZmaWNpZW50U2xpZGVzOiB0cnVlLFxyXG4gIGJyZWFrcG9pbnRzOiB7XHJcbiAgICAxMDI0OiB7XHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDUuNVxyXG4gICAgfSxcclxuICAgIDk5Mjoge1xyXG4gICAgICBzbGlkZXNQZXJWaWV3OiAzLjVcclxuICAgIH0sXHJcbiAgICA1NzY6IHtcclxuICAgICAgc2xpZGVzUGVyVmlldzogMy41XHJcbiAgICB9LFxyXG4gICAgNDI1OiB7XHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDJcclxuICAgIH1cclxuICB9XHJcbn0pO1xyXG5cclxuICAvL21haW5wYWdlIGluZm8gc3dpcGVyIGVuZFxyXG5cclxuICAvL21haW5wYWdlIGthbGVuZGFyIHN3aXBlclxyXG5cclxuICB2YXIgc3dpcGVyS2FsZW5kYXIgPSBuZXcgU3dpcGVyKCcubWFpbnBhZ2VfX2luZm8gLmthbGVuZGFyX19zbGlkZXIgLnN3aXBlci1jb250YWluZXInLCB7XHJcbiAgICBzbGlkZXNQZXJWaWV3OiAxLjMsXHJcbiAgICBuYXZpZ2F0aW9uOiB7XHJcbiAgICAgIG5leHRFbDogJy5tYWlucGFnZV9faW5mbyAua2FsZW5kYXJfX2NvbnRhaW5lciAuc3dpcGVyLWJ1dHRvbi1uZXh0JyxcclxuICAgICAgcHJldkVsOiAnLm1haW5wYWdlX19pbmZvIC5rYWxlbmRhcl9fY29udGFpbmVyIC5zd2lwZXItYnV0dG9uLXByZXYnXHJcbiAgICB9LFxyXG4gICAgc3BlZWQ6IDEwMDAsXHJcbiAgICBzcGFjZUJldHdlZW46IDI2LFxyXG4gICAgYnJlYWtwb2ludHM6IHtcclxuICAgICAgMTM1MToge1xyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDQuNSxcclxuICAgICAgICBzcGFjZUJldHdlZW46IDM2XHJcbiAgICAgIH0sXHJcbiAgICAgIDEwMjU6IHtcclxuICAgICAgICBzcGFjZUJldHdlZW46IDM2LFxyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDMuM1xyXG4gICAgICB9LFxyXG4gICAgICA4MDE6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAzLFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMjZcclxuICAgICAgfSxcclxuICAgICAgNjAxOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMi41LFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMjZcclxuICAgICAgfSxcclxuICAgICAgNTQwOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMS41LFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMjZcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICAvL21haW5wYWdlIGthbGVuZGFyIHN3aXBlciBlbmRcclxuXHJcbiAgLy9tYWlucGFnZSBzb2NpYWwgc3dpcGVyXHJcblxyXG4gIHZhciBzd2lwZXJTb2NpYWwgPSBuZXcgU3dpcGVyKCcubWFpbnBhZ2VfX3NvY2lhbCAgLmthbGVuZGFyX19zbGlkZXIgLnN3aXBlci1jb250YWluZXInLCB7XHJcbiAgICBzbGlkZXNQZXJWaWV3OiAxLjMsXHJcbiAgICBuYXZpZ2F0aW9uOiB7XHJcbiAgICAgIG5leHRFbDogJy5tYWlucGFnZV9fc29jaWFsICAua2FsZW5kYXJfX2NvbnRhaW5lciAuc3dpcGVyLWJ1dHRvbi1uZXh0JyxcclxuICAgICAgcHJldkVsOiAnLm1haW5wYWdlX19zb2NpYWwgIC5rYWxlbmRhcl9fY29udGFpbmVyIC5zd2lwZXItYnV0dG9uLXByZXYnXHJcbiAgICB9LFxyXG4gICAgc3BlZWQ6IDEwMDAsXHJcbiAgICBzcGFjZUJldHdlZW46IDI2LFxyXG4gICAgYnJlYWtwb2ludHM6IHtcclxuICAgICAgMTM1MToge1xyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDQuNSxcclxuICAgICAgICBzcGFjZUJldHdlZW46IDM2XHJcbiAgICAgIH0sXHJcbiAgICAgIDEwMjU6IHtcclxuICAgICAgICBzcGFjZUJldHdlZW46IDM2LFxyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDMuM1xyXG4gICAgICB9LFxyXG4gICAgICA4MDE6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAzLFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMjZcclxuICAgICAgfSxcclxuICAgICAgNjAxOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMi41LFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMjZcclxuICAgICAgfSxcclxuICAgICAgNTQwOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMS41LFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMjZcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICAvL21haW5wYWdlIHNvY2lhbCBzd2lwZXIgZW5kXHJcblxyXG5cclxuICAvL2Nvb2tpZXNcclxuICAvKlxyXG5mdW5jdGlvbiBzZXRDb29raWUobmFtZSx2YWx1ZSxkYXlzKSB7XHJcbiAgdmFyIGV4cGlyZXMgPSBcIlwiO1xyXG4gIGlmIChkYXlzKSB7XHJcbiAgICAgIHZhciBkYXRlID0gbmV3IERhdGUoKTtcclxuICAgICAgZGF0ZS5zZXRUaW1lKGRhdGUuZ2V0VGltZSgpICsgKGRheXMqMjQqNjAqNjAqMTAwMCkpO1xyXG4gICAgICBleHBpcmVzID0gXCI7IGV4cGlyZXM9XCIgKyBkYXRlLnRvVVRDU3RyaW5nKCk7XHJcbiAgfVxyXG4gIGRvY3VtZW50LmNvb2tpZSA9IG5hbWUgKyBcIj1cIiArICh2YWx1ZSB8fCBcIlwiKSAgKyBleHBpcmVzICsgXCI7IHBhdGg9L1wiO1xyXG59XHJcbmZ1bmN0aW9uIGdldENvb2tpZShuYW1lKSB7XHJcbiAgdmFyIG5hbWVFUSA9IG5hbWUgKyBcIj1cIjtcclxuICB2YXIgY2EgPSBkb2N1bWVudC5jb29raWUuc3BsaXQoJzsnKTtcclxuICBmb3IodmFyIGk9MDtpIDwgY2EubGVuZ3RoO2krKykge1xyXG4gICAgICB2YXIgYyA9IGNhW2ldO1xyXG4gICAgICB3aGlsZSAoYy5jaGFyQXQoMCk9PScgJykgYyA9IGMuc3Vic3RyaW5nKDEsYy5sZW5ndGgpO1xyXG4gICAgICBpZiAoYy5pbmRleE9mKG5hbWVFUSkgPT0gMCkgcmV0dXJuIGMuc3Vic3RyaW5nKG5hbWVFUS5sZW5ndGgsYy5sZW5ndGgpO1xyXG4gIH1cclxuICByZXR1cm4gbnVsbDtcclxufVxyXG5cclxuICBsZXQgY29va2llc0Jsb2NrPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuY29va2llc1wiKVxyXG4gIGxldCBjb29raWVzX3N1bWJpdGVkID0gZ2V0Q29va2llKCdjb29raWVzX3N1Ym1pdGVkJylcclxuICBpZihjb29raWVzQmxvY2sgJiYgIWNvb2tpZXNfc3VtYml0ZWQpe1xyXG4gICAgc2V0VGltZW91dCgoKT0+e1xyXG4gICAgICBjb29raWVzQmxvY2suY2xhc3NMaXN0LnJlbW92ZShcIm5vLWNvb2tpZXNcIilcclxuICAgIH0sMTAwMClcclxuICAgIGxldCBjb29raWVCdG49ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5jb29raWVzX19idG5cIilcclxuICAgIGNvb2tpZUJ0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIixmdW5jdGlvbigpe1xyXG4gICAgICBzZXRDb29raWUoJ2Nvb2tpZXNfc3VibWl0ZWQnLCB0cnVlLCAwKVxyXG4gICAgICBjb29raWVzQmxvY2suY2xhc3NMaXN0LmFkZChcIm5vLWNvb2tpZXNcIilcclxuICAgIH0pXHJcbiAgfVxyXG4qL1xyXG4gIC8vY29va2llcyBlbmRcclxuXHJcbiAgLy8gY29udGFjdCBjaGVja2JveFxyXG5cclxuICBsZXQgY2hlY2tib3hQYXJlbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuZm9ybS1jaGVja2JveCcpO1xyXG4gIGlmIChjaGVja2JveFBhcmVudCkge1xyXG4gICAgY2hlY2tib3hQYXJlbnQuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICB2YXIgY2hlY2tib3hIdG1sID0gJCgnLndwY2Y3LWxpc3QtaXRlbS1sYWJlbCcpLmh0bWwoKTtcclxuICAgICAgJCgnLndwY2Y3LWxpc3QtaXRlbS1sYWJlbCcpLnJlbW92ZSgpO1xyXG4gICAgICB2YXIgbmV3SHRtbCA9XHJcbiAgICAgICAgJycgK1xyXG4gICAgICAgICc8c3BhbiBjbGFzcz1cImNoZWNrLXJldGFuZ2xlXCI+JyArXHJcbiAgICAgICAgJyA8c3ZnIGNsYXNzPVwiaWNvbiBpY29uLWRvbmUgXCI+JyArXHJcbiAgICAgICAgJyAgIDx1c2UgaHJlZj1cIicgK1xyXG4gICAgICAgIGxvY2F0aW9uLnByb3RvY29sICtcclxuICAgICAgICAnLy8nICtcclxuICAgICAgICBsb2NhdGlvbi5ob3N0ICtcclxuICAgICAgICAnL3dwLWNvbnRlbnQvdGhlbWVzL3RoZW1lL2Zyb250L2Fzc2V0cy9pY29uL3N5bWJvbC9zcHJpdGUuc3ZnI2RvbmVcIj48L3VzZT4nICtcclxuICAgICAgICAnIDwvc3ZnPicgK1xyXG4gICAgICAgICc8L3NwYW4+JyArXHJcbiAgICAgICAgJzxzcGFuIGNsYXNzPVwiY2hlY2tfX3RleHRcIj4nICtcclxuICAgICAgICBjaGVja2JveEh0bWwgK1xyXG4gICAgICAgICc8L3NwYW4+JztcclxuICAgICAgJChuZXdIdG1sKS5pbnNlcnRBZnRlcignLmZvcm0tY2hlY2tib3ggLndwY2Y3LWZvcm0tY29udHJvbC13cmFwJyk7XHJcblxyXG4gICAgICBsZXQgY2hlY2tTcXVlcmUgPSBpdGVtLnF1ZXJ5U2VsZWN0b3IoJy5jaGVjay1yZXRhbmdsZScpO1xyXG4gICAgICBsZXQgY2hlY2tUZXh0ID0gaXRlbS5xdWVyeVNlbGVjdG9yKCcuY2hlY2tfX3RleHQnKTtcclxuICAgICAgbGV0IGNoZWNrV3JhcHBlciA9IGl0ZW0ucXVlcnlTZWxlY3RvcignLndwY2Y3LWZvcm0tY29udHJvbC13cmFwJyk7XHJcbiAgICAgIGxldCBsaW5rTGF3ID0gY2hlY2tUZXh0LnF1ZXJ5U2VsZWN0b3IoJ2EnKTtcclxuXHJcbiAgICAgIGNoZWNrVGV4dC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGNoZWNrQm94KTtcclxuXHJcbiAgICAgIGNoZWNrU3F1ZXJlLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY2hlY2tCb3gpO1xyXG5cclxuICAgICAgZnVuY3Rpb24gY2hlY2tCb3goZSkge1xyXG4gICAgICAgIGlmIChsaW5rTGF3ICYmIGUudGFyZ2V0ID09IGxpbmtMYXcpIHtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgaXRlbS5jbGFzc0xpc3QudG9nZ2xlKCdhY3RpdmUnKTtcclxuICAgICAgICAgIGNoZWNrV3JhcHBlci5jbGFzc0xpc3QudG9nZ2xlKCdjaGVjaycpO1xyXG4gICAgICAgICAgaWYgKGl0ZW0uY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSkge1xyXG4gICAgICAgICAgICBpdGVtLmNsYXNzTGlzdC5yZW1vdmUoJ2Vycm9yJyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vY29udGFjdCBjaGVja2JveCBlbmRcclxuXHJcbiAgLy92YWxpZGF0ZVxyXG4gIC8qXHJcbmNvbnN0IGZvcm1zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5mb3JtLWNvbnRhaW5lclwiKVxyXG4gIGxldCBtYWlsRm9ybSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubWFpbC1mb3JtXCIpXHJcblxyXG4gIGZvcm1zICYmIGZvcm1zLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICBsZXQgZW1haWwgPSBpdGVtLnF1ZXJ5U2VsZWN0b3IoXCIubWFpbFwiKVxyXG4gICAgaWYgKGVtYWlsKSB7XHJcbiAgICAgIGxldCBwYXJlbnQgPSBlbWFpbC5jbG9zZXN0KFwiLmZvcm0taXRlbVwiKVxyXG4gICAgICBsZXQgcGF0dGVybiA9IC9eW14gXStAW14gXStcXC5bYS16XXsyLDN9JC87XHJcbiAgICAgIGVtYWlsLmFkZEV2ZW50TGlzdGVuZXIoXCJrZXl1cFwiLCBmdW5jdGlvbiAoZXYpIHtcclxuICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBsZXQgdmFsdWUgPSBlbWFpbC52YWx1ZTtcclxuICAgICAgICBpZiAodmFsdWUubWF0Y2gocGF0dGVybikpIHtcclxuICAgICAgICAgIHBhcmVudC5jbGFzc0xpc3QucmVtb3ZlKFwiZXJyb3JfX2VtYWlsXCIpXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHBhcmVudC5jbGFzc0xpc3QuYWRkKFwiZXJyb3JfX2VtYWlsXCIpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh2YWx1ZS5sZW5ndGggPCAxKSB7XHJcbiAgICAgICAgICBwYXJlbnQuY2xhc3NMaXN0LnJlbW92ZShcImVycm9yX19lbWFpbFwiKVxyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG5cclxuICAgICAgbGV0IHZhbGlkZSA9IHRydWU7XHJcbiAgICAgIGNvbnN0IGlucHV0cyA9IGl0ZW0ucXVlcnlTZWxlY3RvckFsbCgnLmZvcm0tcmVxdWlyZWQnKTtcclxuICAgICAgbGV0IGNoZWNrYm94PWl0ZW0ucXVlcnlTZWxlY3RvcihcIi5mb3JtLWNoZWNrYm94IGlucHV0XCIpXHJcbiAgICAgIGxldCBjaGVja2JveFBhcmVudD1pdGVtLnF1ZXJ5U2VsZWN0b3IoXCIuZm9ybS1jaGVja2JveFwiKVxyXG4gICAgICBpZihjaGVja2JveCl7XHJcbiAgICAgICAgdmFsaWRlPSBmYWxzZVxyXG4gICAgICAgIGlmKGNoZWNrYm94UGFyZW50LmNsYXNzTGlzdC5jb250YWlucyhcImFjdGl2ZVwiKSl7XHJcbiAgICAgICAgICB2YWxpZGU9dHJ1ZVxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgY2hlY2tib3hQYXJlbnQuY2xhc3NMaXN0LmFkZChcImVycm9yXCIpXHJcbiAgICAgICAgICB2YWxpZGU9ZmFsc2VcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlucHV0cy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG5cclxuICAgICAgICBpZiAobWFpbEZvcm0pIHtcclxuICAgICAgICAgIGxldCBlbWFpbCA9IGl0ZW0ucXVlcnlTZWxlY3RvcihcIi5tYWlsXCIpXHJcbiAgICAgICAgICBsZXQgZm9ybUl0ZW0gPSBlbWFpbC5jbG9zZXN0KFwiLmZvcm0taXRlbVwiKVxyXG4gICAgICAgICAgaWYgKGZvcm1JdGVtLmNsYXNzTGlzdC5jb250YWlucyhcImVycm9yX19lbWFpbFwiKSkge1xyXG4gICAgICAgICAgICB2YWxpZGUgPSBmYWxzZVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gbGV0IHJhZGlvPWl0ZW0ucXVlcnlTZWxlY3RvcihcIi5yYWRpb19fcGFyZW50XCIpXHJcbiAgICAgICAgLy8gaWYocmFkaW8gJiYgIXJhZGlvLmNsYXNzTGlzdC5jb250YWlucyhcImNoZWNrZWRcIikpe1xyXG4gICAgICAgIC8vICAgdmFsaWRlPWZhbHNlXHJcbiAgICAgICAgLy8gICByYWRpby5jbGFzc0xpc3QuYWRkKFwiZXJyb3JcIilcclxuXHJcblxyXG4gICAgICAgIC8vIH1cclxuXHJcblxyXG4gICAgICAgIGNvbnN0IHBhcmVudCA9IGVsZW1lbnQuY2xvc2VzdCgnLmZvcm0taXRlbScpO1xyXG4gICAgICAgIGNvbnN0IGZvcm0gPSBlbGVtZW50LmNsb3Nlc3QoXCIuZm9sb3dfX2Zvcm1cIilcclxuICAgICAgICBpZiAoZWxlbWVudC52YWx1ZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICBwYXJlbnQuY2xhc3NMaXN0LmFkZCgnZXJyb3InKTtcclxuICAgICAgICAgIGlmKHBhcmVudC5jbGFzc0xpc3QuY29udGFpbnMoXCJlcnJvci1jb21wYW55XCIpKXtcclxuICAgICAgICAgICAgcGFyZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJlcnJvclwiKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKGZvcm0pIHtcclxuICAgICAgICAgICAgZm9ybS5jbGFzc0xpc3QuYWRkKFwiZXJyb3JcIilcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHZhbGlkZSA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBwYXJlbnQuY2xhc3NMaXN0LnJlbW92ZSgnZXJyb3InKTtcclxuICAgICAgICAgIGlmIChmb3JtKSB7XHJcbiAgICAgICAgICAgIGZvcm0uY2xhc3NMaXN0LnJlbW92ZShcImVycm9yXCIpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG5cclxuXHJcbiAgICAgIGlmICh2YWxpZGUpIHtcclxuICAgICAgICBmZXRjaCgnaHR0cHM6Ly9qc29ucGxhY2Vob2xkZXIudHlwaWNvZGUuY29tL3Bvc3RzJywge1xyXG4gICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICBib2R5OiBuZXcgRm9ybURhdGEoaXRlbSlcclxuICAgICAgICB9KS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlLm9rKSB7XHJcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QocmVzcG9uc2UpO1xyXG4gICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcclxuXHJcbiAgICAgICAgfSlbXCJjYXRjaFwiXShmdW5jdGlvbiAoZXJyb3IpIHtcclxuICAgICAgICAgIGNvbnNvbGUud2FybihlcnJvcik7XHJcbiAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICBpbnB1dHMuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICAgIGVsZW1lbnQudmFsdWUgPSBcIlwiO1xyXG4gICAgICAgIH0pXHJcblxyXG5cclxuXHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgfSlcclxuXHJcblxyXG5jb25zdCBpbnB1dHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuZm9ybS1pdGVtJyk7XHJcbmZvciAobGV0IGkgPSAwOyBpIDwgaW5wdXRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgaW5wdXRzW2ldLmFkZEV2ZW50TGlzdGVuZXIoXCJpbnB1dFwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgZm9ybSA9IGlucHV0c1tpXS5jbG9zZXN0KFwiLmZvbG93X19mb3JtXCIpXHJcbiAgICBpZiAoZm9ybSkge1xyXG4gICAgICBmb3JtLmNsYXNzTGlzdC5yZW1vdmUoXCJlcnJvclwiKVxyXG4gICAgfVxyXG4gICAgaW5wdXRzW2ldLmNsYXNzTGlzdC5yZW1vdmUoXCJlcnJvclwiKTtcclxuICB9KVxyXG59XHJcbiovXHJcbiAgLy8gdmFsaWRhdGUgZW5kXHJcblxyXG4gIC8vc2Nob29sLWRldGFpbCBwb3B1cFxyXG5cclxuICBsZXQgdHJpZ2dlclBvcHVwU2Nob29sID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNjaG9vbC1kZXRhaWxfX2Fib3V0IC5hYm91dF9fbGluay5hYm91dF9fbW9kYWwnKTtcclxuICBsZXQgcG9wdXBTY2hvb2wgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2Nob29sLWRldGFpbF9fbW9kYWwnKTtcclxuXHJcbiAgaWYgKHRyaWdnZXJQb3B1cFNjaG9vbCAmJiBwb3B1cFNjaG9vbCkge1xyXG4gICAgdHJpZ2dlclBvcHVwU2Nob29sLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICBjb25zdCBwcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgIHJlc29sdmUoKHBvcHVwU2Nob29sLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snKSk7XHJcbiAgICAgIH0pO1xyXG4gICAgICBwcm9taXNlLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgcG9wdXBTY2hvb2wuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcbiAgICAgICAgICBib2R5LmNsYXNzTGlzdC5hZGQoJ2xvY2snKTtcclxuICAgICAgICAgIGh0bWwuY2xhc3NMaXN0LmFkZCgnbG9jaycpO1xyXG4gICAgICAgIH0sIDUwKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBsZXQgY2xvc2VQb3BTY2hvb2wgPSBwb3B1cFNjaG9vbC5xdWVyeVNlbGVjdG9yKCcubW9kYWxfX2Nsb3NlJyk7XHJcbiAgICBpZiAoY2xvc2VQb3BTY2hvb2wpIHtcclxuICAgICAgY2xvc2VQb3BTY2hvb2wuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgIHJlc29sdmUocG9wdXBTY2hvb2wuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJykpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHByb21pc2UudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgcG9wdXBTY2hvb2wuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgICAgICAgYm9keS5jbGFzc0xpc3QucmVtb3ZlKCdsb2NrJyk7XHJcbiAgICAgICAgICAgIGh0bWwuY2xhc3NMaXN0LnJlbW92ZSgnbG9jaycpO1xyXG4gICAgICAgICAgfSwgNTUwKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvL3NjaG9vbC1kZXRhaWwgcG9wdXAgZW5kXHJcblxyXG4gIC8vcG9zaWJpbGl0eSBzbGlkZXJcclxuXHJcbiAgdmFyIHN3aXBlclBvc2liaWxpdHkgPSBuZXcgU3dpcGVyKCcucG9zaWJpbGl0eV9fY2xhc3MgLnN3aXBlci1jb250YWluZXInLCB7XHJcbiAgICBzbGlkZXNQZXJWaWV3OiAxLjI1LFxyXG4gICAgbmF2aWdhdGlvbjoge1xyXG4gICAgICBuZXh0RWw6ICcucG9zaWJpbGl0eV9fY2xhc3MgLnN3aXBlci1idXR0b24tbmV4dHAnLFxyXG4gICAgICBwcmV2RWw6ICcucG9zaWJpbGl0eV9fY2xhc3MgLnN3aXBlci1idXR0b24tcHJldnAnXHJcbiAgICB9LFxyXG4gICAgc3BlZWQ6IDEwMDAsXHJcbiAgICBzcGFjZUJldHdlZW46IDI2LFxyXG4gICAgYnJlYWtwb2ludHM6IHtcclxuICAgICAgMTM1MToge1xyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDIuNyxcclxuICAgICAgICBzcGFjZUJldHdlZW46IDMyXHJcbiAgICAgIH0sXHJcbiAgICAgIDEwMjU6IHtcclxuICAgICAgICBzcGFjZUJldHdlZW46IDMyLFxyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDIuMlxyXG4gICAgICB9LFxyXG4gICAgICA4NTE6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAzLjIsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAzMlxyXG4gICAgICB9LFxyXG4gICAgICA2OTE6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAyLjUsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAzMlxyXG4gICAgICB9LFxyXG4gICAgICA1NDA6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAxLjUsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAzMlxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gIC8vcG9zaWJpbGl0eSBzbGlkZXIgZW5kXHJcblxyXG4gIC8vbmV3cyBzbGlkZXJcclxuXHJcbiAgdmFyIHN3aXBlclNvY2lhbCA9IG5ldyBTd2lwZXIoJy5uZXdzX19zb2NpYWwgLmthbGVuZGFyX19zbGlkZXIgLnN3aXBlci1jb250YWluZXInLCB7XHJcbiAgICBzbGlkZXNQZXJWaWV3OiAxLjMsXHJcbiAgICBuYXZpZ2F0aW9uOiB7XHJcbiAgICAgIG5leHRFbDogJy5uZXdzX19zb2NpYWwgLmthbGVuZGFyX19jb250YWluZXIgLnN3aXBlci1idXR0b24tbmV4dCcsXHJcbiAgICAgIHByZXZFbDogJy5uZXdzX19zb2NpYWwgLmthbGVuZGFyX19jb250YWluZXIgLnN3aXBlci1idXR0b24tcHJldidcclxuICAgIH0sXHJcbiAgICBzcGVlZDogMTAwMCxcclxuICAgIHNwYWNlQmV0d2VlbjogMjYsXHJcbiAgICBicmVha3BvaW50czoge1xyXG4gICAgICAxMzUxOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogNC41LFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMzZcclxuICAgICAgfSxcclxuICAgICAgMTAyNToge1xyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMzYsXHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMy4zXHJcbiAgICAgIH0sXHJcbiAgICAgIDgwMToge1xyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDMsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAyNlxyXG4gICAgICB9LFxyXG4gICAgICA2MDE6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAyLjUsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAyNlxyXG4gICAgICB9LFxyXG4gICAgICA1NDA6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAxLjUsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAyNlxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gIC8vbmV3cyBzbGlkZXIgZW5kXHJcblxyXG4gIC8vY29udGFjdCBtYXBcclxuXHJcbiAgdmFyIG1hcENvbnRhaW5lckNvbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtYXAnKTtcclxuXHJcbiAgaWYgKG1hcENvbnRhaW5lckNvbikge1xyXG4gICAgdmFyIGxhdGl0dWRlID0gJCgnI21hcCcpLmRhdGEoJ2xhdGl0dWRlJyk7XHJcbiAgICB2YXIgbG9uZ2l0dWRlID0gJCgnI21hcCcpLmRhdGEoJ2xvbmdpdHVkZScpO1xyXG4gICAgdmFyIG1hcmtlciA9ICQoJyNtYXAnKS5kYXRhKCdtYXJrZXInKTtcclxuICAgIHZhciB6b29tID0gJCgnI21hcCcpLmRhdGEoJ3pvb20nKTtcclxuICAgIHZhciBpbml0TWFwID0gKCkgPT4ge1xyXG4gICAgICB2YXIgbXltYXAgPSBMLm1hcCgnbWFwJykuc2V0VmlldyhbbGF0aXR1ZGUsIGxvbmdpdHVkZV0sIHpvb20pO1xyXG4gICAgICB2YXIgaWNvbiA9IEwuaWNvbih7XHJcbiAgICAgICAgaWNvblVybDogbWFya2VyLFxyXG4gICAgICAgIGljb25TaXplOiBbODAsIDgwXVxyXG4gICAgICB9KTtcclxuICAgICAgTC50aWxlTGF5ZXIoJ2h0dHBzOi8ve3N9LnRpbGUub3BlbnN0cmVldG1hcC5vcmcve3p9L3t4fS97eX0ucG5nJykuYWRkVG8obXltYXApO1xyXG4gICAgICBMLm1hcmtlcihbbGF0aXR1ZGUsIGxvbmdpdHVkZV0sIHtcclxuICAgICAgICBpY29uOiBpY29uXHJcbiAgICAgIH0pLmFkZFRvKG15bWFwKTtcclxuICAgICAgbXltYXAuc2Nyb2xsV2hlZWxab29tLmRpc2FibGUoKTtcclxuICAgIH07XHJcblxyXG4gICAgbWFwQ29udGFpbmVyQ29uICYmIGluaXRNYXAoKTtcclxuICB9XHJcblxyXG4gIC8vY29udGFjdCBtYXAgZW5kXHJcblxyXG4gIC8vbmV3cyBzbGlkZXJcclxuXHJcbiAgdmFyIHN3aXBlclByb2plY3QgPSBuZXcgU3dpcGVyKCcucHJvamVjdC1kZXRhaWxfX3Byb2plY3RzIC5wcm9qZWN0X19zbGlkZXIgLnN3aXBlci1jb250YWluZXInLCB7XHJcbiAgICBzbGlkZXNQZXJWaWV3OiAxLFxyXG4gICAgbmF2aWdhdGlvbjoge1xyXG4gICAgICBuZXh0RWw6ICcucHJvamVjdC1kZXRhaWxfX3Byb2plY3RzIC5wcm9qZWN0X19zbGlkZXIgLmJ1dHRvbi1uZXh0JyxcclxuICAgICAgcHJldkVsOiAnLnByb2plY3QtZGV0YWlsX19wcm9qZWN0cyAucHJvamVjdF9fc2xpZGVyIC5idXR0b24tcHJldidcclxuICAgIH0sXHJcbiAgICBzcGVlZDogMTAwMCxcclxuICAgIHNwYWNlQmV0d2VlbjogMCxcclxuICAgIGJyZWFrcG9pbnRzOiB7XHJcbiAgICAgIDEyNTE6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAxLjcsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiA3XHJcbiAgICAgIH0sXHJcbiAgICAgIDEwMjU6IHtcclxuICAgICAgICBzcGFjZUJldHdlZW46IDcsXHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMS4zXHJcbiAgICAgIH0sXHJcbiAgICAgIDgwMToge1xyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDEuMSxcclxuICAgICAgICBzcGFjZUJldHdlZW46IDBcclxuICAgICAgfSxcclxuICAgICAgNjAxOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMS41LFxyXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMFxyXG4gICAgICB9LFxyXG4gICAgICA1NzA6IHtcclxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAxLjEsXHJcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAwXHJcbiAgICAgIH0sXHJcbiAgICAgIDM1MDoge1xyXG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDEuMSxcclxuICAgICAgICBzcGFjZUJldHdlZW46IDBcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICAvL25ld3Mgc2xpZGVyIGVuZFxyXG5cclxuICAvLyBkYXRlcGlja2VyXHJcblxyXG4gICQoZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2RhdGVwaWNrZXItb2QnKS5kYXRlcGlja2VyKHtcclxuICAgICAgZGF0ZUZvcm1hdDogJ2RkLW1tLXl5JyxcclxuICAgICAgZHVyYXRpb246ICdmYXN0JyxcclxuICAgICAgb25TZWxlY3Q6ICgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZGF0ZXBpY2tlci1vZCcpLmRpc3BhdGNoRXZlbnQobmV3IEV2ZW50KCdjaGFuZ2UnKSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH0pO1xyXG5cclxuICAkKGZ1bmN0aW9uICgpIHtcclxuICAgICQoJyNkYXRlcGlja2VyLWRvJykuZGF0ZXBpY2tlcih7XHJcbiAgICAgIGRhdGVGb3JtYXQ6ICdkZC1tbS15eScsXHJcbiAgICAgIGR1cmF0aW9uOiAnZmFzdCcsXHJcbiAgICAgIG9uU2VsZWN0OiAoKSA9PiB7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2RhdGVwaWNrZXItZG8nKS5kaXNwYXRjaEV2ZW50KG5ldyBFdmVudCgnY2hhbmdlJykpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9KTtcclxuXHJcbiAgbGV0IGlucHV0c05ld3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubmV3c19fc2VsZWN0cyAuc2VsZWN0X19pbnB1dCcpO1xyXG5cclxuICBpZiAoaW5wdXRzTmV3cykge1xyXG4gICAgaW5wdXRzTmV3cy5mb3JFYWNoKChpbnB1dCkgPT4ge1xyXG4gICAgICBpbnB1dC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgbGV0IHBhcmVudCA9IGlucHV0LmNsb3Nlc3QoJy5mb3JtX19pdGVtJyk7XHJcbiAgICAgICAgaWYgKCFwYXJlbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSkge1xyXG4gICAgICAgICAgcGFyZW50LmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgY2xvc2UgPSBwYXJlbnQucXVlcnlTZWxlY3RvcignLnNlbGVjdF9fY2xvc2UnKTtcclxuICAgICAgICBjbG9zZSAmJlxyXG4gICAgICAgICAgY2xvc2UuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgaW5wdXQudmFsdWUgPSAnJztcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbGV0IHRyaWdnZXJzSW5wdXRzTmV3cyA9IHRoaXMuZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm5ld3NfX3NlbGVjdHMgLmZvcm0tdHJpZ2dlcicpO1xyXG5cclxuLy8gbGlnaHRnYWxsZXJ5XHJcbi8vIGxldCBsaWdodGdhbGxlcnlCb3hlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIubGlnaHRnYWxsZXJ5XCIpXHJcbi8vIGlmKGxpZ2h0Z2FsbGVyeUJveGVzKXtcclxuLy8gICBsaWdodGdhbGxlcnlCb3hlcy5mb3JFYWNoKGxnQm94PT57XHJcbi8vICAgICAkKGxnQm94KS5saWdodEdhbGxlcnkoeyAgICBcclxuLy8gICAgICAgdmlkZW9qczogdHJ1ZSxcclxuLy8gICAgICAgdmlkZW9qc09wdGlvbnM6IHtcclxuLy8gICAgICAgICBjb250cm9sczp0cnVlXHJcbi8vICAgICAgIH0sXHJcbi8vICAgICAgIHNwZWVkOiA1MDAsXHJcbi8vICAgICB9KTtcclxuLy8gICB9KVxyXG4vLyB9XHJcblxyXG5cclxuLy8gaW5pdCBnYWxsZXJ5IGZhbmN5Ym94IGh0dHBzOi8vZmFuY3lhcHBzLmNvbS9mYW5jeWJveC9nZXR0aW5nLXN0YXJ0ZWQvXHJcblxyXG5jb25zdCBmYW5jeWJveEl0ZW1zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtZmFuY3lib3hdJyk7XHJcbmlmIChmYW5jeWJveEl0ZW1zKSB7XHJcbiAgRmFuY3lib3guYmluZCgnW2RhdGEtZmFuY3lib3hdJywge1xyXG4gICAgLy8gWW91ciBjdXN0b20gb3B0aW9uc1xyXG4gIH0pO1xyXG59XHJcbi8vIGxpZ2h0Z2FsbGVyeSBlbmRcclxuXHJcblxyXG4gIC8vIENyZWF0ZSBtb2JpbGUgbGluayBvbiBuZXdzIHBhZ2VzIG9uIG5ld3MgaXRlbVxyXG4gIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDk5MSkge1xyXG4gICAgY29uc3QgJHNsaWRlSXRlbXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc2xpZGVfX2l0ZW0nKTtcclxuXHJcbiAgICAkc2xpZGVJdGVtcy5mb3JFYWNoKCgkc2xpZGVySXRlbSkgPT4ge1xyXG4gICAgICAkc2xpZGVySXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChldmVudCkgPT4ge1xyXG4gICAgICAgIGNvbnN0ICRzbGlkZVRpdGxlID0gJHNsaWRlckl0ZW0ucXVlcnlTZWxlY3RvcignLml0ZW1fX3RpdGxlJyk7XHJcbiAgICAgICAgY29uc3Qgc2xpZGVUaXRsZVZhbHVlID0gJHNsaWRlVGl0bGUuZ2V0QXR0cmlidXRlKCdocmVmJyk7XHJcblxyXG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5hc3NpZ24oc2xpZGVUaXRsZVZhbHVlKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn0pXHJcbiJdfQ==
