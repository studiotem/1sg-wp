<?php get_header(); ?>
<div class="error__content">
    <div class="content__container">
        <img class="error__img" src="<?= get_template_directory_uri(); ?>/front/assets/svg/error.svg" alt="404">
        <div class="error__text"><?= __( 'Ospravedlňujeme sa, táto stránka zrejme neexistuje.', 'theme' ); ?></div>
        <a class="error__btn" href="<?php echo esc_url(home_url('/'));?>"><?= __( 'Prejsť na úvodnú stránku', 'theme' ); ?></a>
    </div>
</div>
<?php get_footer(); ?>